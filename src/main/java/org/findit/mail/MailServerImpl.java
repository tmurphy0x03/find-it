/**
 *
 * @author Anthony Murphy
 */

package org.findit.mail;

import org.findit.model.Retailer;
import org.findit.model.User;
import org.jasypt.util.text.BasicTextEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class MailServerImpl implements MailServer {
	private MailSender mailSender;
    private BasicTextEncryptor basicTextEncryptor;

    @Override
    public void sendForgotPasswordMail(User user) {
        SimpleMailMessage msg = new SimpleMailMessage();
        String messageText = "Dear "+user.getFirstName() + " " + user.getSurname() + ",\n\n";
        messageText += "Your password is: " + basicTextEncryptor.decrypt(user.getPassword())+"\n";
        messageText += "Please keep this password safe.\n\n";
        messageText += "This is a generated Email, please do not reply, \nRegards \nFindIt!";
        msg.setTo(user.getEmail());
        msg.setFrom("FindIt");
        msg.setSubject("Forgot Password");
        msg.setText(messageText);
        try{
            mailSender.send(msg);
        }
        catch(MailException ex) {
            System.err.println(ex.getMessage());            
        }
    }
    
    @Override
	public void sendForgotPasswordMail(Retailer retailer) {
    	SimpleMailMessage msg = new SimpleMailMessage();
        String messageText = "Dear "+retailer.getName() + " staff member,\n\n";
        messageText += "Your password is: " + basicTextEncryptor.decrypt(retailer.getPassword())+"\n";
        messageText += "Please keep this password safe.\n\n";
        messageText += "This is a generated Email, please do not reply, \nRegards \nFindIt!";
        msg.setTo(retailer.getEmail());
        msg.setSubject("Forgot Password");
        msg.setText(messageText);
        try{
            mailSender.send(msg);
        }
        catch(MailException ex) {
            System.err.println(ex.getMessage());            
        }
	}
    
    @Autowired
    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Autowired
    public void setBasicTextEncryptor(BasicTextEncryptor basicTextEncryptor){
    	this.basicTextEncryptor = basicTextEncryptor;
    }

	@Override
	public void sendUpdateAccountEmail(Retailer oldRetailer, Retailer retailer) {
		SimpleMailMessage msg = new SimpleMailMessage();
        String messageText = "Dear "+retailer.getName() + " staff member,\n\n";
        messageText += "Your Account Details have changed:\nYour password is: " + retailer.getPassword()+"\nYour email address is: "+retailer.getEmail()+"\n";
        messageText += "Please keep this password safe.\n\n";
        messageText += "This is a generated Email, please do not reply, \nRegards \nFindIt!";
        msg.setTo(oldRetailer.getEmail());
        msg.setSubject("Update Account");
        msg.setText(messageText);
        try{
            mailSender.send(msg);
        }
        catch(MailException ex) {
            System.err.println(ex.getMessage());            
        }
	}
}
