/**
 *
 * @author Anthony Murphy
 */

package org.findit.mail;

import org.findit.model.Retailer;
import org.findit.model.User;

public interface MailServer {
	public void sendForgotPasswordMail(User user);
	public void sendForgotPasswordMail(Retailer retailer);
	public void sendUpdateAccountEmail(Retailer oldRetailer, Retailer retailer);
}
