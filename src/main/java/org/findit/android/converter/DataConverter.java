/**
 *
 * @author Anthony Murphy
 */

package org.findit.android.converter;

import java.util.LinkedList;
import java.util.List;

import org.findit.android.data.BranchData;
import org.findit.android.data.ProductData;
import org.findit.android.data.ServiceData;
import org.findit.manager.BranchManager;
import org.findit.model.Branch;
import org.findit.model.Product;
import org.findit.model.Service;
import org.springframework.beans.factory.annotation.Autowired;

public class DataConverter {
	private BranchManager branchManager;
	
	public ProductData convertProduct(Product product, Branch branch){
		ProductData productData = new ProductData();
		productData.setName(product.getName());
		productData.setId(product.getId());
		productData.setCategory(product.getCategory());
		productData.setDescription(product.getDescription());
		productData.setHits(product.getHits());
		productData.setPrice(product.getPrice());
		productData.setRetailerName(product.getRetailerName());
		productData.setRetailerUsername(product.getRetailerUsername());
		
		productData.setLatitude(branch.getLatitude());
		productData.setLongitude(branch.getLongitude());
		productData.setBranchId(branch.getId());
		productData.setBranchName(branch.getName());
		productData.setAddress1(branch.getAddress1());
		productData.setAddress2(branch.getAddress2());
		productData.setAddress3(branch.getAddress3());
		productData.setCountry(branch.getCountry());
		productData.setLocation(branch.getLocation());
		productData.setPostcode(branch.getPostcode());
		productData.setProperty(branch.getProperty());
		productData.setRegion(branch.getRegion());
		return productData;
	}
	
	public List<ProductData> convertProduct(List<Product> products){
		List<ProductData> productData = new LinkedList<ProductData>();
		List<Branch> branches;
		for(Product product : products){
			branches = branchManager.getBranchesByRetailerUsername(product.getRetailerUsername());
			for(Branch branch : branches){
				System.out.println(product);
				if(branch.getProducts().contains(product)){
					productData.add(convertProduct(product, branch));
				}
			}
		}
		return productData;
	}
	
	public BranchData convertBranch(Branch branch){
		BranchData branchData = new BranchData();
		branchData.setName(branch.getName());
		branchData.setId(branch.getId());
		branchData.setAddress1(branch.getAddress1());
		branchData.setAddress2(branch.getAddress2());
		branchData.setAddress3(branch.getAddress3());
		branchData.setCountry(branch.getCountry());
		branchData.setHits(branch.getHits());
		branchData.setLatitude(branch.getLatitude());
		branchData.setLocation(branch.getLocation());
		branchData.setLongitude(branch.getLongitude());
		branchData.setPhoneNumber(branch.getPhoneNumber());
		branchData.setEmail(branch.getEmail());
		branchData.setPostcode(branch.getPostcode());
		branchData.setProperty(branch.getProperty());
		branchData.setRegion(branch.getRegion());
		branchData.setRetailerName(branch.getRetailerName());
		branchData.setRetailerUsername(branch.getRetailerUsername());
		return branchData;
	}
	
	public List<BranchData> convertBranch(List<Branch> branches){
		List<BranchData> branchData = new LinkedList<BranchData>();
		for(Branch branch : branches){
			branchData.add(convertBranch(branch));
		}
		return branchData;
	}
	
	public ServiceData convertService(Service service, Branch branch){
		ServiceData serviceData = new ServiceData();
		serviceData.setName(service.getName());
		serviceData.setId(service.getId());
		serviceData.setCategory(service.getCategory());
		serviceData.setDescription(service.getDescription());
		serviceData.setHits(service.getHits());
		serviceData.setPrice(service.getPrice());
		serviceData.setRetailerName(service.getRetailerName());
		serviceData.setRetailerUsername(service.getRetailerUsername());
		
		serviceData.setLatitude(branch.getLatitude());
		serviceData.setLongitude(branch.getLongitude());
		serviceData.setBranchId(branch.getId());
		serviceData.setBranchName(branch.getName());
		serviceData.setAddress1(branch.getAddress1());
		serviceData.setAddress2(branch.getAddress2());
		serviceData.setAddress3(branch.getAddress3());
		serviceData.setCountry(branch.getCountry());
		serviceData.setLocation(branch.getLocation());
		serviceData.setPostcode(branch.getPostcode());
		serviceData.setRegion(branch.getRegion());
		return serviceData;
	}
	
	public List<ServiceData> convertService(List<Service> services){
		List<ServiceData> serviceData = new LinkedList<ServiceData>();
		List<Branch> branches;
		for(Service service : services){
			branches = branchManager.getBranchesByRetailerUsername(service.getRetailerUsername());
			for(Branch branch : branches){
				System.out.println(service);
				if(branch.getServices().contains(service)){
					serviceData.add(convertService(service, branch));
				}
			}
		}
		return serviceData;
	}
	
	@Autowired
	public void setBranchManager(BranchManager branchManager){
		this.branchManager = branchManager;
	}

	public List<Product> convertProductData(List<ProductData> productData) {
		List<Product> products = new LinkedList<Product>();
		for(ProductData product : productData){
			products.add(convertProductData(product));
		}
		return products;
	}
	
	public Product convertProductData(ProductData productData) {
		Product product = new Product();
		product.setName(productData.getName());
		product.setId(productData.getId());
		product.setCategory(productData.getCategory());
		product.setDescription(productData.getDescription());
		product.setHits(productData.getHits());
		product.setPrice(productData.getPrice());
		product.setRetailerName(productData.getRetailerName());
		product.setRetailerUsername(productData.getRetailerUsername());
		
		return product;	
	}
	
	public List<Service> convertServiceData(List<ServiceData> serviceData) {
		List<Service> services = new LinkedList<Service>();
		for(ServiceData service : serviceData){
			services.add(convertServiceData(service));
		}
		return services;
	}
	
	public Service convertServiceData(ServiceData serviceData) {
		Service service = new Service();
		service.setName(serviceData.getName());
		service.setId(serviceData.getId());
		service.setCategory(serviceData.getCategory());
		service.setDescription(serviceData.getDescription());
		service.setHits(serviceData.getHits());
		service.setPrice(serviceData.getPrice());
		service.setRetailerName(serviceData.getRetailerName());
		service.setRetailerUsername(serviceData.getRetailerUsername());
		
		return service;	
	}
}
