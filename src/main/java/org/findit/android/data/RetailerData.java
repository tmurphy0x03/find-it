package org.findit.android.data;

import java.util.LinkedList;
import java.util.List;

public class RetailerData {
	private String name;
	private String email;
	private String username;
	private String password;
	private String confirmPassword;
	private List<BranchData> branches;

	@Override
	public String toString() {
		return "RetailerData [name=" + name + ", email=" + email + ", username="
				+ username + ", password=" + password + ", confirmPassword="
				+ confirmPassword + ", branches=" + branches + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetailerData other = (RetailerData) obj;
		if (confirmPassword == null) {
			if (other.confirmPassword != null)
				return false;
		} else if (!confirmPassword.equals(other.confirmPassword))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	public RetailerData(){
		branches = new LinkedList<BranchData>();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setBranches(List<BranchData> branches) {
		this.branches = branches;
	}

	public List<BranchData> getBranches() {
		return branches;
	}
}