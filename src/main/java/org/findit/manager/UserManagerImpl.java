package org.findit.manager;

import org.findit.dao.RetailerDao;
import org.findit.dao.UserDao;
import org.findit.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public class UserManagerImpl implements UserManager {
	private UserDao userDao;
	private UserDetailsService userService;
	private RetailerDao retailerDao;
	
	@Override
	public User getUserByUsername(String username) {
		return userDao.getUserByUsername(username);
	}
	
	@Override
	public UserDetails loadUserByUsername(String username){
		return userService.loadUserByUsername(username);
	}

	@Override
	public User updateUser(User user) {
		return userDao.updateUser(user);
	}
	
	@Override
	public boolean exists(String username) {
		if(userDao.exists(username) || retailerDao.exists(username)){
			return true;
		}
		return false;
	}
	
	@Autowired
	public void setUserDao(UserDao userDao){
		this.userDao = userDao;
	}
	
	@Autowired
	public void setUserService(UserDetailsService userService){
		this.userService = userService;
	}
	
	@Autowired
	public void setRetailerDao(RetailerDao retailerDao){
		this.retailerDao = retailerDao;
	}
}
