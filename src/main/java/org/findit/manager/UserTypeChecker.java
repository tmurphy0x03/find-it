/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import org.springframework.security.core.context.SecurityContextHolder;

public class UserTypeChecker {
	public boolean isRetailer(){
		if(SecurityContextHolder.getContext().getAuthentication() != null){
			if ((Boolean) SecurityContextHolder.getContext().getAuthentication().getDetails()){
				return true;
			}
		}
		return false;
	}
	
	public boolean isUser(){
		if(SecurityContextHolder.getContext().getAuthentication() != null){
			if (! (Boolean) SecurityContextHolder.getContext().getAuthentication().getDetails()){
				return true;
			}
		}
		return false;
	}
	
	public String getLoggedInUsername(){
		if(SecurityContextHolder.getContext().getAuthentication() != null){
			 return SecurityContextHolder.getContext().getAuthentication().getName();
		}
		return null;
	}
	
	public boolean hasAccount(){
		if(isUser() || isRetailer()){
			return true;
		}
		return false;
	}
}
