/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import org.findit.dao.UserDao;
import org.findit.model.SecurityUser;
import org.findit.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserService implements UserDetailsService {
	private UserDao userDao;
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		User user = userDao.getUserByUsername(username);
		if(user != null){
			SecurityUser securityUser = new SecurityUser(user.getUsername(), user.getPassword(), false, true, true, true, null);
			return securityUser;
		}
		return null;
	}
	
	@Autowired
	public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
