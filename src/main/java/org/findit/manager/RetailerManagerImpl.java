/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import org.findit.dao.RetailerDao;
import org.findit.dao.UserDao;
import org.findit.model.Retailer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class RetailerManagerImpl implements RetailerManager {
	private RetailerDao retailerDao;
	private UserDetailsService retailerService;
	private UserDao userDao;
	
	@Override
	public Retailer getRetailerByUsername(String username) {
		return retailerDao.getRetailerByUsername(username);
	}

	@Override
	public Retailer updateRetailer(Retailer retailer) {
		return retailerDao.updateRetailer(retailer);
	}

	@Override
	public boolean exists(String username) {
		if(retailerDao.exists(username) || userDao.exists(username)){
			return true;
		}
		return false;
	}

	@Override
	public UserDetails loadUserByUsername(String username) {
		return retailerService.loadUserByUsername(username);
	}
	
	@Autowired
	public void setRetailerDao(RetailerDao retailerDao){
		this.retailerDao = retailerDao;
	}
	
	@Autowired
	public void setRetailerService(UserDetailsService retailerService){
		this.retailerService = retailerService;
	}
	
	@Autowired
	public void setUserDao(UserDao userDao){
		this.userDao = userDao;
	}

}
