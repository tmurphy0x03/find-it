/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import java.util.List;

import org.findit.android.data.ProductData;
import org.findit.model.Product;

public interface ProductManager {
	public Product updateProduct(Product product);
	public Product getProductById(int productId);
	public void removeProduct(Product product);
	public List<Product> locateProduct(String name, String description, String category, String longitude, String latutude);
	public List<ProductData> sortProductsByLocation(List<ProductData> products, String longitude, String latiutude);
	public List<Product> getAllProducts();
	public List<Product> sortProductsByRelevance(List<Product> products, Product searchProduct);
	public List<Product> getProducts(Product product, String match, String sort, String longitude, String latitude);
	public List<Product> findit(String param);
}
