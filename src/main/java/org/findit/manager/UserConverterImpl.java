/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import org.findit.model.Retailer;
import org.findit.model.User;

public class UserConverterImpl implements UserConverter {

	@Override
	public User convert(Retailer retailer) {
		User user = new User();
		user.setUsername(retailer.getUsername());
		user.setPassword(retailer.getPassword());
		return user;
	}

	@Override
	public Retailer convert(User user) {
		Retailer retailer = new Retailer();
		retailer.setUsername(user.getUsername());
		retailer.setPassword(user.getPassword());
		return retailer;
	}

}
