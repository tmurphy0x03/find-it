/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.findit.dao.BranchDao;
import org.findit.model.Branch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class BranchManagerImpl implements BranchManager{
	private BranchDao branchDao;
	
	@Override
	public Branch updateBranch(Branch branch) {
		return branchDao.updateBranch(branch);
	}
	
	@Override
	public Branch getBranchById(int branchId){
		return branchDao.getBranchById(branchId);
	}

	@Override
	public void removeBranch(Branch branch) {
		branchDao.removeBranch(branch);
	}
	
	@Autowired
	public void setBranchDao(BranchDao branchDao) {
		this.branchDao = branchDao;
	}

	@Override
	public List<Branch> locateBranch(String name, String location, String retailer, String longitude, String latitude) {
		return branchDao.locateBranch(name, location, retailer, longitude, latitude);
	}

	@Override
	public List<Branch> getBranchesByRetailerUsername(String retailerUsername) {
		return branchDao.getBranchesByRetailerUsername(retailerUsername);
	}
	
	@Override
	public List<Branch> getAllBranches(){
		return branchDao.getAllBranches();
	}
	
	@Override
	public List<Branch> whatsAroundMe(String longitude, String latitude){
		List<Branch> branches = getAllBranches();
		return sortBranchesByLocation(branches, longitude, latitude);
	}
	
	@Override
	public List<Branch> sortBranchesByLocation(List<Branch> branches, String longitude, String latitude){
		List<Branch> nearbyBranches = new LinkedList<Branch>();
		
		System.out.println("branches: "+branches.size()+branches);
		nearbyBranches.add(branches.remove(0));
		double latDiff;
		double lngDiff;
		double savedBranchDiff;
		double nextBranchDiff;
		for(Branch branch : branches){
			for(int i = 0; i < nearbyBranches.size(); i++){
				/* Get the latitude difference */
				if(Double.parseDouble(latitude) > Double.parseDouble(nearbyBranches.get(i).getLatitude())){
					latDiff = Double.parseDouble(latitude) - Double.parseDouble(nearbyBranches.get(i).getLatitude());
				}
				else {
					latDiff = Double.parseDouble(nearbyBranches.get(i).getLatitude()) - Double.parseDouble(latitude);
				}
				
				/* Get the longitude difference */
				if(Double.parseDouble(longitude) > Double.parseDouble(nearbyBranches.get(i).getLongitude())){
					lngDiff = Double.parseDouble(longitude) - Double.parseDouble(nearbyBranches.get(i).getLongitude());
				}
				else {
					lngDiff = Double.parseDouble(nearbyBranches.get(i).getLongitude()) - Double.parseDouble(longitude);
				}
				
				/* Add latitude difference and longitude difference to get overall difference */
				savedBranchDiff = latDiff + lngDiff;
				
				/* Get the latitude difference */
				if(Double.parseDouble(latitude) > Double.parseDouble(branch.getLatitude())){
					latDiff = Double.parseDouble(latitude) - Double.parseDouble(branch.getLatitude());
				}
				else {
					latDiff = Double.parseDouble(branch.getLatitude()) - Double.parseDouble(latitude);
				}
				
				/* Get the longitude difference */
				if(Double.parseDouble(longitude) > Double.parseDouble(branch.getLongitude())){
					lngDiff = Double.parseDouble(longitude) - Double.parseDouble(branch.getLongitude());
				}
				else {
					lngDiff = Double.parseDouble(branch.getLongitude()) - Double.parseDouble(longitude);
				}
				
				/* Add latitude difference and longitude difference to get overall difference */
				nextBranchDiff = latDiff + lngDiff;
				
				System.out.println("diff: "+nextBranchDiff+" "+savedBranchDiff);
				if(nextBranchDiff < savedBranchDiff){
					System.out.println("ADDING");
					nearbyBranches.add(i, branch);
					break;
				}
			}
			if(!nearbyBranches.contains(branch)){
				nearbyBranches.add(branch);
			}
		}
		
		System.out.println("Nearby Branches: "+nearbyBranches.size()+nearbyBranches);
		return nearbyBranches;
	}
	
	@Override
	public List<Branch> getBranchesByRetailerFirstLetter(String beginsWith) {
		return branchDao.getBranchesByRetailerFirstLetter(beginsWith.charAt(0));
	}

	@Override
	public List<Branch> getBranches(Branch branch, String match, String sort) {
		List<Branch> branches = branchDao.getBranches(branch, match, sort);
		if(sort.equals("location") && branches.size() > 0){
			return sortBranchesByLocation(branches, branch.getLongitude(), branch.getLatitude());
		}
		if(sort.equals("relevance") && branches.size() > 0){
			return sortBranchesByRelevance(branches, branch);
		}
		return branches;
	}
	
	@Override
	public List<Branch> sortBranchesByRelevance(List<Branch> branches, Branch searchBranch){
		List<Branch> branchValues = new LinkedList<Branch>();
		List<Integer> weightValues = new LinkedList<Integer>();
		
		System.out.println("BRANCHES SIZE: "+branches.size());
		int weight = 0;
		Branch branch;
		for(int i = 0; i < branches.size(); i++){
			branch = branches.get(i);
			weight = 0;
			
			if(branch.getName().equalsIgnoreCase(searchBranch.getName())){
				weight += 10;
			}
			else if(!branch.getName().equals("") && branch.getName().toLowerCase().contains(searchBranch.getName().toLowerCase())){
				weight += 5;
			}
			
			if(branch.getAddress1().equalsIgnoreCase(searchBranch.getAddress1())){
				weight += 5;
			}
			else if(!branch.getAddress1().equals("") && branch.getAddress1().toLowerCase().contains(searchBranch.getAddress1().toLowerCase())){
				weight += 1;
			}
			
			if(branch.getAddress2().equalsIgnoreCase(searchBranch.getAddress2())){
				weight += 5;
			}
			else if(!branch.getAddress2().equals("") && branch.getAddress2().toLowerCase().contains(searchBranch.getAddress2().toLowerCase())){
				weight += 1;
			}
			
			if(branch.getAddress3().equalsIgnoreCase(searchBranch.getAddress3())){
				weight += 5;
			}
			else if(!branch.getAddress3().equals("") && branch.getAddress3().toLowerCase().contains(searchBranch.getAddress3().toLowerCase())){
				weight += 1;
			}
			
			if(branch.getCountry().equalsIgnoreCase(searchBranch.getCountry())){
				weight += 5;
			}
			else if(!branch.getCountry().equals("") && branch.getCountry().toLowerCase().contains(searchBranch.getCountry().toLowerCase())){
				weight += 1;
			}
			
			if(branch.getLocation().equalsIgnoreCase(searchBranch.getLocation())){
				weight += 5;
			}
			else if(!branch.getLocation().equals("") && branch.getLocation().toLowerCase().contains(searchBranch.getLocation().toLowerCase())){
				weight += 1;
			}
			
			if(branch.getPostcode().equalsIgnoreCase(searchBranch.getPostcode())){
				weight += 5;
			}
			else if(!branch.getPostcode().equals("") && branch.getPostcode().toLowerCase().contains(searchBranch.getPostcode().toLowerCase())){
				weight += 1;
			}
			
			if(branch.getProperty().equalsIgnoreCase(searchBranch.getProperty())){
				weight += 5;
			}
			else if(!branch.getProperty().equals("") && branch.getProperty().toLowerCase().contains(searchBranch.getProperty().toLowerCase())){
				weight += 1;
			}
			
			if(branch.getRegion().equalsIgnoreCase(searchBranch.getRegion())){
				weight += 5;
			}
			else if(!branch.getRegion().equals("") && branch.getRegion().toLowerCase().toLowerCase().contains(searchBranch.getRegion().toLowerCase())){
				weight += 1;
			}
			
			if(branchValues.size() == 0){
				branchValues.add(branch);
				weightValues.add(weight);
			}
			
			else {
				for(int j = 0; j < branchValues.size(); j++){
					if(weight >= weightValues.get(j)){
						branchValues.add(j, branch);
						weightValues.add(j, weight);
						break;
					}
					if(j+1 == branchValues.size()){
						branchValues.add(j, branch);
						weightValues.add(j, weight);
						break;
					}
				}
			}
		}
		
		System.out.println(branchValues);
		System.out.println(weightValues);
		return branchValues;
	}
	
	@Override
	public List<Branch> findit(String param){
		return branchDao.findit(param);
	}
}
