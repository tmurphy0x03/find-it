/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import org.findit.model.Retailer;
import org.springframework.security.core.userdetails.UserDetails;

public interface RetailerManager {
	public Retailer getRetailerByUsername(String username);
	public Retailer updateRetailer(Retailer retailer);
	public boolean exists(String username);
	public UserDetails loadUserByUsername(String username);
}
