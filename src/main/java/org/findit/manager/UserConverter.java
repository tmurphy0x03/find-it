/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import org.findit.model.Retailer;
import org.findit.model.User;

public interface UserConverter {
	public User convert(Retailer retailer);
	public Retailer convert(User user);
}
