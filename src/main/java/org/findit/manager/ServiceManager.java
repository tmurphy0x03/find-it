/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import java.util.List;

import org.findit.android.data.ServiceData;
import org.findit.model.Service;

public interface ServiceManager {
	public Service updateService(Service service);
	public Service getServiceById(int serviceId);
	public void removeService(Service service);
	public List<Service> locateService(String name, String description, String category, String longitude, String latitude);
	public List<ServiceData> sortServicesByLocation(List<ServiceData> services, String longitude, String latiutude);
	public List<Service> getAllServices();
	public List<Service> getServices(Service service, String match, String sort, String longitude, String latitude);
	public List<Service> sortServicesByRelevance(List<Service> services, Service searchService);
	public List<Service> findit(String param);
}
