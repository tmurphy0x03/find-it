/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import org.findit.dao.RetailerDao;
import org.findit.model.Retailer;
import org.findit.model.SecurityRetailer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class RetailerService implements UserDetailsService {
	private RetailerDao retailerDao;
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		Retailer retailer = retailerDao.getRetailerByUsername(username);
		if(retailer != null){
			SecurityRetailer securityRetailer = new SecurityRetailer(retailer.getUsername(), retailer.getPassword(), true, true, true, true, null);
			return securityRetailer;
		}
		return null;
	}

	@Autowired
	public void setRetailerDao(RetailerDao retailerDao){
		this.retailerDao = retailerDao;
	}
}
