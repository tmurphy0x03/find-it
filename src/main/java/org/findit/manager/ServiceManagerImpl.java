/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import java.util.LinkedList;
import java.util.List;

import org.findit.android.converter.DataConverter;
import org.findit.android.data.ServiceData;
import org.findit.dao.ServiceDao;
import org.findit.model.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class ServiceManagerImpl implements ServiceManager {
	private ServiceDao serviceDao;
	private DataConverter dataConverter;
	
	@Override
	public Service updateService(Service service) {
		return serviceDao.updateService(service);
	}

	@Override
	public Service getServiceById(int serviceId) {
		return serviceDao.getServiceById(serviceId);
	}

	@Override
	public void removeService(Service service) {
		serviceDao.removeService(service);
	}
	
	@Override
	public List<Service> locateService(String name, String description, String category, String longitude, String latitude) {
		return serviceDao.locateService(name, description, category, longitude, latitude);
	}

	@Override
	public List<Service> getServices(Service service, String match, String sort, String longitude, String latitude) {
		List<Service> services = serviceDao.getServices(service, match, sort);
		if(sort.equals("location") && services.size() > 0){
			List<ServiceData> serviceData = sortServicesByLocation(dataConverter.convertService(services), longitude, latitude);
			return dataConverter.convertServiceData(serviceData);
		}
		if(sort.equals("relevance") && services.size() > 0){
			return sortServicesByRelevance(services, service);
		}
		return services;
	}
	
	@Override
	public List<ServiceData> sortServicesByLocation(List<ServiceData> services, String longitude, String latitude) {
		List<ServiceData> nearbyServices = new LinkedList<ServiceData>();
		
		System.out.println("servicees: "+services.size()+services);
		nearbyServices.add(services.remove(0));
		double latDiff;
		double lngDiff;
		double savedserviceDiff;
		double nextserviceDiff;
		for(ServiceData service : services){
			for(int i = 0; i < nearbyServices.size(); i++){
				/* Get the latitude difference */
				if(Double.parseDouble(latitude) > Double.parseDouble(nearbyServices.get(i).getLatitude())){
					latDiff = Double.parseDouble(latitude) - Double.parseDouble(nearbyServices.get(i).getLatitude());
				}
				else {
					latDiff = Double.parseDouble(nearbyServices.get(i).getLatitude()) - Double.parseDouble(latitude);
				}
				
				/* Get the longitude difference */
				if(Double.parseDouble(longitude) > Double.parseDouble(nearbyServices.get(i).getLongitude())){
					lngDiff = Double.parseDouble(longitude) - Double.parseDouble(nearbyServices.get(i).getLongitude());
				}
				else {
					lngDiff = Double.parseDouble(nearbyServices.get(i).getLongitude()) - Double.parseDouble(longitude);
				}
				
				/* Add latitude difference and longitude difference to get overall difference */
				savedserviceDiff = latDiff + lngDiff;
				
				/* Get the latitude difference */
				if(Double.parseDouble(latitude) > Double.parseDouble(service.getLatitude())){
					latDiff = Double.parseDouble(latitude) - Double.parseDouble(service.getLatitude());
				}
				else {
					latDiff = Double.parseDouble(service.getLatitude()) - Double.parseDouble(latitude);
				}
				
				/* Get the longitude difference */
				if(Double.parseDouble(longitude) > Double.parseDouble(service.getLongitude())){
					lngDiff = Double.parseDouble(longitude) - Double.parseDouble(service.getLongitude());
				}
				else {
					lngDiff = Double.parseDouble(service.getLongitude()) - Double.parseDouble(longitude);
				}
				
				/* Add latitude difference and longitude difference to get overall difference */
				nextserviceDiff = latDiff + lngDiff;
				
				System.out.println("diff: "+nextserviceDiff+" "+savedserviceDiff);
				if(nextserviceDiff < savedserviceDiff){
					System.out.println("ADDING");
					nearbyServices.add(i, service);
					break;
				}
			}
			if(!nearbyServices.contains(service)){
				nearbyServices.add(service);
			}
		}
		
		System.out.println("Nearby servicees: "+nearbyServices.size()+nearbyServices);
		return nearbyServices;
	}
	
	@Override
	public List<Service> sortServicesByRelevance(List<Service> services, Service searchService){
		List<Service> serviceValues = new LinkedList<Service>();
		List<Integer> weightValues = new LinkedList<Integer>();
		
		System.out.println("BRANCHES SIZE: "+services.size());
		int weight = 0;
		Service service;
		for(int i = 0; i < services.size(); i++){
			service = services.get(i);
			weight = 0;
			
			if(service.getCategory().equalsIgnoreCase(searchService.getCategory())){
				weight += 5;
			}
			else if(!service.getCategory().equals("") && service.getCategory().toLowerCase().contains(searchService.getCategory().toLowerCase())){
				weight += 1;
			}
			
			if(service.getDescription().equalsIgnoreCase(searchService.getDescription())){
				weight += 5;
			}
			else if(!service.getDescription().equals("") && service.getDescription().toLowerCase().contains(searchService.getDescription().toLowerCase())){
				weight += 1;
			}
			
			if(service.getName().equalsIgnoreCase(searchService.getName())){
				weight += 10;
			}
			else if(!service.getName().equals("") && service.getName().toLowerCase().contains(searchService.getName().toLowerCase())){
				weight += 5;
			}
			
			if(searchService.getPrice() != 0 && service.getPrice() == searchService.getPrice()){
				weight += 5;
			}
			
			if(service.getRetailerName().equalsIgnoreCase(searchService.getRetailerName())){
				weight += 5;
			}
			else if(!service.getRetailerName().equals("") && service.getRetailerName().toLowerCase().contains(searchService.getRetailerName().toLowerCase())){
				weight += 1;
			}
			
			if(serviceValues.size() == 0){
				serviceValues.add(service);
				weightValues.add(weight);
			}
			
			else {
				for(int j = 0; j < serviceValues.size(); j++){
					if(weight >= weightValues.get(j)){
						serviceValues.add(j, service);
						weightValues.add(j, weight);
						break;
					}
					if(j+1 == serviceValues.size()){
						serviceValues.add(j, service);
						weightValues.add(j, weight);
						break;
					}
				}
			}
		}
		
		System.out.println(serviceValues);
		System.out.println(weightValues);
		return serviceValues;
	}
	

	@Override
	public List<Service> getAllServices() {
		return serviceDao.getAllServices();
	}
	
	@Override
	public List<Service> findit(String param){
		return serviceDao.findit(param);
	}
	
	@Autowired
	public void setServiceDao(ServiceDao serviceDao){
		this.serviceDao = serviceDao;
	}
	@Autowired
	public void setDataConverter(DataConverter dataConverter){
		this.dataConverter = dataConverter;
	}
}
