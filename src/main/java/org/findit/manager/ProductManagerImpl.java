/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import java.util.LinkedList;
import java.util.List;

import org.findit.android.converter.DataConverter;
import org.findit.android.data.ProductData;
import org.findit.dao.ProductDao;
import org.findit.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class ProductManagerImpl implements ProductManager {
	private ProductDao productDao;
	private DataConverter dataConverter;
	
	@Override
	public Product updateProduct(Product product) {
		return productDao.updateProduct(product);
	}

	@Override
	public Product getProductById(int productId) {
		return productDao.getProductById(productId);
	}

	@Override
	public void removeProduct(Product product) {
		productDao.removeProduct(product);
	}
	
	@Override
	public List<Product> locateProduct(String name, String description, String category, String longitude, String latitude) {
		return productDao.locateProduct(name, category, description, longitude, latitude);
	}
	
	@Override
	public List<Product> getProducts(Product product, String match, String sort, String longitude, String latitude) {
		List<Product> products = productDao.getProducts(product, match, sort);
		if(sort.equals("location") && products.size() > 0){
			List<ProductData> productData = sortProductsByLocation(dataConverter.convertProduct(products), longitude, latitude);
			return dataConverter.convertProductData(productData);
		}
		if(sort.equals("relevance") && products.size() > 0){
			return sortProductsByRelevance(products, product);
		}
		return products;
	}

	@Override
	public List<ProductData> sortProductsByLocation(List<ProductData> products, String longitude, String latitude) {
		List<ProductData> nearbyProducts = new LinkedList<ProductData>();
		
		System.out.println("productes: "+products.size()+products);
		nearbyProducts.add(products.remove(0));
		double latDiff;
		double lngDiff;
		double savedproductDiff;
		double nextproductDiff;
		for(ProductData product : products){
			for(int i = 0; i < nearbyProducts.size(); i++){
				/* Get the latitude difference */
				if(Double.parseDouble(latitude) > Double.parseDouble(nearbyProducts.get(i).getLatitude())){
					latDiff = Double.parseDouble(latitude) - Double.parseDouble(nearbyProducts.get(i).getLatitude());
				}
				else {
					latDiff = Double.parseDouble(nearbyProducts.get(i).getLatitude()) - Double.parseDouble(latitude);
				}
				
				/* Get the longitude difference */
				if(Double.parseDouble(longitude) > Double.parseDouble(nearbyProducts.get(i).getLongitude())){
					lngDiff = Double.parseDouble(longitude) - Double.parseDouble(nearbyProducts.get(i).getLongitude());
				}
				else {
					lngDiff = Double.parseDouble(nearbyProducts.get(i).getLongitude()) - Double.parseDouble(longitude);
				}
				
				/* Add latitude difference and longitude difference to get overall difference */
				savedproductDiff = latDiff + lngDiff;
				
				/* Get the latitude difference */
				if(Double.parseDouble(latitude) > Double.parseDouble(product.getLatitude())){
					latDiff = Double.parseDouble(latitude) - Double.parseDouble(product.getLatitude());
				}
				else {
					latDiff = Double.parseDouble(product.getLatitude()) - Double.parseDouble(latitude);
				}
				
				/* Get the longitude difference */
				if(Double.parseDouble(longitude) > Double.parseDouble(product.getLongitude())){
					lngDiff = Double.parseDouble(longitude) - Double.parseDouble(product.getLongitude());
				}
				else {
					lngDiff = Double.parseDouble(product.getLongitude()) - Double.parseDouble(longitude);
				}
				
				/* Add latitude difference and longitude difference to get overall difference */
				nextproductDiff = latDiff + lngDiff;
				
				System.out.println("diff: "+nextproductDiff+" "+savedproductDiff);
				if(nextproductDiff < savedproductDiff){
					System.out.println("ADDING");
					nearbyProducts.add(i, product);
					break;
				}
			}
			if(!nearbyProducts.contains(product)){
				nearbyProducts.add(product);
			}
		}
		
		System.out.println("Nearby productes: "+nearbyProducts.size()+nearbyProducts);
		return nearbyProducts;
	}

	@Override
	public List<Product> getAllProducts() {
		return productDao.getAllProducts();
	}
	
	@Override
	public List<Product> sortProductsByRelevance(List<Product> products, Product searchProduct){
		List<Product> productValues = new LinkedList<Product>();
		List<Integer> weightValues = new LinkedList<Integer>();
		
		System.out.println("BRANCHES SIZE: "+products.size());
		int weight = 0;
		Product product;
		for(int i = 0; i < products.size(); i++){
			product = products.get(i);
			weight = 0;
			
			if(product.getCategory().equalsIgnoreCase(searchProduct.getCategory())){
				weight += 5;
			}
			else if(!product.getCategory().equals("") && product.getCategory().toLowerCase().contains(searchProduct.getCategory().toLowerCase())){
				weight += 1;
			}
			
			if(product.getDescription().equalsIgnoreCase(searchProduct.getDescription())){
				weight += 5;
			}
			else if(!product.getDescription().equals("") && product.getDescription().toLowerCase().contains(searchProduct.getDescription().toLowerCase())){
				weight += 1;
			}
			
			if(product.getName().equalsIgnoreCase(searchProduct.getName())){
				weight += 10;
			}
			else if(!product.getName().equals("") && product.getName().toLowerCase().contains(searchProduct.getName().toLowerCase())){
				weight += 5;
			}
			
			if(searchProduct.getPrice() != 0 && product.getPrice() == searchProduct.getPrice()){
				weight += 5;
			}
			
			if(product.getRetailerName().equalsIgnoreCase(searchProduct.getRetailerName())){
				weight += 5;
			}
			else if(!product.getRetailerName().equals("") && product.getRetailerName().toLowerCase().contains(searchProduct.getRetailerName().toLowerCase())){
				weight += 1;
			}
			
			if(productValues.size() == 0){
				productValues.add(product);
				weightValues.add(weight);
			}
			
			else {
				for(int j = 0; j < productValues.size(); j++){
					if(weight >= weightValues.get(j)){
						productValues.add(j, product);
						weightValues.add(j, weight);
						break;
					}
					if(j+1 == productValues.size()){
						productValues.add(j, product);
						weightValues.add(j, weight);
						break;
					}
				}
			}
		}
		
		System.out.println(productValues);
		System.out.println(weightValues);
		return productValues;
	}
	
	@Override
	public List<Product> findit(String param){
		return productDao.findit(param);
	}
	
	@Autowired
	public void setProductDao(ProductDao productDao){
		this.productDao = productDao;
	}
	@Autowired
	public void setDataConverter(DataConverter dataConverter){
		this.dataConverter = dataConverter;
	}
}
