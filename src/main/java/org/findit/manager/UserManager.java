package org.findit.manager;

import org.findit.model.User;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserManager {
	public User getUserByUsername(String username);
	public User updateUser(User user);
	public boolean exists(String username);
	public UserDetails loadUserByUsername(String username);
}
