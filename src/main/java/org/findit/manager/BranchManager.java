/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import java.util.List;

import org.findit.model.Branch;

public interface BranchManager {
	public Branch updateBranch(Branch branch);
	public Branch getBranchById(int branchId);
	public void removeBranch(Branch branch);
	public List<Branch> locateBranch(String name, String location, String retailer, String longitude, String latitude);
	public List<Branch> getBranchesByRetailerUsername(String retailerUsername);
	public List<Branch> whatsAroundMe(String longitude, String latitude);
	public List<Branch> getAllBranches();
	public List<Branch> sortBranchesByLocation(List<Branch> branches, String longitude, String latitude);
	public List<Branch> getBranchesByRetailerFirstLetter(String beginsWith);
	public List<Branch> getBranches(Branch branch, String match, String sort);
	public List<Branch> sortBranchesByRelevance(List<Branch> branches, Branch searchBranch);
	public List<Branch> findit(String param);
}
