/**
 * 
 * @author Anthony Murphy
 */

package org.findit.manager;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserAuthentication implements Authentication {
	private static final long serialVersionUID = 1L;
	
	private UserDetails userDetails;
	private boolean authenticated = true;
	
	public UserAuthentication(UserDetails userDetails){
		this.userDetails = userDetails;
	}
	
	@Override
	public String getName() {
		return userDetails.getUsername();
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public Object getCredentials() {
		return userDetails.getPassword();
	}

	/**
	 * Not Used
	 */
	@Override
	public Object getDetails() {
		return userDetails.isEnabled();
	}

	@Override
	public Object getPrincipal() {
		return userDetails;
	}

	@Override
	public boolean isAuthenticated() {
		return authenticated;
	}

	@Override
	public void setAuthenticated(boolean authenticated) throws IllegalArgumentException {
		if(authenticated == false){
			this.authenticated = authenticated;
		}
		else{
			throw new IllegalArgumentException();
		}
	}
}
