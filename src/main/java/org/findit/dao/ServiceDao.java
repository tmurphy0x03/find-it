/**
 *
 * @author Anthony Murphy
 */

package org.findit.dao;

import java.util.List;

import org.findit.model.Service;

public interface ServiceDao {
	public Service updateService(Service service);
	public Service getServiceById(int serviceId);
	public void removeService(Service service);
	public List<Service> locateService(String name, String description, String category, String longitude, String latitude);
	public List<Service> getAllServices();
	public List<Service> getServices(Service service, String match, String sort);
	public List<Service> findit(String param);
}
