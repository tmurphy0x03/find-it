package org.findit.dao;

import java.util.List;

import org.findit.model.User;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface UserDao {
	public User getUserByUsername(String username);
	public User updateUser(User user);
	public boolean exists(String username);
	public int size();
	List<User> getAllUsers();
}
