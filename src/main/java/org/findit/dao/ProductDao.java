/**
 *
 * @author Anthony Murphy
 */

package org.findit.dao;

import java.util.List;

import org.findit.model.Product;

public interface ProductDao {
	public Product updateProduct(Product product);
	public Product getProductById(int productId);
	public void removeProduct(Product product);
	public List<Product> locateProduct(String name, String category, String description, String longitude, String latitude);
	public List<Product> getAllProducts();
	public List<Product> getProducts(Product product, String match, String sort);
	public List<Product> findit(String param);
}
