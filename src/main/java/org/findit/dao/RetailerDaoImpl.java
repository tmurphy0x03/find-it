/**
 *
 * @author Anthony Murphy
 */

package org.findit.dao;

import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.findit.model.Retailer;
import org.springframework.stereotype.Repository;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

@Repository("retailerDao")
public class RetailerDaoImpl implements RetailerDao {

	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	@Override
	public Retailer getRetailerByUsername(String username) {
		Query query = em.createQuery("FROM Retailer WHERE username = :username");
		query.setParameter("username", username);
		if(query.getResultList().size() == 0){
			return null;
		}
		return (Retailer) query.getSingleResult();
	}

	@Transactional(readOnly = false)
	@Rollback(false)
	@Override
	public Retailer updateRetailer(Retailer retailer) {
		em.merge(retailer);
		em.flush();
		System.out.println("##############################################"+retailer+"##############################");
		return retailer;
	}

	@Override
	public boolean exists(String username) {
		Query query = em.createQuery("FROM Retailer WHERE username = :username");
		query.setParameter("username", username);
		if(query.getResultList().size() > 0){
			return true;
		}
		return false;
	}

	@Override
	public int size() {
		return getAllRetailers().size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Retailer> getAllRetailers() {
		Query query = em.createQuery("FROM Retailer");
		return query.getResultList();
	}
	
	public void setEm(EntityManager em){
		this.em = em;
	}

}
