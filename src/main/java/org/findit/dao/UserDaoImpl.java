package org.findit.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.findit.model.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("userDao")
@Transactional
public class UserDaoImpl implements UserDao {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public User getUserByUsername(String username) {
		Query query = em.createQuery("FROM User WHERE username = :username");
		query.setParameter("username", username);
		if(query.getResultList().size() == 0){
			return null;
		}
		return (User) query.getSingleResult();
	}

	@Transactional(readOnly = false)
	@Override
	public User updateUser(User user) {
		em.merge(user);
		em.flush();
		return user;
	}

	/**
	 * Checks to see if a username already exists
	 */
	@Override
	public boolean exists(String username) {
		Query query = em.createQuery("FROM User WHERE username = :username");
		query.setParameter("username", username);
		if(query.getResultList().size() > 0){
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllUsers(){
		Query query = em.createQuery("FROM User");
		return query.getResultList();
	}
	
	@Override
	public int size(){
		return getAllUsers().size();
	}
}
