/**
 *
 * @author Anthony Murphy
 */

package org.findit.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.findit.model.Service;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("serviceDao")
@Transactional
public class ServiceDaoImpl implements ServiceDao {

	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	@Override
	public Service updateService(Service service) {
		em.merge(service);
		em.flush();
		return service;
	}

	@Override
	public Service getServiceById(int id) {
		Query query = em.createQuery("FROM Service WHERE id = :id");
		query.setParameter("id", id);
		return (Service) query.getSingleResult();
	}

	@Override
	public void removeService(Service service) {
		em.remove(service);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Service> locateService(String name, String description, String category, String longitude, String latitude) {
		String queryString = "FROM Service";
		if(!name.equals("")){
			queryString += " WHERE lower(name) LIKE lower(:name)";
		}
		if(!category.equalsIgnoreCase("ALL")){
			if(!name.equals("")){
				queryString += " AND lower(category) LIKE lower(:category)";
			}
			else {
				queryString += " WHERE lower(category) LIKE lower(:category)";
			}
		}
		if(!description.equals("")){
			if(!name.equals("") || !category.equals("")){
				queryString += " AND lower(description) LIKE lower(:description)";
			}
			else {
				queryString += " WHERE lower(description) LIKE lower(:description)";
			}
		}
		
		Query query = em.createQuery(queryString);
		if(!name.equals("")){
			query.setParameter("name", "%"+name+"%");
		}
		if(!category.equalsIgnoreCase("ALL")){
			query.setParameter("category", "%"+category+"%");
		}
		if(!description.equals("")){
			query.setParameter("description", "%"+description+"%");
		}
		
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Service> getAllServices() {
		String queryString = "FROM Service";
		System.out.println(queryString);
		Query query = em.createQuery(queryString);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Service> getServices(Service service, String match, String sort) {
		String queryString = "FROM Service";
		queryString += " WHERE lower(name) LIKE lower(:name)";
		queryString += " AND lower(category) LIKE lower(:category)";
		queryString += " AND lower(description) LIKE lower(:description)";
		if(service.getPrice() != 0){
			queryString += " AND lower(price) BETWEEN :price1 AND :price2";
		}
		queryString += " AND lower(retailerName) LIKE lower(:retailerName)";
		
		if(sort.equals("hits")){
			queryString += " ORDER BY hits DESC";
		}
		
		Query query = em.createQuery(queryString);
		if(match.equals("partial")){
			System.out.println("PARTIAL");
			query.setParameter("name", "%"+service.getName()+"%");
			query.setParameter("category", "%"+service.getCategory()+"%");
			query.setParameter("description", "%"+service.getDescription()+"%");
			if(service.getPrice() != 0){
				query.setParameter("price1", ""+(service.getPrice()-0.01)+"");
				query.setParameter("price2", ""+(service.getPrice()+0.01)+"");
			}
			query.setParameter("retailerName", "%"+service.getRetailerName()+"%");
		}
		else {
			if(service.getName().equals("")){
				query.setParameter("name", "%"+service.getName()+"%");
			}
			else {
				query.setParameter("name", service.getName());
			}
			if(service.getCategory().equals("")){
				query.setParameter("category", "%"+service.getCategory()+"%");
			}
			else {
				query.setParameter("category", service.getCategory());
			}
			if(service.getDescription().equals("")){
				query.setParameter("description", "%"+service.getDescription()+"%");
			}
			else {
				query.setParameter("description", service.getDescription());
			}
			if(service.getPrice() != 0){
				query.setParameter("price1", ""+(service.getPrice()-(service.getPrice()*.10))+"");
				query.setParameter("price2", ""+(service.getPrice()+(service.getPrice()*.10))+"");
			}
			if(service.getRetailerName().equals("")){
				query.setParameter("retailerName", "%"+service.getRetailerName()+"%");
			}
			else {
				query.setParameter("retailerName", service.getRetailerName());
			}
		}
		System.out.println("RESULT SET "+query.getResultList().size());
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Service> findit(String param){
		String queryString = "FROM Service";
		queryString += " WHERE lower(name) LIKE lower(:flexibleParam)";
		queryString += " OR lower(category) LIKE lower(:strictParam)";
		queryString += " OR lower(description) LIKE lower(:strictParam)";
		queryString += " OR lower(retailerName) LIKE lower(:strictParam)";
		
		Query query = em.createQuery(queryString);
		query.setParameter("strictParam", param);
		query.setParameter("flexibleParam", "%"+param+"%");
		
		return query.getResultList();
	}
}
