/**
 *
 * @author Anthony Murphy
 */

package org.findit.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.findit.model.Product;
import org.findit.model.Service;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("productDao")
@Transactional
public class ProductDaoImpl implements ProductDao {

	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	@Override
	public Product updateProduct(Product product) {
		em.merge(product);
		em.flush();
		return product;
	}

	@Override
	public Product getProductById(int id) {
		Query query = em.createQuery("FROM Product WHERE id = :id");
		query.setParameter("id", id);
		return (Product) query.getSingleResult();
	}

	@Override
	public void removeProduct(Product product) {
		em.remove(product);
	}

	@Override
	public List<Product> locateProduct(String name, String category, String description, String longitude, String latitude) {
		String queryString = "FROM Product";
		if(!name.equals("")){
			queryString += " WHERE lower(name) LIKE lower(:name)";
		}
		if(!category.equalsIgnoreCase("ALL")){
			if(!name.equals("")){
				queryString += " AND lower(category) LIKE lower(:category)";
			}
			else {
				queryString += " WHERE lower(category) LIKE lower(:category)";
			}
		}
		if(!description.equals("")){
			if(!name.equals("") || !category.equals("")){
				queryString += " AND lower(description) LIKE lower(:description)";
			}
			else {
				queryString += " WHERE lower(description) LIKE lower(:description)";
			}
		}
		
		Query query = em.createQuery(queryString);
		System.out.println(queryString);
		if(!name.equals("")){
			query.setParameter("name", "%"+name+"%");
		}
		if(!category.equalsIgnoreCase("ALL")){
			query.setParameter("category", "%"+category+"%");
		}
		if(!description.equals("")){
			query.setParameter("description", "%"+description+"%");
		}
		
		return query.getResultList();
	}

	@Override
	public List<Product> getAllProducts() {
		String queryString = "FROM Product";
		System.out.println(queryString);
		Query query = em.createQuery(queryString);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getProducts(Product product, String match, String sort) {
		String queryString = "FROM Product";
		queryString += " WHERE lower(name) LIKE lower(:name)";
		queryString += " AND lower(category) LIKE lower(:category)";
		queryString += " AND lower(description) LIKE lower(:description)";
		if(product.getPrice() != 0){
			queryString += " AND lower(price) BETWEEN :price1 AND :price2";
		}
		queryString += " AND lower(retailerName) LIKE lower(:retailerName)";
		
		if(sort.equals("hits")){
			queryString += " ORDER BY hits DESC";
		}
		
		Query query = em.createQuery(queryString);
		if(match.equals("partial")){
			System.out.println("PARTIAL");
			query.setParameter("name", "%"+product.getName()+"%");
			query.setParameter("category", "%"+product.getCategory()+"%");
			query.setParameter("description", "%"+product.getDescription()+"%");
			if(product.getPrice() != 0){
				query.setParameter("price1", ""+(product.getPrice()-0.01)+"");
				query.setParameter("price2", ""+(product.getPrice()+0.01)+"");
			}
			query.setParameter("retailerName", "%"+product.getRetailerName()+"%");
		}
		else {
			if(product.getName().equals("")){
				query.setParameter("name", "%"+product.getName()+"%");
			}
			else {
				query.setParameter("name", product.getName());
			}
			if(product.getCategory().equals("")){
				query.setParameter("category", "%"+product.getCategory()+"%");
			}
			else {
				query.setParameter("category", product.getCategory());
			}
			if(product.getDescription().equals("")){
				query.setParameter("description", "%"+product.getDescription()+"%");
			}
			else {
				query.setParameter("description", product.getDescription());
			}
			if(product.getPrice() != 0){
				query.setParameter("price1", ""+(product.getPrice()-(product.getPrice()*.10))+"");
				query.setParameter("price2", ""+(product.getPrice()+(product.getPrice()*.10))+"");
			}
			if(product.getRetailerName().equals("")){
				query.setParameter("retailerName", "%"+product.getRetailerName()+"%");
			}
			else {
				query.setParameter("retailerName", product.getRetailerName());
			}
		}
		System.out.println("RESULT SET "+query.getResultList().size());
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> findit(String param){
		String queryString = "FROM Product";
		queryString += " WHERE lower(name) LIKE lower(:flexibleParam)";
		queryString += " OR lower(category) LIKE lower(:strictParam)";
		queryString += " OR lower(description) LIKE lower(:strictParam)";
		queryString += " OR lower(retailerName) LIKE lower(:strictParam)";
		
		Query query = em.createQuery(queryString);
		query.setParameter("strictParam", param);
		query.setParameter("flexibleParam", "%"+param+"%");
		
		return query.getResultList();
	}
}
