/**
 *
 * @author Anthony Murphy
 */

package org.findit.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.findit.model.Retailer;

public interface RetailerDao {
	public Retailer getRetailerByUsername(String username);
	public Retailer updateRetailer(Retailer retailer);
	public boolean exists(String username);
	public int size();
	public List<Retailer> getAllRetailers();
	public void setEm(EntityManager entityManager);
}
