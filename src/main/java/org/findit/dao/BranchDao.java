/**
 *
 * @author Anthony Murphy
 */

package org.findit.dao;

import java.util.List;

import org.findit.model.Branch;

public interface BranchDao {
	public Branch updateBranch(Branch branch);
	public Branch getBranchById(int id);
	public void removeBranch(Branch branch);
	public List<Branch> locateBranch(String name, String location, String retailer, String longitude, String latitude);
	public List<Branch> getBranchesByRetailerUsername(String retailerUsername);
	public List<Branch> getAllBranches();
	public List<Branch> getBranchesByRetailerFirstLetter(char beginsWith);
	public List<Branch> getBranches(Branch branch, String match, String sort);
	public List<Branch> findit(String param);
}
