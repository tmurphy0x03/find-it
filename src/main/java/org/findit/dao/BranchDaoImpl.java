/**
 *
 * @author Anthony Murphy
 */

package org.findit.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.findit.model.Branch;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("branchDao")
@Transactional
public class BranchDaoImpl implements BranchDao {

	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	@Override
	public Branch updateBranch(Branch branch) {
		em.merge(branch);
		em.flush();
		return branch;
	}

	@Override
	public Branch getBranchById(int id) {
		Query query = em.createQuery("FROM Branch WHERE id = :id");
		query.setParameter("id", id);
		return (Branch) query.getSingleResult();
	}

	@Override
	public void removeBranch(Branch branch) {
		em.remove(branch);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Branch> locateBranch(String name, String location, String retailerName, String longitude, String latitude) {
		//TODO: Ignore case in search
		
		String queryString = "FROM Branch";
		if(!name.equals("")){
			queryString += " WHERE lower(name) LIKE lower(:name)";
		}
		if(!location.equals("")){
			if(!name.equals("")){
				queryString += " AND lower(location) LIKE lower(:location)";
			}
			else {
				queryString += " WHERE lower(location) LIKE lower(:location)";
			}
		}
		if(!retailerName.equals("")){
			if(!name.equals("") || !retailerName.equals("")){
				queryString += " AND lower(retailerName) LIKE lower(:retailerName)";
			}
			else {
				queryString += " WHERE lower(retailerName) LIKE lower(:retailerName)";
			}
		}
		
		Query query = em.createQuery(queryString);
		if(!name.equals("")){
			query.setParameter("name", "%"+name+"%");
		}
		if(!location.equals("")){
			query.setParameter("location", "%"+location+"%");
		}
		if(!retailerName.equals("")){
			query.setParameter("retailerName", "%"+retailerName+"%");
		}
		
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Branch> getBranchesByRetailerUsername(String retailerUsername) {
		String queryString = "FROM Branch WHERE retailerusername = :retailerUsername";
		System.out.println(queryString);
		Query query = em.createQuery(queryString);
		query.setParameter("retailerUsername", retailerUsername);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Branch> getAllBranches(){
		String queryString = "FROM Branch";
		System.out.println(queryString);
		Query query = em.createQuery(queryString);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Branch> getBranchesByRetailerFirstLetter(char beginsWith) {
		String queryString = "FROM Branch WHERE lower(retailername) LIKE :beginsWith";
		System.out.println(queryString);
		Query query = em.createQuery(queryString);
		query.setParameter("beginsWith", beginsWith+"%");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Branch> getBranches(Branch branch, String match, String sort) {
		String queryString = "FROM Branch";
		queryString += " WHERE lower(name) LIKE lower(:name)";
		queryString += " AND lower(property) LIKE lower(:property)";
		queryString += " AND lower(address1) LIKE lower(:address1)";
		queryString += " AND lower(address2) LIKE lower(:address2)";
		queryString += " AND lower(address3) LIKE lower(:address3)";
		queryString += " AND lower(location) LIKE lower(:location)";
		queryString += " AND lower(region) LIKE lower(:region)";
		queryString += " AND lower(postcode) LIKE lower(:postcode)";
		queryString += " AND lower(country) LIKE lower(:country)";
		if(sort.equals("hits")){
			queryString += " ORDER BY hits DESC";
		}
		
		Query query = em.createQuery(queryString);
		if(match.equals("partial")){
			query.setParameter("name", "%"+branch.getName()+"%");
			query.setParameter("property", "%"+branch.getProperty()+"%");
			query.setParameter("address1", "%"+branch.getAddress1()+"%");
			query.setParameter("address2", "%"+branch.getAddress2()+"%");
			query.setParameter("address3", "%"+branch.getAddress3()+"%");
			query.setParameter("location", "%"+branch.getLocation()+"%");
			query.setParameter("region", "%"+branch.getRegion()+"%");
			query.setParameter("postcode", "%"+branch.getPostcode()+"%");
			query.setParameter("country", "%"+branch.getCountry()+"%");
		}
		else {
			if(branch.getName().equals("")){
				query.setParameter("name", "%"+branch.getName()+"%");
			}
			else {
				query.setParameter("name", branch.getName());
			}
			if(branch.getProperty().equals("")){
				query.setParameter("property", "%"+branch.getProperty()+"%");
			}
			else {
				query.setParameter("property", branch.getProperty());
			}
			if(branch.getAddress1().equals("")){
				query.setParameter("address1", "%"+branch.getAddress1()+"%");
			}
			else {
				query.setParameter("address1", branch.getAddress1());
			}
			if(branch.getAddress2().equals("")){
				query.setParameter("address2", "%"+branch.getAddress2()+"%");
			}
			else {
				query.setParameter("address2", branch.getAddress2());
			}
			if(branch.getAddress3().equals("")){
				query.setParameter("address3", "%"+branch.getAddress3()+"%");
			}
			else {
				query.setParameter("address3", branch.getAddress3());
			}
			if(branch.getLocation().equals("")){
				query.setParameter("location", "%"+branch.getLocation()+"%");
			}
			else {
				query.setParameter("location", branch.getLocation());
			}
			if(branch.getRegion().equals("")){
				query.setParameter("region", "%"+branch.getRegion()+"%");
			}
			else {
				query.setParameter("region", branch.getRegion());
			}
			if(branch.getPostcode().equals("")){
				query.setParameter("postcode", "%"+branch.getPostcode()+"%");
			}
			else {
				query.setParameter("postcode", branch.getPostcode());
			}
			if(branch.getCountry().equals("")){
				query.setParameter("country", "%"+branch.getCountry()+"%");
			}
			else {
				query.setParameter("country", branch.getCountry());
			}
		}
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Branch> findit(String param){
		String queryString = "FROM Branch";
		queryString += " WHERE lower(name) LIKE lower(:flexibleParam)";
		queryString += " OR lower(property) LIKE lower(:strictParam)";
		queryString += " OR lower(address1) LIKE lower(:flexibleParam)";
		queryString += " OR lower(address2) LIKE lower(:flexibleParam)";
		queryString += " OR lower(address3) LIKE lower(:flexibleParam)";
		queryString += " OR lower(location) LIKE lower(:strictParam)";
		queryString += " OR lower(region) LIKE lower(:strictParam)";
		queryString += " OR lower(postcode) LIKE lower(:strictParam)";
		queryString += " OR lower(country) LIKE lower(:strictParam)";
		queryString += " OR lower(retailerName) LIKE lower(:strictParam)";
		
		Query query = em.createQuery(queryString);
		query.setParameter("strictParam", param);
		query.setParameter("flexibleParam", "%"+param+"%");
		
		return query.getResultList();
	}
}
