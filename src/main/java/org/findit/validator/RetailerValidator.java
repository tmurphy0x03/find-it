package org.findit.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.findit.manager.RetailerManager;
import org.findit.model.Retailer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class RetailerValidator implements Validator {
	private RetailerManager retailerManager;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		Retailer retailer = (Retailer) target;

		if(retailer.getName() == null || retailer.getName().equals("")){
			errors.rejectValue("name", "requiredField");
		}
		if(retailer.getEmail() == null || retailer.getEmail().equals("")){
			errors.rejectValue("email", "requiredField");
		}
		if(retailer.getUsername() == null || retailer.getUsername().equals("")){
			errors.rejectValue("username", "requiredField");
		}
		if(retailer.getPassword() == null || retailer.getPassword().equals("")){
			errors.rejectValue("password", "requiredField");
		}
		if(retailer.getConfirmPassword() == null || retailer.getConfirmPassword().equals("")){
			errors.rejectValue("confirmPassword", "requiredField");
		}
		
		/* Validates the email address */
		String emailRegex = "^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		Pattern pattern = Pattern.compile(emailRegex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(retailer.getEmail());
		if(!matcher.matches() && !retailer.getEmail().equals("")){
			errors.rejectValue("email", "email.invalid");
		}
		
		/* Checks if the username already exists */
		if(retailerManager.exists(retailer.getUsername())){
			errors.rejectValue("username", "username.exists");
		}
		
		/* Checks the password was entered correctly */
		if(retailer.getPassword().length() < 5 && retailer.getPassword() != null){
			errors.rejectValue("password", "password.tooshort");
		}
		if(!retailer.getPassword().equals(retailer.getConfirmPassword()) && retailer.getPassword() != null 
				&& !retailer.getPassword().equals("")){
			errors.rejectValue("password", "password.notEqual");
			errors.rejectValue("confirmPassword", "password.notEqual");
		}
	}

	@Autowired
	public void setRetailerManager(RetailerManager retailerManager) {
		this.retailerManager = retailerManager;
	}
}
