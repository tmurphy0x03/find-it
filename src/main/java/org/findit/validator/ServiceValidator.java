/**
 *
 * @author Anthony Murphy
 */

package org.findit.validator;

import org.findit.model.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ServiceValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		Service service = (Service) target;
		
		if(service.getName() == null || service.getName().equals("")){
			errors.rejectValue("name", "requiredField");
		}
		if(service.getCategory() == null || service.getCategory().equals("")){
			errors.rejectValue("category", "requiredField");
		}
		if(service.getDescription() == null || service.getDescription().equals("")){
			errors.rejectValue("description", "requiredField");
		}
		if(service.getBranchNames().size() == 0){
			errors.rejectValue("branchNames", "noBranches");
		}
		/* Check to see if a Branch was selected */
		else {
			Boolean empty = true;
			for(int i = 0; i < service.getBranchNamesValues().size(); i++){
				if(service.getBranchNamesValues().get(i).equals("true")){
					empty = false;
				}
			}
			if(empty == true){
				errors.rejectValue("branchNames", "noBranches");
			}
		}
	}
}
