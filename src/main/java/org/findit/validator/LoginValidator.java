package org.findit.validator;

import org.findit.manager.RetailerManager;
import org.findit.manager.UserConverter;
import org.findit.manager.UserManager;
import org.findit.model.Retailer;
import org.findit.model.User;
import org.jasypt.util.text.TextEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class LoginValidator implements Validator {
	private UserManager userManager;
	private RetailerManager retailerManager;
	private TextEncryptor basicTextEncryptor;
	private UserConverter userConverter;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		User user = (User) target;
		Retailer retailer = userConverter.convert(user);
		User validUser = userManager.getUserByUsername(user.getUsername());
		Retailer validRetailer = retailerManager.getRetailerByUsername(retailer.getUsername());
		
		if(user.getUsername() == null || user.getUsername().equals("")){
			errors.rejectValue("username", "requiredField");
		}
		if(user.getPassword() == null || user.getPassword().equals("")){
			errors.rejectValue("password", "requiredField");
		}
		if(!user.getUsername().equals("") && validUser == null && !retailer.getUsername().equals("") && validRetailer == null){
			errors.rejectValue("username", "username.notexists");
		}
		else if(validUser!= null && !user.getPassword().equals(basicTextEncryptor.decrypt(validUser.getPassword()))) {
			errors.rejectValue("password", "password.invalid");
		}
		else if(validRetailer!= null && !retailer.getPassword().equals(basicTextEncryptor.decrypt(validRetailer.getPassword()))) {
			errors.rejectValue("password", "password.invalid");
		}
	}

	@Autowired
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
	
	@Autowired
	public void setRetailerManager(RetailerManager retailerManager) {
		this.retailerManager = retailerManager;
	}
	
	@Autowired
	public void setUserConverter(UserConverter userConverter) {
		this.userConverter = userConverter;
	}
	
	@Autowired
	public void setBasicTextEncryptor(TextEncryptor basicTextEncryptor){
		this.basicTextEncryptor = basicTextEncryptor;
	}
}
