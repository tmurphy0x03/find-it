/**
 *
 * @author Anthony Murphy
 */

package org.findit.validator;

import org.findit.model.Product;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ProductValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		Product product = (Product) target;
		
		if(product.getName() == null || product.getName().equals("")){
			errors.rejectValue("name", "requiredField");
		}
		if(product.getCategory() == null || product.getCategory().equals("")){
			errors.rejectValue("category", "requiredField");
		}
		if(product.getDescription() == null || product.getDescription().equals("")){
			errors.rejectValue("description", "requiredField");
		}
		if(product.getBranchNames().size() == 0){
			errors.rejectValue("branchNames", "noBranches");
		}
		/* Check to see if a Branch was selected */
		else {
			Boolean empty = true;
			for(int i = 0; i < product.getBranchNamesValues().size(); i++){
				if(product.getBranchNamesValues().get(i).equals("true")){
					empty = false;
				}
			}
			if(empty == true){
				errors.rejectValue("branchNames", "noBranches");
			}
		}
	}
}
