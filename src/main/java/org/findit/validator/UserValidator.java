package org.findit.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.findit.manager.UserManager;
import org.findit.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class UserValidator implements Validator {
	private UserManager userManager;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		User user = (User) target;

		if(user.getFirstName() == null || user.getFirstName().equals("")){
			errors.rejectValue("firstName", "requiredField");
		}
		if(user.getSurname() == null || user.getSurname().equals("")){
			errors.rejectValue("surname", "requiredField");
		}
		if(user.getEmail() == null || user.getEmail().equals("")){
			errors.rejectValue("email", "requiredField");
		}
		if(user.getUsername() == null || user.getUsername().equals("")){
			errors.rejectValue("username", "requiredField");
		}
		if(user.getPassword() == null || user.getPassword().equals("")){
			errors.rejectValue("password", "requiredField");
		}
		if(user.getConfirmPassword() == null || user.getConfirmPassword().equals("")){
			errors.rejectValue("confirmPassword", "requiredField");
		}
		
		/* Validates the email address */
		String emailRegex = "^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		Pattern pattern = Pattern.compile(emailRegex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(user.getEmail());
		if(!matcher.matches() && !user.getEmail().equals("")){
			errors.rejectValue("email", "email.invalid");
		}
		
		/* Checks if the username already exists */
		if(userManager.exists(user.getUsername())){
			errors.rejectValue("username", "username.exists");
		}
		
		/* Checks the password was entered correctly */
		if(user.getPassword().length() < 5 && user.getPassword() != null){
			errors.rejectValue("password", "password.tooshort");
		}
		else if(!user.getPassword().equals(user.getConfirmPassword()) && user.getPassword() != null 
				&& !user.getPassword().equals("")){
			errors.rejectValue("password", "password.notEqual");
			errors.rejectValue("confirmPassword", "password.notEqual");
		}
	}

	@Autowired
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
}
