/**
 *
 * @author Anthony Murphy
 */

package org.findit.validator;

import org.findit.model.Branch;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class BranchValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		Branch branch = (Branch) target;
		
		if(branch.getName() == null || branch.getName().equals("")){
			errors.rejectValue("name", "requiredField");
		}
		if(branch.getPhoneNumber() == null || branch.getPhoneNumber().equals("")){
			errors.rejectValue("phoneNumber", "requiredField");
		}
		if(branch.getProperty() == null || branch.getProperty().equals("")){
			errors.rejectValue("property", "requiredField");
		}
		if(branch.getAddress1() == null || branch.getAddress1().equals("")){
			errors.rejectValue("address1", "requiredField");
		}
		if(branch.getLocation() == null || branch.getLocation().equals("")){
			errors.rejectValue("location", "requiredField");
		}
		if(branch.getRegion() == null || branch.getRegion().equals("")){
			errors.rejectValue("region", "requiredField");
		}
		if(branch.getCountry() == null || branch.getCountry().equals("")){
			errors.rejectValue("country", "requiredField");
		}
		if(branch.getLatitude() == null || branch.getLatitude().equals("") 
				|| branch.getLongitude() == null || branch.getLongitude().equals("")){
			errors.rejectValue("latitude", "requiredLocation");
		}
	}
}
