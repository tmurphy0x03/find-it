package org.findit.controller;

import org.findit.manager.RetailerManager;
import org.findit.manager.UserAuthentication;
import org.findit.manager.UserManager;
import org.findit.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class LoginLogoutController {
	private Validator loginValidator;
	private UserManager userManager;
	private RetailerManager retailerManager;
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public ModelAndView getLogin(){
		ModelAndView modelAndView = new ModelAndView("login");
		User user = new User();
		modelAndView.addObject("user", user);
		return modelAndView;
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public ModelAndView login(@ModelAttribute("user") User user, BindingResult result){
		ModelAndView modelAndView;
		loginValidator.validate(user, result);
		
		if(result.hasErrors()){
			modelAndView = new ModelAndView("login");
			modelAndView.addObject("errors", result);
			return modelAndView;
		}
		
		/* Get UserDetails and Login */
		UserDetails userDetails = userManager.loadUserByUsername(user.getUsername());
		if(userDetails == null){
			userDetails = retailerManager.loadUserByUsername(user.getUsername());
		}
		SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(userDetails));
		
		return new ModelAndView(new RedirectView("index.html"));
	}
	
	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public ModelAndView logout(){
		/* Logout user */
		SecurityContextHolder.getContext().setAuthentication(null);
		return new ModelAndView(new RedirectView("index.html"));
	}
	
	@Autowired
	public void setLoginValidator(Validator loginValidator){
		this.loginValidator = loginValidator;
	}

	@Autowired
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
	
	@Autowired
	public void setRetailerManager(RetailerManager retailerManager) {
		this.retailerManager = retailerManager;
	}
}
