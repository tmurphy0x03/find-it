/**
 *
 * @author Anthony Murphy
 */

package org.findit.controller;

import java.util.Collection;
import java.util.List;

import org.findit.android.converter.DataConverter;
import org.findit.android.data.BranchData;
import org.findit.android.data.ProductData;
import org.findit.android.data.ServiceData;
import org.findit.manager.BranchManager;
import org.findit.manager.ProductManager;
import org.findit.manager.ServiceManager;
import org.findit.model.Branch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AndroidController {
	private ServiceManager serviceManager;
	private BranchManager branchManager;
	private ProductManager productManager;
	private DataConverter dataConverter;

	/**
	 * Returns the Android search query in JSON format
	 * @param name
	 * @param category
	 * @param description
	 * @param longitude
	 * @param latitude
	 * @return
	 */
	@RequestMapping(value="/productSearch", method=RequestMethod.GET)
	public @ResponseBody Collection<ProductData> productSearch(@RequestParam("name") String name, 
			@RequestParam("category") String category, 
			@RequestParam("description") String description,
			@RequestParam("longitude") String longitude,
			@RequestParam("latitude") String latitude) {
		System.out.println("Here");
		List<ProductData> products = dataConverter.convertProduct(productManager.locateProduct(name, description, category, longitude, latitude));
		System.out.println(products);
		if(products.size() > 0){
			products = productManager.sortProductsByLocation(products, longitude, latitude);
		}
		return products;
	}
	
	/**
	 * Returns the Android search query in JSON format
	 * @param name
	 * @param location
	 * @param retailerName
	 * @param longitude
	 * @param latitude
	 * @return
	 */
	@RequestMapping(value="/branchSearch", method=RequestMethod.GET)
	public @ResponseBody Collection<BranchData> branchSearch(@RequestParam("name") String name, 
			@RequestParam("location") String location, 
			@RequestParam("retailerName") String retailerName,
			@RequestParam("longitude") String longitude,
			@RequestParam("latitude") String latitude) {
		List<Branch> branches = branchManager.locateBranch(name, location, retailerName, longitude, latitude);
		System.out.println(branches.size()+" Branches found");
		return dataConverter.convertBranch(branchManager.sortBranchesByLocation(branches, longitude, latitude));
	}
	
	/**
	 * Returns the Android search query in JSON format
	 * @param name
	 * @param category
	 * @param description
	 * @param longitude
	 * @param latitude
	 * @return
	 */
	@RequestMapping(value="/serviceSearch", method=RequestMethod.GET)
	public @ResponseBody Collection<ServiceData> serviceSearch(@RequestParam("name") String name, 
			@RequestParam("category") String category, 
			@RequestParam("description") String description,
			@RequestParam("longitude") String longitude,
			@RequestParam("latitude") String latitude) {
		List<ServiceData> services = dataConverter.convertService(serviceManager.locateService(name, description, category, longitude, latitude));
		if(services.size() > 0){
			services = serviceManager.sortServicesByLocation(services, longitude, latitude);
		}
		System.out.println(services);
		return services;
	}
	
	@RequestMapping(value="/whatsAroundMe", method=RequestMethod.GET)
	public @ResponseBody Collection<BranchData> whatsAroundMe(@RequestParam("longitude") String longitude,
			@RequestParam("latitude") String latitude) {
		List<BranchData> branches = dataConverter.convertBranch(branchManager.whatsAroundMe(longitude, latitude));
		if(branches.size() > 25){
			branches = branches.subList(0, 25);
		}
		System.out.println(branches);
		return branches;
	}

	@Autowired
	public void setDataConverter(DataConverter dataConverter){
		this.dataConverter = dataConverter;
	}
	@Autowired
	public void setProductManager(ProductManager productManager){
		this.productManager = productManager;
	}
	@Autowired
	public void setBranchManager(BranchManager branchManager){
		this.branchManager = branchManager;
	}
	@Autowired
	public void setServiceManager(ServiceManager serviceManager){
		this.serviceManager = serviceManager;
	}
}
