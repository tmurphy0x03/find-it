/**
 *
 * @author Anthony Murphy
 */

package org.findit.controller;

import java.util.LinkedList;
import java.util.List;

import org.findit.manager.BranchManager;
import org.findit.manager.ProductManager;
import org.findit.manager.RetailerManager;
import org.findit.manager.UserTypeChecker;
import org.findit.model.Branch;
import org.findit.model.Product;
import org.findit.model.Retailer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class ProductsController {
	private UserTypeChecker userTypeChecker;
	private ProductManager productManager;
	private RetailerManager retailerManager;
	private Validator productValidator;
	private BranchManager branchManager;
	
	/**
	 * Assembles the branches page
	 * @return the branches view
	 */
	@RequestMapping(value="/products", method=RequestMethod.GET)
	public ModelAndView getProductsPage(){
		ModelAndView modelAndView = new ModelAndView("products");
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		List<Branch> branches = retailer.getBranches();
		List<Product> products = new LinkedList<Product>();
		for(Branch branch : branches){
			for(int i = 0; i < branch.getProducts().size(); i++){
				/* If the list already contains the Product, don't add it again */
				if(!products.contains(branch.getProducts().get(i))){
					products.add(branch.getProducts().get(i));
				}
			}
		}
		if(branches != null && branches.size() > 0){
			modelAndView.addObject("products", products);
		}
		else {
			modelAndView.addObject("products", null);
		}
		return modelAndView;
	}
	
	@RequestMapping(value="/addProduct", method=RequestMethod.GET)
	public ModelAndView getAddProduct(){
		ModelAndView modelAndView = new ModelAndView("addProduct");
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		Product product = new Product();
		product.setRetailerName(retailer.getName());
		product.setRetailerUsername(retailer.getUsername());
		List<Branch> branches = (List<Branch>) retailer.getBranches();
		if(retailer.getBranches().size() > 0){
			for(int i = 0; i < retailer.getBranches().size(); i++){
				product.getBranchNames().add(i, branches.get(i).getName());
				product.getBranchNamesValues().add(i, "false");
			}
		}
		else {
			return new ModelAndView(new RedirectView("index.html"));
		}
		modelAndView.addObject("action", "add");
		modelAndView.addObject("product", product);
		return modelAndView;
	}
	
	@RequestMapping(value="/addProduct", method=RequestMethod.POST)
	public ModelAndView addProduct(@ModelAttribute("product") Product product, BindingResult result){
		/* Ensure the user is logged in and is a retailer */
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		
		/* Validate the branch and return errors if errors exist */
		productValidator.validate(product, result);
		if(result.hasErrors()){
			ModelAndView modelAndView = new ModelAndView("addProduct");
			modelAndView.addObject("errors", result);
			modelAndView.addObject("action", "add");
			return modelAndView;
		}

		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		List<String> branchNames = product.getBranchNames();
		product.setBranchNames(null);
		List<String> branchNamesValues = product.getBranchNamesValues();
		product.setBranchNamesValues(null);
		/* Loop through all branches and set the branch names */
		int numberOfBranches = retailer.getBranches().size();
		for(int i = 0 ; i < numberOfBranches; i++){
			/* If the branch isn't selected in the Product, Remove it from the Product */
			if(branchNamesValues.get(i).equals("false")){
				branchNames.remove(i);
				branchNamesValues.remove(i);
				numberOfBranches--;
				i=i-1;
			}
		}
		
		List<Branch> branches = retailer.getBranches();
		product.setBranchNames(branchNames);
		product.setBranchNamesValues(branchNamesValues);
		/* Loop through all branches for the Retailer */
		for(Branch branch : branches){
			/* Loop through the branch names of the Product */
			for(int j = 0; j < product.getBranchNames().size(); j++){
				/* If the Branch equals the Branch name in the product, add the product to the branch */
				if(product.getBranchNames().get(j).equals(branch.getName())){
					branch.addProduct(product);
				}
			}
		}
		
		retailerManager.updateRetailer(retailer);
		return new ModelAndView(new RedirectView("products.html"));
	}
	
	@RequestMapping(value="/editProduct", method=RequestMethod.GET)
	public ModelAndView getEditProduct(@RequestParam("productId") int productId){
		ModelAndView modelAndView = new ModelAndView("addProduct");
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		Product product = productManager.getProductById(productId);
		if(retailer.getBranches().size() == 0){
			return new ModelAndView(new RedirectView("index.html"));
		}

		List<Branch> branches = new LinkedList<Branch>(retailer.getBranches());
		product.getBranchNames().clear();
		product.getBranchNamesValues().clear();
		
		Branch branch;
		for(int i = 0 ; i < branches.size() ; i++){
			branch = branches.get(i);
			if(!branch.getProducts().contains(product)){
				product.getBranchNames().add(branch.getName());
				product.getBranchNamesValues().add("false");
			}
			else {
				product.getBranchNames().add(branch.getName());
				product.getBranchNamesValues().add("true");
			}
		}
		
		modelAndView.addObject("action", "edit");
		modelAndView.addObject("product", product);
		return modelAndView;
	}
	
	@RequestMapping(value="/editProduct", method=RequestMethod.POST)
	public ModelAndView editProduct(@ModelAttribute("product") Product product, BindingResult result){
		/* Ensure the user is logged in and is a retailer */
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		
		/* Validate the branch and return errors if errors exist */
		productValidator.validate(product, result);
		if(result.hasErrors()){
			ModelAndView modelAndView = new ModelAndView("addProduct");
			modelAndView.addObject("errors", result);
			modelAndView.addObject("action", "edit");
			return modelAndView;
		}

		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		List<String> branchNames = new LinkedList<String>(product.getBranchNames());
		List<String> branchNamesValues = new LinkedList<String>(product.getBranchNamesValues());
		product.setBranchNames(null);
		product.setBranchNamesValues(null);
		/* Loop through all branches and set the branch names */
		int numberOfBranches = retailer.getBranches().size();
		for(int i = 0 ; i < numberOfBranches; i++){
			/* If the branch isn't selected in the Product, Remove it from the Product */
			if(branchNamesValues.get(i).equals("false")){
				branchNames.remove(i);
				branchNamesValues.remove(i);
				numberOfBranches--;
				i=i-1;
			}
		}
		
		List<Branch> branches = new LinkedList<Branch>(retailer.getBranches());
		product.setBranchNames(branchNames);
		product.setBranchNamesValues(branchNamesValues);
		
		/* Loop through all branches for the Retailer */
		for(Branch branch : branches){
			branch.removeProduct(product);
			/* Loop through the branch names of the Product */
			for(int j = 0; j < product.getBranchNames().size(); j++){
				/* If the Branch equals the Branch name in the product, add the product to the branch */
				if(product.getBranchNames().get(j).equals(branch.getName())){
					System.out.println("UPDATE PRODUCT"+branch);
					branch.updateProduct(product);
					retailer.updateBranch(branch);
				}
			}
		}
		
		System.out.println(retailer);
		retailerManager.updateRetailer(retailer);
		return new ModelAndView(new RedirectView("products.html"));
	}
	
	/**
	 * Assemble the viewProduct view
	 * @param index
	 * @return the viewProduct view
	 */
	@RequestMapping(value="/viewProduct", method=RequestMethod.GET)
	public ModelAndView getViewBranch(@RequestParam("productId") int productId){
		ModelAndView modelAndView = new ModelAndView("viewProduct");
		/* Get the Branch by the Id */
		Product product = productManager.getProductById(productId);
		System.out.println(product);
		Retailer retailer = retailerManager.getRetailerByUsername(product.getRetailerUsername());
		List<Branch> branches = new LinkedList<Branch>(retailer.getBranches());
		
		product.getBranchNames().clear();
		product.getBranchNamesValues().clear();
		Branch branch;
		for(int i = 0 ; i < branches.size() ; i++){
			branch = branches.get(i);
			if(!branch.getProducts().contains(product)){
				System.out.println("REMOVING");
				branches.remove(i);
				i--;
			}
			else {
				System.out.println("ELSE");
				product.setHits(product.getHits()+1);
				branch.updateProduct(product);
				retailer.updateBranch(branch);
				retailerManager.updateRetailer(retailer);
				product.getBranchNames().add(branch.getName());
				product.getBranchNamesValues().add("true");
			}
		}
		
		/* Add the Branch and RetailerName to the view */
		modelAndView.addObject("product", product);
		modelAndView.addObject("branches", branches);
		modelAndView.addObject("retailerName", product.getRetailerName());
		return modelAndView;
	}
	
	/**
	 * Removes a Product
	 * @param index
	 * @return a redirect view to the products view
	 */
	@RequestMapping(value="/removeProduct", method=RequestMethod.GET)
	public ModelAndView removeProduct(@RequestParam("productId") int productId){
		/* Ensure the user is logged in and is a retailer */
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		/* Remove the Product from the Retailer */
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		List<Branch> branches = new LinkedList<Branch>(retailer.getBranches());
		Product product = productManager.getProductById(productId);
		
		for(Branch branch : branches){
			System.out.println("branch.getProducts "+branch.getProducts()+" product "+product);
			if(branch.getProducts().contains(product)){
				branch.getProducts().remove(branch.getProducts().indexOf(product));
				retailer.updateBranch(branch);
				System.out.println("###############################REMOVING");
			}
		}
		
		System.out.println(retailer+"TETSTSTSTS");
		/* Update the Retailer */
		retailerManager.updateRetailer(retailer);
		return new ModelAndView(new RedirectView("products.html"));
	}
	
	@Autowired
	public void setUserTypeChecker(UserTypeChecker userTypeChecker){
		this.userTypeChecker = userTypeChecker;
	}
	@Autowired
	public void setProductManager(ProductManager productManager){
		this.productManager = productManager;
	}
	@Autowired
	public void setRetailerManager(RetailerManager retailerManager){
		this.retailerManager = retailerManager;
	}
	@Autowired
	public void setProductValidator(Validator productValidator){
		this.productValidator = productValidator;
	}
	@Autowired
	public void setBranchManager(BranchManager branchManager){
		this.branchManager = branchManager;
	}
}
