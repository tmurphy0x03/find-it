/**
 *
 * @author Anthony Murphy
 */

package org.findit.controller;

import java.util.LinkedList;
import java.util.List;

import org.findit.manager.BranchManager;
import org.findit.manager.ServiceManager;
import org.findit.manager.RetailerManager;
import org.findit.manager.UserTypeChecker;
import org.findit.model.Branch;
import org.findit.model.Service;
import org.findit.model.Retailer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class ServicesController {
	private UserTypeChecker userTypeChecker;
	private ServiceManager serviceManager;
	private RetailerManager retailerManager;
	private Validator serviceValidator;
	private BranchManager branchManager;
	
	/**
	 * Assembles the branches page
	 * @return the branches view
	 */
	@RequestMapping(value="/services", method=RequestMethod.GET)
	public ModelAndView getBranchesPage(){
		ModelAndView modelAndView = new ModelAndView("services");
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		List<Branch> branches = (List<Branch>) retailer.getBranches();
		List<Service> services = new LinkedList<Service>();
		for(Branch branch : branches){
			for(int i = 0; i < branch.getServices().size(); i++){
				/* If the list already contains the Service, don't add it again */
				if(!services.contains(branch.getServices().get(i))){
					services.add(branch.getServices().get(i));
				}
			}
		}
		if(branches != null && branches.size() > 0){
			modelAndView.addObject("services", services);
		}
		else {
			modelAndView.addObject("services", null);
		}
		return modelAndView;
	}
	
	@RequestMapping(value="/addService", method=RequestMethod.GET)
	public ModelAndView getAddService(){
		ModelAndView modelAndView = new ModelAndView("addService");
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		Service service = new Service();
		service.setRetailerName(retailer.getName());
		service.setRetailerUsername(retailer.getUsername());
		List<Branch> branches = (List<Branch>) retailer.getBranches();
		if(retailer.getBranches().size() > 0){
			for(int i = 0; i < retailer.getBranches().size(); i++){
				service.getBranchNames().add(i, branches.get(i).getName());
				service.getBranchNamesValues().add(i, "false");
			}
		}
		else {
			return new ModelAndView(new RedirectView("index.html"));
		}
		modelAndView.addObject("action", "add");
		modelAndView.addObject("service", service);
		return modelAndView;
	}
	
	@RequestMapping(value="/addService", method=RequestMethod.POST)
	public ModelAndView addService(@ModelAttribute("service") Service service, BindingResult result){
		/* Ensure the user is logged in and is a retailer */
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		
		/* Validate the branch and return errors if errors exist */
		serviceValidator.validate(service, result);
		if(result.hasErrors()){
			ModelAndView modelAndView = new ModelAndView("addService");
			modelAndView.addObject("errors", result);
			modelAndView.addObject("action", "add");
			return modelAndView;
		}

		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		List<String> branchNames = service.getBranchNames();
		service.setBranchNames(null);
		List<String> branchNamesValues = service.getBranchNamesValues();
		service.setBranchNamesValues(null);
		/* Loop through all branches and set the branch names */
		int numberOfBranches = retailer.getBranches().size();
		for(int i = 0 ; i < numberOfBranches; i++){
			/* If the branch isn't selected in the Service, Remove it from the Service */
			if(branchNamesValues.get(i).equals("false")){
				branchNames.remove(i);
				branchNamesValues.remove(i);
				numberOfBranches--;
				i=i-1;
			}
		}
		
		List<Branch> branches = retailer.getBranches();
		service.setBranchNames(branchNames);
		service.setBranchNamesValues(branchNamesValues);
		/* Loop through all branches for the Retailer */
		for(Branch branch : branches){
			/* Loop through the branch names of the Service */
			for(int j = 0; j < service.getBranchNames().size(); j++){
				/* If the Branch equals the Branch name in the service, add the service to the branch */
				if(service.getBranchNames().get(j).equals(branch.getName())){
					branch.addService(service);
				}
			}
		}
		
		retailerManager.updateRetailer(retailer);
		return new ModelAndView(new RedirectView("services.html"));
	}
	
	@RequestMapping(value="/editService", method=RequestMethod.GET)
	public ModelAndView getEditService(@RequestParam("serviceId") int serviceId){
		ModelAndView modelAndView = new ModelAndView("addService");
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		Service service = serviceManager.getServiceById(serviceId);
		if(retailer.getBranches().size() == 0){
			return new ModelAndView(new RedirectView("index.html"));
		}

		List<Branch> branches = new LinkedList<Branch>(retailer.getBranches());
		service.getBranchNames().clear();
		service.getBranchNamesValues().clear();
		
		Branch branch;
		for(int i = 0 ; i < branches.size() ; i++){
			branch = branches.get(i);
			if(!branch.getServices().contains(service)){
				service.getBranchNames().add(branch.getName());
				service.getBranchNamesValues().add("false");
			}
			else {
				service.getBranchNames().add(branch.getName());
				service.getBranchNamesValues().add("true");
			}
		}
		
		modelAndView.addObject("action", "edit");
		modelAndView.addObject("service", service);
		return modelAndView;
	}
	
	@RequestMapping(value="/editService", method=RequestMethod.POST)
	public ModelAndView editService(@ModelAttribute("service") Service service, BindingResult result){
		/* Ensure the user is logged in and is a retailer */
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		
		/* Validate the branch and return errors if errors exist */
		serviceValidator.validate(service, result);
		if(result.hasErrors()){
			ModelAndView modelAndView = new ModelAndView("addService");
			modelAndView.addObject("errors", result);
			modelAndView.addObject("action", "edit");
			return modelAndView;
		}

		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		List<String> branchNames = service.getBranchNames();
		service.setBranchNames(null);
		List<String> branchNamesValues = service.getBranchNamesValues();
		service.setBranchNamesValues(null);
		/* Loop through all branches and set the branch names */
		int numberOfBranches = retailer.getBranches().size();
		for(int i = 0 ; i < numberOfBranches; i++){
			/* If the branch isn't selected in the Service, Remove it from the Service */
			if(branchNamesValues.get(i).equals("false")){
				branchNames.remove(i);
				branchNamesValues.remove(i);
				numberOfBranches--;
				i=i-1;
			}
		}
		
		List<Branch> branches = retailer.getBranches();
		service.setBranchNames(branchNames);
		service.setBranchNamesValues(branchNamesValues);
		
		/* Loop through all branches for the Retailer */
		for(Branch branch : branches){
			branch.removeService(service);
			/* Loop through the branch names of the Service */
			for(int j = 0; j < service.getBranchNames().size(); j++){
				/* If the Branch equals the Branch name in the service, add the service to the branch */
				if(service.getBranchNames().get(j).equals(branch.getName())){
					branch.addService(service);
				}
			}
		}
		
		retailerManager.updateRetailer(retailer);
		return new ModelAndView(new RedirectView("services.html"));
	}
	
	/**
	 * Assemble the viewService view
	 * @param index
	 * @return the viewService view
	 */
	@RequestMapping(value="/viewService", method=RequestMethod.GET)
	public ModelAndView getViewBranch(@RequestParam("serviceId") int serviceId){
		ModelAndView modelAndView = new ModelAndView("viewService");
		/* Get the Branch by the Id */
		Service service = serviceManager.getServiceById(serviceId);
		Retailer retailer = retailerManager.getRetailerByUsername(service.getRetailerUsername());
		List<Branch> branches = new LinkedList<Branch>(retailer.getBranches());
		
		service.getBranchNames().clear();
		service.getBranchNamesValues().clear();
		Branch branch;
		for(int i = 0 ; i < branches.size() ; i++){
			branch = branches.get(i);
			if(!branch.getServices().contains(service)){
				branches.remove(i);
				i--;
			}
			else {
				service.setHits(service.getHits()+1);
				branch.updateService(service);
				retailer.updateBranch(branch);
				retailerManager.updateRetailer(retailer);
				service.getBranchNames().add(branch.getName());
				service.getBranchNamesValues().add("true");
			}
		}
		
		/* Add the Branch and RetailerName to the view */
		service.setHits(service.getHits()+1);
		serviceManager.updateService(service);
		modelAndView.addObject("service", service);
		modelAndView.addObject("branches", branches);
		modelAndView.addObject("retailerName", service.getRetailerName());
		return modelAndView;
	}
	
	/**
	 * Removes a Service
	 * @param index
	 * @return a redirect view to the services view
	 */
	@RequestMapping(value="/removeService", method=RequestMethod.GET)
	public ModelAndView removeService(@RequestParam("serviceId") int serviceId){
		/* Ensure the user is logged in and is a retailer */
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		/* Remove the Service from the Retailer */
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		List<Branch> branches = new LinkedList<Branch>(retailer.getBranches());
		Service service = serviceManager.getServiceById(serviceId);
		
		for(Branch branch : branches){
			if(branch.getServices().contains(service)){
				retailer.updateBranch(branch);
				branch.getServices().remove(branch.getServices().indexOf(service));
			}
		}
		
		/* Update the Retailer */
		retailerManager.updateRetailer(retailer);
		return new ModelAndView(new RedirectView("services.html"));
	}
	
	@Autowired
	public void setUserTypeChecker(UserTypeChecker userTypeChecker){
		this.userTypeChecker = userTypeChecker;
	}
	@Autowired
	public void setServiceManager(ServiceManager serviceManager){
		this.serviceManager = serviceManager;
	}
	@Autowired
	public void setRetailerManager(RetailerManager retailerManager){
		this.retailerManager = retailerManager;
	}
	@Autowired
	public void setServiceValidator(Validator serviceValidator){
		this.serviceValidator = serviceValidator;
	}
}
