package org.findit.controller;

import org.findit.mail.MailServer;
import org.findit.manager.RetailerManager;
import org.findit.manager.UserAuthentication;
import org.findit.manager.UserManager;
import org.findit.model.Retailer;
import org.findit.model.User;
import org.jasypt.util.text.BasicTextEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class CreateAccountController {
	private Validator userValidator;
	private BasicTextEncryptor basicTextEncryptor;
	private UserManager userManager;
	private Validator retailerValidator;
	private RetailerManager retailerManager;
	private MailServer mailServer;
	
	@RequestMapping(value="/createAccount", method=RequestMethod.GET)
	public ModelAndView getCreateAccountPage(){
		ModelAndView modelAndView = new ModelAndView("createAccount");
		User user = new User();
		modelAndView.addObject("action", "Create a Customer");
		modelAndView.addObject("user", user);
		return modelAndView;
	}
	
	@RequestMapping(value="/createAccount", method=RequestMethod.POST)
	public ModelAndView createAccount(@ModelAttribute("user") User user, BindingResult result){
		ModelAndView modelAndView; 
		
		userValidator.validate(user, result);
		if(result.hasErrors()){
			modelAndView = new ModelAndView("createAccount");
			modelAndView.addObject("errors", result);
			return modelAndView;
		}
		
		/* Encrypt Password & Add User to DB */
        user.setPassword(basicTextEncryptor.encrypt(user.getPassword()));
		userManager.updateUser(user);
		
		/* Get UserDetails and Login */
		UserDetails userDetails = userManager.loadUserByUsername(user.getUsername());
		SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(userDetails));
		
		return new ModelAndView(new RedirectView("index.html"));
	}
	
	@RequestMapping(value="/editAccount", method=RequestMethod.GET)
	public ModelAndView getEditAccountPage(){
		ModelAndView modelAndView = new ModelAndView("createAccount");
		if(SecurityContextHolder.getContext().getAuthentication() == null){
			return new ModelAndView(new RedirectView("index.html"));
		}
		User user = userManager.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		if(user == null){
			return new ModelAndView(new RedirectView("index.html"));
		}
		user.setPassword(basicTextEncryptor.decrypt(user.getPassword()));
		user.setConfirmPassword(user.getPassword());
		modelAndView.addObject("action", "Edit");
		modelAndView.addObject("user", user);
		return modelAndView;
	}
	
	@RequestMapping(value="/editRetailerAccount", method=RequestMethod.GET)
	public ModelAndView getEditRetailerAccountPage(){
		ModelAndView modelAndView = new ModelAndView("createRetailerAccount");
		if(SecurityContextHolder.getContext().getAuthentication() == null){
			return new ModelAndView(new RedirectView("index.html"));
		}
		Retailer retailer = retailerManager.getRetailerByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		if(retailer == null){
			return new ModelAndView(new RedirectView("index.html"));
		}
//		retailer.setPassword(basicTextEncryptor.decrypt(retailer.getPassword()));
//		retailer.setConfirmPassword(retailer.getPassword());
		modelAndView.addObject("retailer", retailer);
		modelAndView.addObject("action", "Edit");
		return modelAndView;
	}
	
	@RequestMapping(value="/createRetailerAccount", method=RequestMethod.GET)
	public ModelAndView getCreateRetailerAccountPage(){
		ModelAndView modelAndView = new ModelAndView("createRetailerAccount");
		Retailer retailer = new Retailer();
		modelAndView.addObject("retailer", retailer);
		modelAndView.addObject("action", "Create a Retailer");
		return modelAndView;
	}
	
	@RequestMapping(value="/createRetailerAccount", method=RequestMethod.POST)
	public ModelAndView createRetailerAccount(@ModelAttribute("retailer") Retailer retailer, BindingResult result){
		ModelAndView modelAndView; 
		
		retailerValidator.validate(retailer, result);
		if(result.hasErrors()){
			modelAndView = new ModelAndView("createRetailerAccount");
			modelAndView.addObject("errors", result);
			return modelAndView;
		}
		
		/* Encrypt Password & Add User to DB */
        retailer.setPassword(basicTextEncryptor.encrypt(retailer.getPassword()));
		retailerManager.updateRetailer(retailer);
		
		/* Get UserDetails and Login */
		UserDetails userDetails = retailerManager.loadUserByUsername(retailer.getUsername());
		SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(userDetails));
		
		return new ModelAndView(new RedirectView("index.html"));
	}
	
	@RequestMapping(value="/editRetailerAccount", method=RequestMethod.POST)
	public ModelAndView editRetailerAccount(@ModelAttribute("retailer") Retailer retailer, BindingResult result){
		ModelAndView modelAndView; 
		
		retailerValidator.validate(retailer, result);
		if(result.getErrorCount() > 1){
			modelAndView = new ModelAndView("createRetailerAccount");
			modelAndView.addObject("errors", result);
			modelAndView.addObject("action", "Edit");
			return modelAndView;
		}
		
		Retailer oldRetailer = retailerManager.getRetailerByUsername(retailer.getUsername());
		mailServer.sendUpdateAccountEmail(oldRetailer, retailer);
		
		oldRetailer.setEmail(retailer.getEmail());
		oldRetailer.setName(retailer.getName());
		oldRetailer.setPassword(basicTextEncryptor.encrypt(retailer.getPassword()));
		oldRetailer.setWebsite(retailer.getWebsite());
		
		/* Add User to DB */
		retailerManager.updateRetailer(oldRetailer);
		
		/* Get UserDetails and Login */
		UserDetails userDetails = retailerManager.loadUserByUsername(retailer.getUsername());
		SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(userDetails));
		
		return new ModelAndView(new RedirectView("index.html"));
	}
	
	@RequestMapping(value="/editAccount", method=RequestMethod.POST)
	public ModelAndView editAccount(@ModelAttribute("user") User user, BindingResult result){
		ModelAndView modelAndView; 
		
		userValidator.validate(user, result);
		if(result.getErrorCount() > 1){
			modelAndView = new ModelAndView("createAccount");
			modelAndView.addObject("errors", result);
			modelAndView.addObject("action", "Edit");
			return modelAndView;
		}
		
		/* Encrypt Password & Add User to DB */
        user.setPassword(basicTextEncryptor.encrypt(user.getPassword()));
		userManager.updateUser(user);
		
		/* Get UserDetails and Login */
		UserDetails userDetails = userManager.loadUserByUsername(user.getUsername());
		SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(userDetails));
		
		return new ModelAndView(new RedirectView("index.html"));
	}
	
	@RequestMapping(value="/forgotPassword", method=RequestMethod.GET)
	public ModelAndView getForgotPasswordPage(){
		ModelAndView modelAndView = new ModelAndView("forgotPassword");
		return modelAndView;
	}
	
	@RequestMapping(value="/forgotPassword", method=RequestMethod.POST)
	public ModelAndView forgotPassword(@RequestParam("username") String username){
		ModelAndView modelAndView = new ModelAndView("forgotPassword");
		User user = userManager.getUserByUsername(username);
		if(user == null){
			Retailer retailer = retailerManager.getRetailerByUsername(username);
			if(retailer == null){
				modelAndView.addObject("errors", "Invalid username");
				return modelAndView;
			}
			mailServer.sendForgotPasswordMail(retailer);
		}
		else{
			mailServer.sendForgotPasswordMail(user);
		}
		modelAndView.addObject("success", "Your password has been sent to your email account");
		return modelAndView;
	}
	
	@Autowired
	public void setUserValidator(Validator userValidator){
		this.userValidator = userValidator;
	}
	
	@Autowired
	public void setBasicTextEncryptor(BasicTextEncryptor basicTextEncryptor){
		this.basicTextEncryptor = basicTextEncryptor;
	}
	
	@Autowired
	public void setUserManager(UserManager userManager){
		this.userManager = userManager;
	}
	
	@Autowired
	public void setRetailerManager(RetailerManager retailerManager){
		this.retailerManager = retailerManager;
	}
	
	@Autowired
	public void setRetailerValidator(Validator retailerValidator){
		this.retailerValidator = retailerValidator;
	}
	
	@Autowired
	public void setMailServer(MailServer mailServer){
		this.mailServer = mailServer;
	}
}
