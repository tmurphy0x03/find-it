package org.findit.controller;

import java.util.LinkedList;
import java.util.List;

import org.findit.manager.BranchManager;
import org.findit.manager.RetailerManager;
import org.findit.manager.UserTypeChecker;
import org.findit.model.Branch;
import org.findit.model.Product;
import org.findit.model.Retailer;
import org.findit.model.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class HomeController {
	private UserTypeChecker userTypeChecker;
	private RetailerManager retailerManager;
	private BranchManager branchManager;
	
	@RequestMapping(value="/home", method=RequestMethod.GET)
	public ModelAndView getHome(){
		if(userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("retailerHome.html"));
		}
		return new ModelAndView(new RedirectView("customerHome.html"));
	}
	
	@RequestMapping(value="/index", method=RequestMethod.GET)
	public ModelAndView getIndex(){
		if(userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("retailerHome.html"));
		}
		return new ModelAndView(new RedirectView("customerHome.html"));
	}
	
	@RequestMapping(value="/retailerHome", method=RequestMethod.GET)
	public ModelAndView getRetailerHome(){
		ModelAndView modelAndView = new ModelAndView("retailerHome");
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		List<Branch> branches = retailer.getBranches();
		modelAndView.addObject("branches", branches);
		List<Product> products = new LinkedList<Product>();
		List<Service> services = new LinkedList<Service>();
		for(Branch branch : branches){
			for(int i = 0; i < branch.getProducts().size(); i++){
				/* If the list already contains the Product, don't add it again */
				if(!products.contains(branch.getProducts().get(i))){
					products.add(branch.getProducts().get(i));
				}
			}
			for(int i = 0; i < branch.getServices().size(); i++){
				/* If the list already contains the Product, don't add it again */
				if(!services.contains(branch.getServices().get(i))){
					services.add(branch.getServices().get(i));
				}
			}
		}
		if(branches != null && branches.size() > 0){
			modelAndView.addObject("products", products);
			modelAndView.addObject("services", services);
		}
		else {
			modelAndView.addObject("products", null);
			modelAndView.addObject("services", null);
		}
		return modelAndView;
	}
	
	@RequestMapping(value="/customerHome", method=RequestMethod.GET)
	public ModelAndView getCustomerHome(){
		ModelAndView modelAndView = new ModelAndView("customerHome");
		Branch branch = new Branch();
		Product product = new Product();
		Service service = new Service();
		modelAndView.addObject("branch", branch);
		modelAndView.addObject("product", product);
		modelAndView.addObject("service", service);
		return modelAndView;
	}
	
	@Autowired
	public void setUserTypeChecker(UserTypeChecker userTypeChecker){
		this.userTypeChecker = userTypeChecker;
	}
	@Autowired
	public void setBranchManager(BranchManager branchManager) {
		this.branchManager = branchManager;
	}
	@Autowired
	public void setRetailerManager(RetailerManager retailerManager){
		this.retailerManager = retailerManager;
	}
}
