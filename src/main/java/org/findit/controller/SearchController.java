/**
 *
 * @author Anthony Murphy
 */

package org.findit.controller;

import java.util.List;

import org.findit.manager.BranchManager;
import org.findit.manager.ProductManager;
import org.findit.manager.ServiceManager;
import org.findit.model.Branch;
import org.findit.model.Product;
import org.findit.model.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SearchController {
	private BranchManager branchManager;
	private ProductManager productManager;
	private ServiceManager serviceManager;

	@RequestMapping(value="/advancedSearch", method=RequestMethod.GET)
	public ModelAndView getAdvancedSearch(){
		ModelAndView modelAndView = new ModelAndView("advancedSearch");
		Branch branch = new Branch();
		modelAndView.addObject("branch", branch);
		
		Product product = new Product();
		modelAndView.addObject("product", product);
		
		Service service = new Service();
		modelAndView.addObject("service", service);
		return modelAndView;
	}
	
	@RequestMapping(value="/advancedSearchBranch", method=RequestMethod.POST)
	public ModelAndView advancedSearchBranch(@ModelAttribute("branch") Branch branch,
			@RequestParam("match") String match, @RequestParam("sort") String sort){
		System.out.println(branch);
		System.out.println(match);
		System.out.println(sort);
		ModelAndView modelAndView = new ModelAndView("branchSearchResults");
		List<Branch> branches = branchManager.getBranches(branch, match, sort);
		modelAndView.addObject("branches", branches);
		return modelAndView;
	}
	
	@RequestMapping(value="/advancedSearchProduct", method=RequestMethod.POST)
	public ModelAndView advancedSearchProduct(@ModelAttribute("product") Product product,
			@RequestParam("match") String match, @RequestParam("sort") String sort,
			@RequestParam("latitude") String latitude, @RequestParam("longitude") String longitude){
		System.out.println(product);
		System.out.println(match);
		System.out.println("lat"+latitude);
		ModelAndView modelAndView = new ModelAndView("productSearchResults");
		List<Product> products = productManager.getProducts(product, match, sort, longitude, latitude);
		modelAndView.addObject("products", products);
		return modelAndView;
	}
	
	@RequestMapping(value="/advancedSearchService", method=RequestMethod.POST)
	public ModelAndView advancedSearchService(@ModelAttribute("service") Service service,
			@RequestParam("match") String match, @RequestParam("sort") String sort,
			@RequestParam("latitude") String latitude, @RequestParam("longitude") String longitude){
		System.out.println(service);
		System.out.println(match);
		System.out.println(latitude);
		ModelAndView modelAndView = new ModelAndView("serviceSearchResults");
		List<Service> services = serviceManager.getServices(service, match, sort, longitude, latitude);
		modelAndView.addObject("services", services);
		return modelAndView;
	}
	
	@RequestMapping(value="/searchRetailer", method=RequestMethod.GET)
	public ModelAndView searchRetailer(@RequestParam("beginsWith") String beginsWith){
		ModelAndView modelAndView = new ModelAndView("branchSearchResults");
		List<Branch> branches = branchManager.getBranchesByRetailerFirstLetter(beginsWith);
		modelAndView.addObject("branches", branches);
		return modelAndView;
	}
	
	@RequestMapping(value="/findit", method=RequestMethod.GET)
	public ModelAndView findit(@RequestParam("searchinput") String param){
		ModelAndView modelAndView = new ModelAndView("finditResults");
		System.out.println(param);
		List<Branch> branches = branchManager.findit(param);
		modelAndView.addObject("branches", branches);
		
		List<Product> products = productManager.findit(param);
		modelAndView.addObject("products", products);
		
		List<Service> services = serviceManager.findit(param);
		modelAndView.addObject("services", services);
		
		return modelAndView;
	}
	
	@Autowired
	public void setBranchManager(BranchManager branchManager){
		this.branchManager = branchManager;
	}
	@Autowired
	public void setProductManager(ProductManager productManager){
		this.productManager = productManager;
	}
	@Autowired
	public void setServiceManager(ServiceManager serviceManager){
		this.serviceManager = serviceManager;
	}
}
