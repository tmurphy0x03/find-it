/**
 *
 * @author Anthony Murphy
 */

package org.findit.controller;

import java.util.List;

import org.findit.manager.BranchManager;
import org.findit.manager.RetailerManager;
import org.findit.manager.UserTypeChecker;
import org.findit.model.Branch;
import org.findit.model.Retailer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller for all Branches pages
 * @author Anthony Murphy
 */
@Controller
public class BranchesController {
	private UserTypeChecker userTypeChecker;
	private BranchManager branchManager;
	private Validator branchValidator;
	private RetailerManager retailerManager;
	
	/**
	 * Assembles the branches page
	 * @return the branches view
	 */
	@RequestMapping(value="/branches", method=RequestMethod.GET)
	public ModelAndView getBranchesPage(){
		ModelAndView modelAndView = new ModelAndView("branches");
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		List<Branch> branches = (List<Branch>) retailer.getBranches();
		modelAndView.addObject("branches", branches);
		return modelAndView;
	}
	
	/**
	 * Assembles the addBranch view
	 * @return the addBranch view
	 */
	@RequestMapping(value="/addBranch", method=RequestMethod.GET)
	public ModelAndView getAddBranchesPage(){
		ModelAndView modelAndView = new ModelAndView("addBranch");
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		Branch branch = new Branch();
		branch.setRetailerName(retailer.getName());
		branch.setRetailerUsername(retailer.getUsername());
		modelAndView.addObject("action", "add");
		modelAndView.addObject("branch", branch);
		return modelAndView;
	}
	
	/**
	 * Adds a branch to the retailers account
	 * @param branch
	 * @param result
	 * @return a redirect view to the branches view
	 */
	@RequestMapping(value="/addBranch", method=RequestMethod.POST)
	public ModelAndView addBranch(@ModelAttribute("branch") Branch branch, BindingResult result){
		/* Ensure the user is logged in and is a retailer */
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		/* Validate the branch and return errors if errors exist */
		branchValidator.validate(branch, result);
		if(result.hasErrors()){
			ModelAndView modelAndView = new ModelAndView("addBranch");
			modelAndView.addObject("errors", result);
			modelAndView.addObject("action", "add");
			return modelAndView;
		}
		/* Add the branch to the Retailer and update the Database */
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		branch.setRetailerName(retailer.getName());
		branch.setWebsite(retailer.getWebsite());
		retailer.addBranch(branch);
		retailerManager.updateRetailer(retailer);
		return new ModelAndView(new RedirectView("branches.html"));
	}
	
	/**
	 * Assembles the editBranch view
	 * @param index
	 * @return the editBranch view
	 */
	@RequestMapping(value="/editBranch", method=RequestMethod.GET)
	public ModelAndView getEditBranch(@RequestParam("index") int index){
		ModelAndView modelAndView = new ModelAndView("addBranch");
		/* Ensure the user is logged in and is a retailer */
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		/* Add the Branch as a Form Backing Object to the View */
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		List <Branch> branches = retailer.getBranches();
		Branch branch = branches.get(index);
		modelAndView.addObject("action", "edit");
		modelAndView.addObject("branch", branch);
		return modelAndView;
	}
	
	/**
	 * Edits a Branch
	 * @param branch
	 * @param result
	 * @return a redirect view to the branches view
	 */
	@RequestMapping(value="/editBranch", method=RequestMethod.POST)
	public ModelAndView editBranch(@ModelAttribute("branch") Branch branch, BindingResult result){
		/* Ensure the user is logged in and is a retailer */
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		/* Validate the branch and return errors if errors exist */
		branchValidator.validate(branch, result);
		if(result.hasErrors()){
			ModelAndView modelAndView = new ModelAndView("addBranch");
			modelAndView.addObject("errors", result);
			modelAndView.addObject("action", "edit");
			return modelAndView;
		}
		/* Update the Branch in the Retailer */
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		retailer.updateBranch(branch);
		retailerManager.updateRetailer(retailer);
		return new ModelAndView(new RedirectView("branches.html"));
	}
	
	/**
	 * Removes a Branch
	 * @param index
	 * @return a redirect view to the branches view
	 */
	@RequestMapping(value="/removeBranch", method=RequestMethod.GET)
	public ModelAndView removeBranch(@RequestParam("index") int index){
		/* Ensure the user is logged in and is a retailer */
		if(!userTypeChecker.isRetailer()){
			return new ModelAndView(new RedirectView("index.html"));
		}
		/* Remove the Branch from the Retailer */
		Retailer retailer = retailerManager.getRetailerByUsername(userTypeChecker.getLoggedInUsername());
		retailer.getBranches().remove(index);
		
		/* Update the Retailer */
		retailerManager.updateRetailer(retailer);
		return new ModelAndView(new RedirectView("branches.html"));
	}
	
	/**
	 * Assemble the viewBranch view
	 * @param index
	 * @return the viewBranch view
	 */
	@RequestMapping(value="/viewBranch", method=RequestMethod.GET)
	public ModelAndView getViewBranch(@RequestParam("branchId") int branchId){
		ModelAndView modelAndView = new ModelAndView("viewBranch");
		/* Get the Branch by the Id */
		Branch branch = branchManager.getBranchById(branchId);
		Retailer retailer = retailerManager.getRetailerByUsername(branch.getRetailerUsername());
		if(retailer.getBranches().contains(branch)){
			branch = retailer.getBranches().get(retailer.getBranches().indexOf(branch));
			branch.setHits(branch.getHits()+1);
			retailer.updateBranch(branch);
			retailerManager.updateRetailer(retailer);
			/* Add the Branch and RetailerName to the view */
			modelAndView.addObject("branch", branch);
			modelAndView.addObject("retailerName", branch.getRetailerName());
			System.out.println(branch);
			return modelAndView;
		}
		else {
			return new ModelAndView(new RedirectView("index.html"));
		}
	}
	
	/**
	 * Automatically called by Spring with the Bean definition value as the parameter. 
	 * This is known as Dependency Injection
	 * @param userTypeChecker
	 */
	@Autowired
	public void setUserTypeChecker(UserTypeChecker userTypeChecker){
		this.userTypeChecker = userTypeChecker;
	}
	
	/**
	 * Automatically called by Spring with the Bean definition value as the parameter. 
	 * This is known as Dependency Injection
	 * @param userTypeChecker
	 */
	@Autowired
	public void setBranchManager(BranchManager branchManager) {
		this.branchManager = branchManager;
	}
	
	/**
	 * Automatically called by Spring with the Bean definition value as the parameter. 
	 * This is known as Dependency Injection
	 * @param userTypeChecker
	 */
	@Autowired
	public void setRetailerManager(RetailerManager retailerManager){
		this.retailerManager = retailerManager;
	}
	
	/**
	 * Automatically called by Spring with the Bean definition value as the parameter. 
	 * This is known as Dependency Injection
	 * @param userTypeChecker
	 */
	@Autowired
	public void setBranchValidator(Validator branchValidator) {
		this.branchValidator = branchValidator;
	}
}
