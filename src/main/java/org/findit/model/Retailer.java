package org.findit.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(schema="findit", name = "retailer")
public class Retailer extends AbstractUser implements Serializable{
	private static final long serialVersionUID = -574048922226606303L;

	@Column(name = "name")
	private String name;
	
	@Column(name = "email")
	private String email;
	
	@Id
	@Column(name = "username")
	private String username;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "website")
	private String website;
	
	@Transient
	private String confirmPassword;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="username")
	private List<Branch> branches;
	
	@Override
	public String toString() {
		return "Retailer [name=" + name + ", email=" + email + ", username="
				+ username + ", password=" + password + ", confirmPassword="
				+ confirmPassword + ", branches=" + branches + "]";
	}

	public void addBranch(Branch branch){
		branches.add(branch);
	}
	
	public void updateBranch(Branch branch){
		for(int i = 0; i < branches.size(); i++){
			if(branches.get(i).getId() == branch.getId()){
				branches.set(i, branch);
				return;
			}
		}
	}
	
	public Branch getBranchById(int id){
		for(int i = 0; i < branches.size(); i++){
			if(branches.get(i).getId() == id){
				return branches.get(i);
			}
		}
		return null;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Retailer other = (Retailer) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	public Retailer(){
		branches = new LinkedList<Branch>();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setBranches(List<Branch> branches) {
		this.branches = branches;
	}

	public List<Branch> getBranches() {
		return branches;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getWebsite() {
		return website;
	}
}