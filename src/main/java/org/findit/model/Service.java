/**
 *
 * @author Anthony Murphy
 */

package org.findit.model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CollectionOfElements;

@Entity
@Table(schema="findit", name = "service")
public class Service implements Serializable {
	private static final long serialVersionUID = 1433441027643511312L;

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="price")
	private double price;
	
	@Column(name="category")
	private String category;
	
	@Column(name="description")
	private String description;
	
	@Column(name="branchNames")
	@CollectionOfElements
	private List<String> branchNames;
	
	@Column(name="branchNamesValue")
	@CollectionOfElements
	private List<String> branchNamesValues;
	
	@Column(name="hits")
	private int hits;
	
	@Column(name="retailerName")
	private String retailerName;
	
	@Column(name="retailerUsername")
	private String retailerUsername;

	
	@Override
	public String toString() {
		return "Service [id=" + id + ", name=" + name + ", price=" + price
				+ ", category=" + category + ", description=" + description
				+ ", branchNames=" + branchNames + ", branchNamesValues="
				+ branchNamesValues + ", hits=" + hits + ", retailerName="
				+ retailerName + ", retailerUsername=" + retailerUsername + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((branchNames == null) ? 0 : branchNames.hashCode());
		result = prime
				* result
				+ ((branchNamesValues == null) ? 0 : branchNamesValues
						.hashCode());
		result = prime * result
				+ ((category == null) ? 0 : category.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + hits;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		long temp;
		temp = Double.doubleToLongBits(price);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((retailerName == null) ? 0 : retailerName.hashCode());
		result = prime
				* result
				+ ((retailerUsername == null) ? 0 : retailerUsername.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Service other = (Service) obj;
		if (id != other.id)
			return false;
		if (retailerName == null) {
			if (other.retailerName != null)
				return false;
		} else if (!retailerName.equals(other.retailerName))
			return false;
		if (retailerUsername == null) {
			if (other.retailerUsername != null)
				return false;
		} else if (!retailerUsername.equals(other.retailerUsername))
			return false;
		return true;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Service(){
		branchNames = new LinkedList<String>();
		branchNamesValues = new LinkedList<String>();
		DecimalFormat twoDecimals = new DecimalFormat("#.##");
		price = Double.valueOf(twoDecimals.format(0));
	}
	
	public int getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getHits() {
		return hits;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}

	public String getRetailerName() {
		return retailerName;
	}

	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}

	public List<String> getBranchNames() {
		return branchNames;
	}

	public void setBranchNames(List<String> branchNames) {
		this.branchNames = branchNames;
	}

	public void setBranchNamesValues(List<String> branchNamesValues) {
		this.branchNamesValues = branchNamesValues;
	}

	public List<String> getBranchNamesValues() {
		return branchNamesValues;
	}

	public void setRetailerUsername(String retailerUsername) {
		this.retailerUsername = retailerUsername;
	}

	public String getRetailerUsername() {
		return retailerUsername;
	}
}
