/**
 *
 * @author Anthony Murphy
 */

package org.findit.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(schema="findit", name = "branch")
public class Branch implements Serializable{
	private static final long serialVersionUID = -6859199237634871889L;

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="address1")
	private String address1;
	
	@Column(name="address2")
	private String address2;
	
	@Column(name="address3")
	private String address3;
	
	@Column(name="location")
	private String location;
	
	@Column(name="region")
	private String region;
	
	@Column(name="postcode")
	private String postcode;
	
	@Column(name="country")
	private String country;
	
	@Column(name="property")
	private String property;
	
	@Column(name="latitude")
	private String latitude;
	
	@Column(name="longitude")
	private String longitude;
	
	@Column(name="hits")
	private int hits;
	
	@Column(name="phoneNumber")
	private String phoneNumber;
	
	@Column(name="email")
	private String email;
	
	@Column(name = "website")
	private String website;
	
	@Column(name="retailerName")
	private String retailerName;
	
	@Column(name="retailerUsername")
	private String retailerUsername;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="branchId")
	private List<Product> products;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="branchId")
	private List<Service> services;

	public void addProduct(Product product){
		products.add(product);
		System.out.println("ADD PRODUCT ID"+product.getId()+"products.size"+products.size());
	}
	
	public void removeProduct(Product product){
		products.remove(product);
	}
	
	public void updateProduct(Product product){
		for(int i = 0; i < products.size(); i++){
			if(products.get(i).getId() == product.getId()){
				products.set(i, product);
				return;
			}
		}
		products.add(product);
	}
	
	public void addService(Service service){
		services.add(service);
	}
	
	public void removeService(Service service){
		services.remove(service);
	}
	
	public void updateService(Service service){
		for(int i = 0; i < services.size(); i++){
			if(services.get(i).getId() == service.getId()){
				services.remove(i);
				services.add(service);
				return;
			}
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		System.out.println("EQUALS#######################################");
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Branch other = (Branch) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Branch [id=" + id + ", name=" + name + ", address1=" + address1
				+ ", address2=" + address2 + ", address3=" + address3
				+ ", location=" + location + ", region=" + region
				+ ", postcode=" + postcode + ", country=" + country
				+ ", property=" + property + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", hits=" + hits
				+ ", phoneNumber=" + phoneNumber + ", retailerName="
				+ retailerName + ", products=" + products + "]";
	}

	public Branch(){
		products = new LinkedList<Product>();
		services = new LinkedList<Service>();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getProperty() {
		return property;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}

	public int getHits() {
		return hits;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}

	public String getRetailerName() {
		return retailerName;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setRetailerUsername(String retailerUsername) {
		this.retailerUsername = retailerUsername;
	}

	public String getRetailerUsername() {
		return retailerUsername;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	public List<Service> getServices() {
		return services;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getWebsite() {
		return website;
	}
}
