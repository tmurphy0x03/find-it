<%@include file="fragments/header.jspf" %>

<c:if test="${action != 'Edit'}" >
	<h1><spring:message code="account.create"/></h1>
</c:if>
<c:if test="${action == 'Edit'}" >
	<h1><spring:message code="account.edit"/></h1>
</c:if>


<c:if test="${action != 'Edit'}" >
	<p class="smallText"><spring:message code="account.retailer.create.label"/>: <a href="createRetailerAccount.html"><spring:message code="retailer.account.create"/></a></p>
</c:if>

<form:form id="createAccountForm" commandName="user" method="post">
	<fieldset>
		<legend><spring:message code="user.details"/></legend>
		<p>
			<form:errors path="firstName" cssClass="error" />
			<label for="fname"><spring:message code="firstname"/>: *</label><form:input id="fname" path="firstName" maxlength="24" />
		</p>
		<p>
			<form:errors path="surname" cssClass="error" />
			<label for="sname"><spring:message code="surname"/>: *</label><form:input id="sname" path="surname" maxlength="24" />
		</p>
		<p>
			<form:errors path="email" cssClass="error" />
			<label for="email"><spring:message code="email"/>: *</label><form:input id="email" path="email" maxlength="24" />
		</p>
	</fieldset>
	
	<br />
	
	<fieldset>
		<legend><spring:message code="account.details"/></legend>
		<c:choose>
		<c:when test="${action != 'Edit'}" >
			<p>
				<form:errors path="username" cssClass="error" />
				<label for="uname"><spring:message code="username"/>: *</label><form:input id="uname" path="username" maxlength="24" />
			</p>
			<p>
				<form:errors path="password" cssClass="error" />
				<label for="pword"><spring:message code="password"/>: *</label><form:password id="pword" path="password" maxlength="24" />
			</p>
			<p>
				<form:errors path="confirmPassword" cssClass="error" />
				<label for="pword2"><spring:message code="password.confirm"/>: *</label><form:password id="pword2" path="confirmPassword" maxlength="24" />
			</p>
		</c:when>
		<c:otherwise>
			<p>
				<label for="uname"><spring:message code="username"/>: *</label><form:input id="uname" path="username" maxlength="24" readonly="true" />
			</p>
			<p>
				<form:errors path="password" cssClass="error" />
				<label for="pword"><spring:message code="newpassword"/>: *</label><form:password id="pword" path="password" maxlength="24" />
			</p>
			<p>
				<form:errors path="confirmPassword" cssClass="error" />
				<label for="pword2"><spring:message code="confirmNewPassword"/>: *</label><form:password id="pword2" path="confirmPassword" maxlength="24" />
			</p>
		</c:otherwise>
		</c:choose>
	</fieldset>
	<br />
	<c:choose>
	<c:when test="${action != 'Edit'}" >
		<div align="center">
			<input id="submit" type="submit" value="<spring:message code="account.create"/>" />
		</div>
	</c:when>
	<c:otherwise>
		<div align="center">
			<input id="submit" type="submit" value="<spring:message code="account.update"/>Update Account" />
		</div>
	</c:otherwise>
	</c:choose>
	
	<br /><br /><br />

</form:form>
<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - Create Account';
	});
</script>
</body>
</html>