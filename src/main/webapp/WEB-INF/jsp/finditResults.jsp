<%@include file="fragments/header.jspf" %>

<h1><spring:message code="results"/></h1>
	<div class="results">
	<c:choose>
		<c:when test="${fn:length(branches) == 0 && fn:length(products) == 0 && fn:length(services) == 0}" >
			<div class="emptyList"><spring:message code="search.noResults"/></div>
		</c:when>
		<c:otherwise>
		
		<c:forEach var="branch" items="${branches}" varStatus="index" >
		<div <c:choose><c:when test="${index.index % 2 == 1}">class="odd"</c:when><c:otherwise>class="even"</c:otherwise></c:choose> >
				<a href="viewBranch.html?branchId=${branch.id}"><span class="result_title">${branch.retailerName} - ${branch.name}</span></a><br />
				<c:if test="${branch.property != ''}">${branch.property} </c:if><c:if test="${branch.address1 != ''}">${branch.address1}</c:if><c:if test="${branch.address2 != ''}">, ${branch.address2}</c:if><c:if test="${branch.address3 != ''}">, ${branch.address3}</c:if><c:if test="${branch.location != ''}">, ${branch.location}</c:if><br />
				<c:if test="${branch.region != ''}">${branch.region}</c:if><c:if test="${branch.postcode != ''}">${branch.postcode}</c:if><c:if test="${branch.country != ''}">, ${branch.country}.</c:if>
		</div>
		</c:forEach>
		
		<c:forEach var="product" items="${products}" varStatus="index" >
		<div <c:choose><c:when test="${(index.index + fn:length(branches)) % 2 == 1}">class="odd"</c:when><c:otherwise>class="even"</c:otherwise></c:choose> >
				<a href="viewProduct.html?productId=${product.id}"><span class="result_title">${product.name} - ${product.retailerName}</span></a><br />
				<c:if test="${product.category != ''}">${product.category}</c:if>
				<c:if test="${product.description != ''}"><br />${product.description}</c:if>
				<c:if test="${product.price != ''}"><span class="number"><fmt:formatNumber value="${product.price}" type="currency" currencySymbol="&euro;" minFractionDigits="2" /></span></c:if>
		</div>
		</c:forEach>  
		
		<c:forEach var="service" items="${services}" varStatus="index" >
		<div <c:choose><c:when test="${(index.index + fn:length(branches) + fn:length(products)) % 2 == 1}">class="odd"</c:when><c:otherwise>class="even"</c:otherwise></c:choose> >
				<a href="viewService.html?serviceId=${service.id}"><span class="result_title">${service.name} - ${service.retailerName}</span></a><br />
				<c:if test="${service.category != ''}">${service.category}</c:if>
				<c:if test="${service.description != ''}"><br />${service.description}</c:if>
				<c:if test="${service.price != ''}"><span class="number"><fmt:formatNumber value="${service.price}" type="currency" currencySymbol="&euro;" minFractionDigits="2" /></span></c:if>
		</div>
		</c:forEach>
		</c:otherwise>
		</c:choose>
	</div>
<br />
<p class="smallText">
	<spring:message code="search.advancedSearch"/>: <a href="advancedSearch.html"><spring:message code="advancedSearch"/></a>
</p>








<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - Search Results';
	});
</script>
</body>
</html>