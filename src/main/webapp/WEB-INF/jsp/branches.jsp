<%@include file="fragments/header.jspf" %>

<h1><spring:message code="branches"/></h1>

<table>
	<tr>
		<th><spring:message code="name"/></th>
		<th><spring:message code="location"/></th>
		<th><spring:message code="region"/></th>
		<th><spring:message code="hits"/></th>
		<th><spring:message code="action"/></th>
	</tr>
	<c:choose>
	<c:when test="${fn:length(branches) == 0}" >
		<tr>
			<td colspan="5"><span class="emptyList"><spring:message code="retailer.noBranches"/></span></td>
		</tr>
	</c:when>
	<c:otherwise>
	</c:otherwise>
	</c:choose>
	<c:forEach var="branch" items="${branches}" varStatus="index" >
	<tr <c:if test="${index.index % 2 == 1}">class="odd"</c:if> >
		<td>${branch.name}</td>
		<td>${branch.location}</td>
		<td>${branch.region}</td>
		<td>${branch.hits}</td>
		<td align="center"><a href="viewBranch.html?branchId=${branch.id}"><spring:message code="view"/></a> <a href="editBranch.html?index=${index.index}"><spring:message code="edit"/></a> <a href="removeBranch.html?index=${index.index}"><spring:message code="remove"/></a></td>
	</tr>
	</c:forEach> 
</table>
<br />
<p class="smallText">
	<spring:message code="branch.add.label"/>: <a href="addBranch.html"><spring:message code="branch.add"/></a>
</p>








<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - Branches';
	});
</script>
</body>
</html>