<%@include file="fragments/header.jspf" %>

<h1>${retailerName}</h1>

<div class="home">
<fieldset>
<legend><a href="branches.html"><spring:message code="branches"/></a></legend>
<table>
	<tr>
		<th><spring:message code="name"/></th>
		<th><spring:message code="location"/></th>
		<th><spring:message code="region"/></th>
		<th><spring:message code="hits"/></th>
	</tr>
	<c:choose>
	<c:when test="${fn:length(branches) == 0}" >
		<tr>
			<td colspan="5"><span class="emptyList"><spring:message code="retailer.noBranches"/></span></td>
		</tr>
	</c:when>
	<c:otherwise>
	</c:otherwise>
	</c:choose>
	<c:forEach var="branch" items="${branches}" varStatus="index" >
	<tr <c:if test="${index.index % 2 == 1}">class="odd"</c:if> >
		<td>${branch.name}</td>
		<td>${branch.location}</td>
		<td>${branch.region}</td>
		<td>${branch.hits}</td>
	</tr>
	</c:forEach> 
</table>
</fieldset>
<br />

<c:if test="${fn:length(branches) > 0}" >
<fieldset>
<legend><a href="products.html"><spring:message code="products"/></a></legend>
<table>
	<tr>
		<th><spring:message code="name"/></th>
		<th><spring:message code="product.category"/></th>
		<th><spring:message code="hits"/></th>
		<th><spring:message code="product.price"/></th>
	</tr>
	<c:choose>
	<c:when test="${fn:length(products) == 0}" >
		<tr>
			<td colspan="6"><span class="emptyList"><spring:message code="product.noProducts"/></span></td>
		</tr>
	</c:when>
	<c:otherwise>
	
	<c:forEach var="product" items="${products}" varStatus="index" >
	<tr <c:if test="${index.index % 2 == 1}">class="odd"</c:if> >
		<td>${product.name}</td>
		<td>${product.category}</td>
		<td>${product.hits}</td>
		<td><span class="number"><fmt:formatNumber value="${product.price}" type="currency" currencySymbol="&euro;" minFractionDigits="2" /></span></td>
	</tr>
	</c:forEach> 
	</c:otherwise>
	</c:choose>
</table>
</fieldset>
<br />
<fieldset>
<legend><a href="services.html"><spring:message code="services"/></a></legend>
<table>
	<tr>
		<th><spring:message code="name"/></th>
		<th><spring:message code="service.category"/></th>
		<th><spring:message code="hits"/></th>
		<th><spring:message code="service.price"/></th>
	</tr>
	<c:choose>
	<c:when test="${fn:length(services) == 0}" >
		<tr>
			<td colspan="6"><span class="emptyList"><spring:message code="service.noServices"/></span></td>
		</tr>
	</c:when>
	<c:otherwise>
	
	<c:forEach var="service" items="${services}" varStatus="index" >
	<tr <c:if test="${index.index % 2 == 1}">class="odd"</c:if> >
		<td>${service.name}</td>
		<td>${service.category}</td>
		<td>${service.hits}</td>
		<td><span class="number"><fmt:formatNumber value="${service.price}" type="currency" currencySymbol="&euro;" minFractionDigits="2" /></span></td>
	</tr>
	</c:forEach> 
	</c:otherwise>
	</c:choose>
</table>
</fieldset>
</c:if>
</div>
<br />

<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - Home';
	});
</script>
</body>
</html>