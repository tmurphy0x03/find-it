<%@include file="fragments/header.jspf" %>
<div class="retailerSearch" align="center">
	<a href="searchRetailer.html?beginsWith=a">A</a>
	<a href="searchRetailer.html?beginsWith=b">B</a>
	<a href="searchRetailer.html?beginsWith=c">C</a>
	<a href="searchRetailer.html?beginsWith=d">D</a>
	<a href="searchRetailer.html?beginsWith=e">E</a>
	<a href="searchRetailer.html?beginsWith=f">F</a>
	<a href="searchRetailer.html?beginsWith=g">G</a>
	<a href="searchRetailer.html?beginsWith=h">H</a>
	<a href="searchRetailer.html?beginsWith=i">I</a>
	<a href="searchRetailer.html?beginsWith=j">J</a>
	<a href="searchRetailer.html?beginsWith=k">K</a>
	<a href="searchRetailer.html?beginsWith=l">L</a>
	<a href="searchRetailer.html?beginsWith=m">M</a>
	<a href="searchRetailer.html?beginsWith=n">N</a>
	<a href="searchRetailer.html?beginsWith=o">O</a>
	<a href="searchRetailer.html?beginsWith=p">P</a>
	<a href="searchRetailer.html?beginsWith=q">Q</a>
	<a href="searchRetailer.html?beginsWith=r">R</a>
	<a href="searchRetailer.html?beginsWith=s">S</a>
	<a href="searchRetailer.html?beginsWith=t">T</a>
	<a href="searchRetailer.html?beginsWith=u">U</a>
	<a href="searchRetailer.html?beginsWith=v">V</a>
	<a href="searchRetailer.html?beginsWith=w">W</a>
	<a href="searchRetailer.html?beginsWith=x">X</a>
	<a href="searchRetailer.html?beginsWith=y">Y</a>
	<a href="searchRetailer.html?beginsWith=z">Z</a>
	<a href="searchRetailer.html?beginsWith=0">0</a>
	<a href="searchRetailer.html?beginsWith=1">1</a>
	<a href="searchRetailer.html?beginsWith=2">2</a>
	<a href="searchRetailer.html?beginsWith=3">3</a>
	<a href="searchRetailer.html?beginsWith=4">4</a>
	<a href="searchRetailer.html?beginsWith=5">5</a>
	<a href="searchRetailer.html?beginsWith=6">6</a>
	<a href="searchRetailer.html?beginsWith=7">7</a>
	<a href="searchRetailer.html?beginsWith=8">8</a>
	<a href="searchRetailer.html?beginsWith=9">9</a>
</div>
<h1><spring:message code="advancedSearch"/></h1>



<div id="searchOptions">
<fieldset>
<legend><spring:message code="search.options"/></legend>
<p>
<label for="options"><spring:message code="search.category"/>: </label>
<select id="options" name="options" onchange="toggleOptions()">
	<option value="branch"><spring:message code="branch.search"/></option>
	<option value="product"><spring:message code="product.search"/></option>
	<option value="service"><spring:message code="service.search"/></option>
</select>
</p>
<p>
<label for="match"><spring:message code="search.match"/>: </label>
<select id="match" name="match" onchange="toggleMatchType()">
	<option value="partial"><spring:message code="search.partial"/></option>
	<option value="exact"><spring:message code="search.exact"/></option>
</select>
</p>
<p>
<label for="sort"><spring:message code="search.sortby"/>: </label>
<select id="sort" name="sort" onchange="toggleSortType()">
	<option value="relevance"><spring:message code="relevance"/></option>
	<option value="hits"><spring:message code="hits"/></option>
	<option value="location"><spring:message code="location"/></option>
</select>
</p>
</div>
</fieldset>

<br />
<div id="branchSearch">
<form:form id="advancedSearchBranch" name="advancedSearchBranch" action="advancedSearchBranch.html" commandName="branch" method="post">
	<fieldset>
		<legend><spring:message code="branch.address"/></legend>
		<p>
			<form:errors path="name" cssClass="error" />
			<label for="name"><spring:message code="branch.name"/>: </label><form:input id="name" path="name" maxlength="24" />
		</p>
		<p>
			<form:errors path="property" cssClass="error" />
			<label for="property"><spring:message code="property"/>: </label><form:input id="property" path="property" maxlength="24" />
		</p>
		<p>
			<form:errors path="address1" cssClass="error" />
			<label for="address1"><spring:message code="address.1"/>: </label><form:input id="address1" path="address1" maxlength="25" />
		</p>
		<p>
			<form:errors path="address2" cssClass="error" />
			<label for="address2"><spring:message code="address.2"/>: </label><form:input id="address2" path="address2" maxlength="24" />
		</p>
		<p>
			<form:errors path="address3" cssClass="error" />
			<label for="address3"><spring:message code="address.3"/>: </label><form:input id="address3" path="address3" maxlength="24" />
		</p>
		<p>
			<form:errors path="location" cssClass="error" />
			<label for="location"><spring:message code="location"/>: </label><form:input id="location" path="location" maxlength="24" />
		</p>
		<p>
			<form:errors path="region" cssClass="error" />
			<label for="region"><spring:message code="region"/>: </label><form:input id="region" path="region" maxlength="24" />
		</p>
		<p>
			<label for="postcode"><spring:message code="postcode"/>: </label><form:input id="postcode" path="postcode" maxlength="24" />
		</p>
		<p>
			<form:errors path="country" cssClass="error" />
			<label for="country"><spring:message code="country"/>: </label><form:input id="country" path="country" maxlength="24" />
		</p>
		<form:hidden id="latitude" path="latitude" />
		<form:hidden id="longitude" path="longitude" />
		<form:hidden id="id" path="id" />
		<form:hidden id="retailerName" path="retailerName" />
		<form:hidden id="phoneNumber" path="phoneNumber" />
		<form:hidden id="retailerUsername" path="retailerUsername" />
		<input type="hidden" id="branchMatch" name="match" value="partial" />
		<input type="hidden" id="branchSort" name="sort" value="relevance" />
	</fieldset>
	
	<br />
	<div align="center">
		<input id="submit" type="submit" value="<spring:message code="branch.search"/>" />
	</div>
</form:form>
<br />
<br />
</div>

<div id="productSearch">
<form:form id="advancedSearchProduct" action="advancedSearchProduct.html" commandName="product" method="post">
	<fieldset>
		<legend><spring:message code="product.details"/></legend>
		<p>
			<form:errors path="name" cssClass="error" />
			<label for="productName"><spring:message code="product.name"/>: </label><form:input id="productName" path="name" maxlength="24" />
		</p>
		<p>
			<form:errors path="category" cssClass="error" />
			<label for="category"><spring:message code="product.category"/>: </label>
			<form:select id="category" path="category">
				<form:option value=""><spring:message code="all"/></form:option>
				<form:option value="Clothing"><spring:message code="clothing"/></form:option>
				<form:option value="DIY"><spring:message code="diy"/></form:option>
				<form:option value="Food"><spring:message code="food"/></form:option>
				<form:option value="Toiletries"><spring:message code="toiletries"/></form:option>
				<form:option value="Electronics"><spring:message code="electronics"/></form:option>
				<form:option value="Entertainment"><spring:message code="entertainment"/></form:option>
				<form:option value="Hobbies"><spring:message code="hobbies"/></form:option>
				<form:option value="Transport"><spring:message code="transport"/></form:option>
				<form:option value="Decorations"><spring:message code="decorations"/></form:option>
				<form:option value="Appliances"><spring:message code="appliances"/></form:option>
				<form:option value="Maintenance"><spring:message code="maintenence"/></form:option>
				<form:option value="Furniture"><spring:message code="furniture"/></form:option>
				<form:option value="Security"><spring:message code="security"/></form:option>
				<form:option value="Accessories"><spring:message code="accessories"/></form:option>
				<form:option value="Miscellaneous"><spring:message code="miscellaneous"/></form:option>
			</form:select>
		</p>
		<p>
			<form:errors path="description" cssClass="error" />
			<label for="description"><spring:message code="product.description"/>: </label><form:textarea id="description" path="description" rows="3"/>
		</p>
		<form:hidden id="id" path="id" />
		<form:hidden id="retailerName" path="retailerName" />
		<form:hidden id="retailerUsername" path="retailerUsername" />
		<input type="hidden" id="productMatch" name="match" value="partial" />
		<input type="hidden" id="productSort" name="sort" value="relevance" />
		<input type="hidden" id="productLongitude" name="longitude" />
		<input type="hidden" id="productLatitude" name="latitude" />
	</fieldset>  
	<br />
	<div align="center">
		<input id="submit" type="submit" value="<spring:message code="product.search"/>" />
	</div>
</form:form>
<br />
<br />
</div>

<div id="serviceSearch">
<form:form id="advancedSearchService" action="advancedSearchService.html" commandName="service" method="post">
	<fieldset>
		<legend><spring:message code="service.details"/></legend>
		<p>
			<form:errors path="name" cssClass="error" />
			<label for="serviceName"><spring:message code="service.name"/>: </label><form:input id="serviceName" path="name" maxlength="24" />
		</p>
		<p>
			<form:errors path="category" cssClass="error" />
			<label for="category"><spring:message code="service.category"/>: </label>
			<form:select id="category" path="category">
				<form:option value=""><spring:message code="all"/></form:option>
				<form:option value="Clothing"><spring:message code="clothing"/></form:option>
				<form:option value="DIY"><spring:message code="diy"/></form:option>
				<form:option value="Food"><spring:message code="food"/></form:option>
				<form:option value="Toiletries"><spring:message code="toiletries"/></form:option>
				<form:option value="Electronics"><spring:message code="electronics"/></form:option>
				<form:option value="Entertainment"><spring:message code="entertainment"/></form:option>
				<form:option value="Hobbies"><spring:message code="hobbies"/></form:option>
				<form:option value="Transport"><spring:message code="transport"/></form:option>
				<form:option value="Decorations"><spring:message code="decorations"/></form:option>
				<form:option value="Appliances"><spring:message code="appliances"/></form:option>
				<form:option value="Maintenance"><spring:message code="maintenence"/></form:option>
				<form:option value="Furniture"><spring:message code="furniture"/></form:option>
				<form:option value="Security"><spring:message code="security"/></form:option>
				<form:option value="Accessories"><spring:message code="accessories"/></form:option>
				<form:option value="Miscellaneous"><spring:message code="miscellaneous"/></form:option>
			</form:select>
		</p>
		<p>
			<form:errors path="description" cssClass="error" />
			<label for="description"><spring:message code="service.description"/>: </label><form:textarea id="description" path="description" rows="3"/>
		</p>
		<form:hidden id="id" path="id" />
		<form:hidden id="retailerName" path="retailerName" />
		<form:hidden id="retailerUsername" path="retailerUsername" />
		<input type="hidden" id="serviceMatch" name="match" value="partial" />
		<input type="hidden" id="serviceSort" name="sort" value="relevance" />
		<input type="hidden" id="serviceLongitude" name="longitude" />
		<input type="hidden" id="serviceLatitude" name="latitude" />
	</fieldset>  
	<br />
	<div align="center">
		<input id="submit" type="submit" value="<spring:message code="service.search"/>" />
	</div>
</form:form>
</div>
<br />

<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - Advanced Search';
		$('#options option[value=branch]').attr('selected', 'selected');
		$("#productSearch").hide();
		$("#serviceSearch").hide();
		getLocation();
	});
	
	/* Load Google Maps */
	function getLocation(){
		var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
		
		/* Check if the users browser supports HTML5 Geolocation */
		if(is_chrome){
			if (navigator.geolocation) {
	            navigator.geolocation.getCurrentPosition(function (position) {
	            	if(position != null){
	            		document.getElementById("latitude").value = position.coords.latitude;
	    				document.getElementById("longitude").value = position.coords.longitude;
	    				document.getElementById("productLatitude").value = position.coords.latitude;
	    				document.getElementById("productLongitude").value = position.coords.longitude;
	    				document.getElementById("serviceLatitude").value = position.coords.latitude;
	    				document.getElementById("serviceLongitude").value = position.coords.longitude;
	            	}
	            });
	        }
		}
		else {
			$("#sort option[value='location']").remove();
		}
	}
	
	function toggleOptions(){
		if($("#options option:selected").val() == "branch"){
			toggleBranch();
		}
		if($("#options option:selected").val() == "product"){
			toggleProduct();
		}
		if($("#options option:selected").val() == "service"){
			toggleService();
		}
	}
	
	function toggleBranch(){
		alert("product");
		if($("#branchSearch").is(":visible")){
			$("#branchSearch").fadeOut();
			$("#productSearch").hide();
			$("#serviceSearch").hide();
		}
		if($("#branchSearch").is(":hidden")){
			$("#branchSearch").fadeIn();
			$("#productSearch").hide();
			$("#serviceSearch").hide();
		}
	}
	
	function toggleProduct(){
		if($("#productSearch").is(":visible")){
			$("#productSearch").fadeOut();
			$("#branchSearch").hide();
			$("#serviceSearch").hide();
		}
		if($("#productSearch").is(":hidden")){
			$("#productSearch").fadeIn();
			$("#branchSearch").hide();
			$("#serviceSearch").hide();
		}
	}
	
	function toggleService(){
		if($("#serviceSearch").is(":visible")){
			$("#serviceSearch").fadeOut();
			$("#productSearch").hide();
			$("#branchSearch").hide();
		}
		if($("#serviceSearch").is(":hidden")){
			$("#serviceSearch").fadeIn();
			$("#productSearch").hide();
			$("#branchSearch").hide();
		}
	}
	
	function toggleMatchType(){
		if($("#match option:selected").val() == "exact"){
			$("#branchMatch").val("exact");
			$("#productMatch").val("exact");
			$("#serviceMatch").val("exact");
		}
		if($("#match option:selected").val() == "partial"){
			$("#branchMatch").val("partial");
			$("#productMatch").val("partial");
			$("#serviceMatch").val("partial");
		}
	}
	
	function toggleSortType(){
		if($("#sort option:selected").val() == "relevance"){
			$("#branchSort").val("relevance");
			$("#productSort").val("relevance");
			$("#serviceSort").val("relevance");
		}
		if($("#sort option:selected").val() == "hits"){
			$("#branchSort").val("hits");
			$("#productSort").val("hits");
			$("#serviceSort").val("hits");
		}
		if($("#sort option:selected").val() == "location"){
			$("#branchSort").val("location");
			$("#productSort").val("location");
			$("#serviceSort").val("location");
		}
	}
	
	
</script>
<script src="http://maps.google.com/maps?file=api&v=3&key=ABQIAAAAcuBDjlkW6c6EJ4el3KM_8xSc09D1umn1OR-kfLK_fM6PkjzgThStBVznrYdC8qDotONirH73e_-wwQ" type="text/javascript"></script>

</body>
</html>