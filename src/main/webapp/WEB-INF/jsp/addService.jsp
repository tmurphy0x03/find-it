<%@include file="fragments/header.jspf" %>

<c:choose>
<c:when test="${action == 'add'}">
	<h1><spring:message code="service.add"/></h1>
</c:when>
<c:otherwise>
	<h1><spring:message code="service.edit"/></h1>
</c:otherwise>
</c:choose>
<form:form id="addServiceForm" commandName="service" method="post">
	<fieldset>
		<legend><spring:message code="service.details"/></legend>
		<p>
			<form:errors path="name" cssClass="error" />
			<label for="name"><spring:message code="service.name"/>: *</label><form:input id="name" path="name" maxlength="24" />
		</p>
		<p>
			<form:errors path="price" cssClass="error" />
			<label for="price"><spring:message code="service.price"/>(&#8364): *</label><form:input id="price" path="price" maxlength="24" />
		</p>
		<p>
			<form:errors path="category" cssClass="error" />
			<label for="category"><spring:message code="service.category"/>: *</label>
			<form:select id="category" path="category">
				<form:option value="Clothing"><spring:message code="clothing"/></form:option>
				<form:option value="DIY"><spring:message code="diy"/></form:option>
				<form:option value="Food"><spring:message code="food"/></form:option>
				<form:option value="Toiletries"><spring:message code="toiletries"/></form:option>
				<form:option value="Electronics"><spring:message code="electronics"/></form:option>
				<form:option value="Entertainment"><spring:message code="entertainment"/></form:option>
				<form:option value="Hobbies"><spring:message code="hobbies"/></form:option>
				<form:option value="Transport"><spring:message code="transport"/></form:option>
				<form:option value="Decorations"><spring:message code="decorations"/></form:option>
				<form:option value="Appliances"><spring:message code="appliances"/></form:option>
				<form:option value="Maintenance"><spring:message code="maintenence"/></form:option>
				<form:option value="Furniture"><spring:message code="furniture"/></form:option>
				<form:option value="Security"><spring:message code="security"/></form:option>
				<form:option value="Accessories"><spring:message code="accessories"/></form:option>
				<form:option value="Miscellaneous"><spring:message code="miscellaneous"/></form:option>
			</form:select>
		</p>
		<p>
			<form:errors path="description" cssClass="error" />
			<label for="description"><spring:message code="service.description"/>: *</label><form:textarea id="description" path="description" rows="3"/>
		</p>
		<form:hidden id="id" path="id" />
		<form:hidden id="retailerName" path="retailerName" />
		<form:hidden id="retailerUsername" path="retailerUsername" />
	</fieldset>  
	<br />
	<fieldset>
	<legend>Branches</legend>
	<form:errors path="branchNames" cssClass="generalError" />
  	<ul class="combobox">
		<c:forEach var="branch" items="${service.branchNames}" varStatus="index" >
			<c:choose>
			<c:when test="${service.branchNamesValues[index.index] == 'true'}">
				<li class="combobox"><label class="combobox" for="branch${index.index}"><input type="checkbox" id="branch${index.index}" value="'${service.branchNamesValues[index.index]}'" checked="true" onclick="updateCheckboxValue('branch${index.index}');" />${service.branchNames[index.index]}</label></li>
			</c:when>
			<c:otherwise>
		   		<li class="combobox"><label class="combobox" for="branch${index.index}"><input type="checkbox" id="branch${index.index}" value="'${service.branchNamesValues[index.index]}'" onclick="updateCheckboxValue('branch${index.index}');" />${service.branchNames[index.index]}</label></li>
		  	</c:otherwise>
		  	</c:choose>
		<form:hidden id="branchNames${index.index}" path="branchNames[${index.index}]" />
		<form:hidden id="branch${index.index}Value" path="branchNamesValues[${index.index}]" />
	  	</c:forEach>
  	</ul>
	</fieldset>
	<br />
	<div align="center">
		<c:choose>
		<c:when test="${action == 'add'}">
			<input id="submit" type="submit" value="<spring:message code="service.add"/>" />
		</c:when>
		<c:otherwise>
			<input id="submit" type="submit" value="<spring:message code="service.update"/>" />
		</c:otherwise>
		</c:choose>
	</div>
</form:form>
<br />


<%@include file="fragments/footer.jspf" %>


<script type="text/javascript">
	$(document).ready(function() {
		if(${action == 'add'}){
			document.title = 'FindIt - Add Service';
		}
		else {
			document.title = 'FindIt - Edit Service';
		}
	});
	
	function updateCheckboxValue(id){
		<!-- Check if it is NOW checked -->
		if($("#"+id).attr('checked')){
			$("#"+id+"Value").attr('value', 'true');
		}
		else {
			$("#"+id+"Value").attr('value', 'false');
		}
	}
	
</script>

</body>
</html>