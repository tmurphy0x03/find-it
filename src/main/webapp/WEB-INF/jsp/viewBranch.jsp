<%@include file="fragments/header.jspf" %>

<h1>${retailerName}</h1>
	<div id="details">
	<fieldset>
		<legend><spring:message code="branch.address"/></legend>
		<c:if test="${branch.name != ''}">
		<p>
			<label><spring:message code="branch.name"/>: </label><label class="details">${branch.name}</label>
		</p>
		</c:if>
		<c:if test="${branch.phoneNumber != ''}">
		<p>
			<label><spring:message code="phoneNumber"/>: </label><label class="details">${branch.phoneNumber}</label>
		</p>
		</c:if>
		<c:if test="${branch.email != ''}">
		<p>
			<label><spring:message code="email"/>: </label><label class="details">${branch.email}</label>
		</p>
		</c:if>
		<c:if test="${branch.website != ''}">
		<p>
			<label><spring:message code="website"/>: </label><label class="details"><a href="${branch.website}">${branch.website}</a></label>
		</p>
		</c:if>
		<c:if test="${branch.property != ''}">
		<p>
			<label><spring:message code="property"/>: </label><label class="details">${branch.property}</label>
		</p>
		</c:if>
		<c:if test="${branch.address1 != ''}">
		<p>
			<label><spring:message code="address.1"/>: </label><label class="details">${branch.address1}</label>
		</p>
		</c:if>
		<c:if test="${branch.address2 != ''}">
		<p>
			<label><spring:message code="address.2"/>: </label><label class="details">${branch.address2}</label>
		</p>
		</c:if>
		<c:if test="${branch.address3 != ''}">
		<p>
			<label><spring:message code="address.3"/>: </label><label class="details">${branch.address3}</label>
		</p>
		</c:if>
		<c:if test="${branch.location != ''}">
		<p>
			<label><spring:message code="location"/>: </label><label class="details">${branch.location}</label>
		</p>
		</c:if>
		<c:if test="${branch.region != ''}">
		<p>
			<label><spring:message code="region"/>: </label><label class="details">${branch.region}</label>
		</p>
		</c:if>
		<c:if test="${branch.postcode != ''}">
		<p>
			<label><spring:message code="postcode"/>: </label><label class="details">${branch.postcode}</label>
		</p>
		</c:if>
		<c:if test="${branch.country != ''}">
		<p>
			<label><spring:message code="country"/>: </label><label class="details">${branch.country}</label>
		</p>
		</c:if>
	</fieldset>
	
	<br />
	<fieldset>
	<legend><spring:message code="map"/></legend>
	<div align="center" id="map" class="map" ></div>
	</fieldset>
	
	<c:if test="${fn:length(branch.products) > 0}" >
	<br />
	<fieldset>
	<legend id="products" ><spring:message code="products"/></legend>
		<c:forEach var="product" items="${branch.products}" varStatus="index" >
			<br />
			<a href="#product" class="headerLink" onclick="showProduct('${index.index}', '${fn:length(branch.products)}')">${product.name}</a>
			<div id="product${index.index}" style="display: none;">
			<div id="productInfo">
				<c:if test="${product.name != ''}">
				<p>
					<label><spring:message code="product.name"/>: </label><label class="details">${product.name}</label>
				</p>
				</c:if>
				<c:if test="${product.price != ''}">
				<p>
					<label><spring:message code="product.price"/>: </label><label class="details"><fmt:formatNumber value="${product.price}" type="currency" currencySymbol="&euro;" minFractionDigits="2" /></label>
				</p>
				</c:if>
				<c:if test="${product.category != ''}">
				<p>
					<label><spring:message code="product.category"/>: </label><label class="details">${product.category}</label>
				</p>
				</c:if>
				<c:if test="${product.description != ''}">
				<p>
				<label><spring:message code="product.description"/>: </label><label class="details">${product.description}</label>
				</p>
				</c:if>
				<c:if test="${product.hits != ''}">
				<p>
					<label><spring:message code="hits"/>: </label><label class="details">${product.hits}</label>
				</p>
				</c:if>
			</div>
			<br />
			</div>
		</c:forEach>
	</fieldset>
	</c:if>
	
	<c:if test="${fn:length(branch.services) > 0}" >
	<br />
	<fieldset>
	<legend id="services" ><spring:message code="services"/></legend>
		<c:forEach var="service" items="${branch.services}" varStatus="index" >
			<br />
			<a href="#service" class="headerLink" onclick="showService('${index.index}', '${fn:length(branch.services)}')">${service.name}</a>
			<div id="service${index.index}" style="display: none;">
			<div id="serviceInfo">
				<c:if test="${service.name != ''}">
				<p>
					<label><spring:message code="service.name"/>: </label><label class="details">${service.name}</label>
				</p>
				</c:if>
				<c:if test="${service.price != ''}">
				<p>
					<label><spring:message code="service.price"/>: </label><label class="details"><fmt:formatNumber value="${service.price}" type="currency" currencySymbol="&euro;" minFractionDigits="2" /></label>
				</p>
				</c:if>
				<c:if test="${service.category != ''}">
				<p>
					<label><spring:message code="service.category"/>: </label><label class="details">${service.category}</label>
				</p>
				</c:if>
				<c:if test="${service.description != ''}">
				<p>
				<label><spring:message code="service.description"/>: </label><label class="details">${service.description}</label>
				</p>
				</c:if>
				<c:if test="${service.hits != ''}">
				<p>
					<label><spring:message code="hits"/>: </label><label class="details">${service.hits}</label>
				</p>
				</c:if>
			</div>
			<br />
			</div>
		</c:forEach>
	</fieldset>
	</c:if>
	
	<br />
	<fieldset>
	<legend><spring:message code="socialNetwork"/></legend>
		<div align="center">
			<span class="facebook">
				<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="" layout="box_count" show_faces="true" width="100" action="recommend" font=""></fb:like>
			</span>
			<span class="twitter">
				<a href="http://twitter.com/share" class="twitter-share-button" data-count="vertical">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
			</span>
		</div>
	</fieldset>
	</div>
	<br />
<br />


<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		viewMap();
		document.title = 'FindIt - View Branch';
	});
	
	function viewMap(){
		/* Setup the Map */
		var map = new GMap2(document.getElementById("map"));
		map.setMapType(G_HYBRID_MAP);
		map.addControl(new GLargeMapControl());
		map.addControl(new GMapTypeControl());
		map.enableScrollWheelZoom();
		
        /* Gets the Longitued and Latitude values */
		if(${branch.latitude} != null && ${branch.longitude} != null){
			map.clearOverlays();
			map.addOverlay(new GMarker(new GLatLng(${branch.latitude}, ${branch.longitude}), 12));
			map.setCenter(new GLatLng(${branch.latitude}, ${branch.longitude}), 16);
		}
	}
	
	function showProduct(id, products){
		if($("#product"+id).is(":hidden")){
			$("#product"+id).slideDown();
		}
		else {
			$("#product"+id).slideUp();
		}
		
		for(i = 0 ; i < products ; i++){
			if(i != id && $("#product"+i).is(":visible")){
				$("#product"+i).slideUp();
			}
		}
	}
	
	function showService(id, services){
		if($("#service"+id).is(":hidden")){
			$("#service"+id).slideDown();
		}
		else {
			$("#service"+id).slideUp();
		}
		
		for(i = 0 ; i < services ; i++){
			if(i != id && $("#service"+i).is(":visible")){
				$("#service"+i).slideUp();
			}
		}
	}
		
</script>
<script src="http://maps.google.com/maps?file=api&v=3&key=ABQIAAAAcuBDjlkW6c6EJ4el3KM_8xSc09D1umn1OR-kfLK_fM6PkjzgThStBVznrYdC8qDotONirH73e_-wwQ" type="text/javascript"></script>

</body>
</html>