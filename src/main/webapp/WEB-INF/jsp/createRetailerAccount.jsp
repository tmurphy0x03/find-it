<%@include file="fragments/header.jspf" %>


<c:choose>
<c:when test="${action != 'Edit'}" >
	<h1><spring:message code="retailer.account.create"/></h1>
</c:when>
<c:otherwise>
	<h1><spring:message code="account.edit"/></h1>
</c:otherwise>
</c:choose>

<form:form id="createRetailerAccountForm" name="createRetailerAccountForm" commandName="retailer" method="post">
	<fieldset>
		<legend><spring:message code="retailer.details"/></legend>
		<p>
			<form:errors path="name" cssClass="error" />
			<label for="name"><spring:message code="name"/>: *</label><form:input id="name" path="name" maxlength="24" />
		</p>
		<p>
			<form:errors path="email" cssClass="error" />
			<label for="email"><spring:message code="email"/>: *</label><form:input id="email" path="email" maxlength="24" />
		</p>
		<p>
			<form:errors path="website" cssClass="error" />
			<label for="website"><spring:message code="website"/>: *</label><form:input id="website" path="website" />
		</p>
	</fieldset>
	
	<br />
	
	<fieldset>
		<legend><spring:message code="account.details"/></legend>
		<c:choose>
		<c:when test="${action != 'Edit'}" >
			<p>
				<form:errors path="username" cssClass="error" />
				<label for="uname"><spring:message code="username"/>: *</label><form:input id="uname" path="username" maxlength="24" />
			</p>
			<p>
				<form:errors path="password" cssClass="error" />
				<label for="pword"><spring:message code="password"/>: *</label><form:password id="pword" path="password" maxlength="24" />
			</p>
			<p>
				<form:errors path="confirmPassword" cssClass="error" />
				<label for="pword2"><spring:message code="password.confirm"/>: *</label><form:password id="pword2" path="confirmPassword" maxlength="24" />
			</p>
		</c:when>
		<c:otherwise>
			<p>
				<label for="uname"><spring:message code="username"/>: *</label><form:input id="uname" path="username" maxlength="24" readonly="true" />
			</p>
			<p>
				<form:errors path="password" cssClass="error" />
				<label for="pword"><spring:message code="newpassword"/>: *</label><form:password id="pword" path="password" maxlength="24" />
			</p>
			<p>
				<form:errors path="confirmPassword" cssClass="error" />
				<label for="pword2"><spring:message code="confirmNewPassword"/>: *</label><form:password id="pword2" path="confirmPassword" maxlength="24" />
			</p>
		</c:otherwise>
		</c:choose>
	</fieldset>
	<br />
	<c:choose>
	<c:when test="${action != 'Edit'}" >
		<div align="center">
			<input id="submit" type="submit" value="<spring:message code="account.create"/>" />
		</div>
	</c:when>
	<c:otherwise>
		<div align="center">
			<input id="submit" type="submit" value="<spring:message code="account.update"/>" />
		</div>
	</c:otherwise>
	</c:choose>
	<br /><br /><br />

</form:form>
<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - Create Account';
	});
</script>
</body>
</html>