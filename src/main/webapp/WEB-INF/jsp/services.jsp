<%@include file="fragments/header.jspf" %>

<h1><spring:message code="services"/></h1>

<table>
	<tr>
		<th><spring:message code="name"/></th>
		<th><spring:message code="service.category"/></th>
		<th><spring:message code="hits"/></th>
		<th><spring:message code="service.price"/></th>
		<th><spring:message code="action"/></th>
	</tr>
	<c:choose>
	<c:when test="${fn:length(services) == 0}" >
		<tr>
			<td colspan="5"><span class="emptyList"><spring:message code="service.noServices"/></span></td>
		</tr>
	</c:when>
	<c:otherwise>
	</c:otherwise>
	</c:choose>
	<c:forEach var="service" items="${services}" varStatus="index" >
	<tr <c:if test="${index.index % 2 == 1}">class="odd"</c:if> >
		<td>${service.name}</td>
		<td>${service.category}</td>
		<td>${service.hits}</td>
		<td><span class="number"><fmt:formatNumber value="${service.price}" type="currency" currencySymbol="&euro;" minFractionDigits="2" /></span></td>
		<td align="center"><a href="viewService.html?serviceId=${service.id}"><spring:message code="view"/></a> <a href="editService.html?serviceId=${service.id}"><spring:message code="edit"/></a> <a href="removeService.html?serviceId=${service.id}"><spring:message code="remove"/></a></td>
	</tr>
	</c:forEach> 
</table>
<br />
<p class="smallText">
	<spring:message code="service.add.label"/>: <a href="addService.html"><spring:message code="service.add"/></a>
</p>








<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - Services';
	});
</script>
</body>
</html>