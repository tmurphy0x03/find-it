<%@include file="fragments/header.jspf" %>

<h1><spring:message code="products"/></h1>

<table>
	<tr>
		<th><spring:message code="name"/></th>
		<th><spring:message code="product.category"/></th>
		<th><spring:message code="hits"/></th>
		<th><spring:message code="product.price"/></th>
		<th><spring:message code="action"/></th>
	</tr>
	<c:choose>
	<c:when test="${fn:length(products) == 0}" >
		<tr>
			<td colspan="5"><span class="emptyList"><spring:message code="product.noProducts"/></span></td>
		</tr>
	</c:when>
	<c:otherwise>
	</c:otherwise>
	</c:choose>
	<c:forEach var="product" items="${products}" varStatus="index" >
	<tr <c:if test="${index.index % 2 == 1}">class="odd"</c:if> >
		<td>${product.name}</td>
		<td>${product.category}</td>
		<td>${product.hits}</td>
		<td><span class="number"><fmt:formatNumber value="${product.price}" type="currency" currencySymbol="&euro;" minFractionDigits="2" /></span></td>
		<td align="center"><a href="viewProduct.html?productId=${product.id}"><spring:message code="view"/></a> <a href="editProduct.html?productId=${product.id}"><spring:message code="edit"/></a> <a href="removeProduct.html?productId=${product.id}"><spring:message code="remove"/></a></td>
	</tr>
	</c:forEach> 
</table>
<br />
<p class="smallText">
	<spring:message code="product.add.label"/>: <a href="addProduct.html"><spring:message code="product.add"/></a>
</p>








<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - Products';
	});
</script>
</body>
</html>