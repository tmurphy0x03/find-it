<%@include file="fragments/header.jspf" %>

<h1>${retailerName}</h1>
<h2>${product.name}</h2>
	<div id="details">
	<fieldset>
		<legend><spring:message code="product.info"/></legend>
		<c:if test="${product.name != ''}">
		<p>
			<label><spring:message code="product.name"/>: </label><label class="details">${product.name}</label>
		</p>
		</c:if>
		<c:if test="${product.price != ''}">
		<p>
			<label><spring:message code="product.price"/>: </label><label class="details"><fmt:formatNumber value="${product.price}" type="currency" currencySymbol="&euro;" minFractionDigits="2" /></label>
		</p>
		</c:if>
		<c:if test="${product.category != ''}">
		<p>
			<label><spring:message code="product.category"/>: </label><label class="details">${product.category}</label>
		</p>
		</c:if>
		<c:if test="${product.description != ''}">
		<p>
			<label><spring:message code="product.description"/>: </label><label class="details">${product.description}</label>
		</p>
		</c:if>
		<c:if test="${product.branchNames != ''}">
		<p>
			<label><spring:message code="branches"/>: </label><label class="details">${product.branchNames}</label>
		</p>
		</c:if>
		<c:if test="${product.hits != ''}">
		<p>
			<label><spring:message code="hits"/>: </label><label class="details">${product.hits}</label>
		</p>
		</c:if>
		<c:if test="${product.retailerName != ''}">
		<p>
			<label><spring:message code="retailer.name"/>: </label><label class="details">${product.retailerName}</label>
		</p>
		</c:if>
	</fieldset>
	<br />
	<fieldset>
	<legend id="branches" ><spring:message code="branches"/></legend>
		<c:forEach var="branch" items="${branches}" varStatus="index" >
			<br />
			<a href="#branches" class="headerLink" onclick="showBranch('${index.index}', '${branch.longitude}', '${branch.latitude}', '${fn:length(branches)}')">${branch.name}</a>
			<div id="${index.index}" style="display: none;">
			<div id="branchInfo">
			<c:if test="${branch.name != ''}">
			<p>
				<label><spring:message code="branch.name"/>: </label><label class="details">${branch.name}</label>
			</p>
			</c:if>
			<c:if test="${branch.phoneNumber != ''}">
			<p>
				<label><spring:message code="phoneNumber"/>: </label><label class="details">${branch.phoneNumber}</label>
			</p>
			</c:if>
			<c:if test="${branch.property != ''}">
			<p>
				<label><spring:message code="property"/>: </label><label class="details">${branch.property}</label>
			</p>
			</c:if>
			<c:if test="${branch.address1 != ''}">
			<p>
				<label><spring:message code="address.1"/>: </label><label class="details">${branch.address1}</label>
			</p>
			</c:if>
			<c:if test="${branch.address2 != ''}">
			<p>
				<label><spring:message code="address.2"/>: </label><label class="details">${branch.address2}</label>
			</p>
			</c:if>
			<c:if test="${branch.address3 != ''}">
			<p>
				<label><spring:message code="address.3"/>: </label><label class="details">${branch.address3}</label>
			</p>
			</c:if>
			<c:if test="${branch.location != ''}">
			<p>
				<label><spring:message code="location"/>: </label><label class="details">${branch.location}</label>
			</p>
			</c:if>
			<c:if test="${branch.region != ''}">
			<p>
				<label><spring:message code="region"/>: </label><label class="details">${branch.region}</label>
			</p>
			</c:if>
			<c:if test="${branch.postcode != ''}">
			<p>
				<label><spring:message code="postcode"/>: </label><label class="details">${branch.postcode}</label>
			</p>
			</c:if>
			<c:if test="${branch.country != ''}">
			<p>
				<label><spring:message code="country"/>: </label><label class="details">${branch.country}</label>
			</p>
			</c:if>
			</div>
			<br />
			<div id="map${index.index}" class="map" ></div>
			</div>
	</c:forEach>
	</fieldset>
	<br />
	<fieldset>
	<legend><spring:message code="socialNetwork"/></legend>
		<div align="center">
			<span class="facebook">
				<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="" layout="box_count" show_faces="true" width="100" action="recommend" font=""></fb:like>
			</span>
			<span class="twitter">
				<a href="http://twitter.com/share" class="twitter-share-button" data-count="vertical">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
			</span>
		</div>
	</fieldset>
	
	<br />
	</div>
	<br />
<br />


<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - View Product';
	});
	
	function showBranch(id, longitude, latitude, branches){
		if($("#"+id).is(":hidden")){
			$("#"+id).slideDown();
			viewMap(id, longitude, latitude);
		}
		else {
			$("#"+id).slideUp();
		}
		
		for(i = 0 ; i < branches ; i++){
			if(i != id && $("#"+i).is(":visible")){
				$("#"+i).slideUp();
			}
		}
	}
	
	function viewMap(id, longitude, latitude){
		/* Setup the Map */
		var map = new GMap2(document.getElementById("map"+id));
		map.setMapType(G_HYBRID_MAP);
		map.addControl(new GLargeMapControl());
		map.addControl(new GMapTypeControl());
		map.enableScrollWheelZoom();
		
        /* Gets the Longitued and Latitude values */
		if(latitude != null && longitude != null){
			map.clearOverlays();
			map.addOverlay(new GMarker(new GLatLng(latitude, longitude), 12));
			map.setCenter(new GLatLng(latitude, longitude), 16);
		}
	}
		
</script>
<script src="http://maps.google.com/maps?file=api&v=3&key=ABQIAAAAcuBDjlkW6c6EJ4el3KM_8xSc09D1umn1OR-kfLK_fM6PkjzgThStBVznrYdC8qDotONirH73e_-wwQ" type="text/javascript"></script>

</body>
</html>