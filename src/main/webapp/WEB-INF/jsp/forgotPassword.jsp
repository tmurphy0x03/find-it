<%@include file="fragments/header.jspf" %>

<div id="wrapper">
<h1><spring:message code="forgotpassword"/></h1>
<c:choose>
<c:when test="${success != null}">
	<p class="smallText">
		<spring:message code="forgotpassword.email.check"/>
		<br />
		<br />
		<br />
	</p>
</c:when>
<c:otherwise>
<form id="forgotPasswordForm" name="forgotPasswordForm" method="POST">
	<fieldset>
		<legend><spring:message code="account.details"/></legend>
		<p>
			<label for="username"><spring:message code="username"/>: </label><input id="username" name="username" maxLength="24" />
			<c:if test="${error != null}"><label for="username"></label><span class="error">${error}</span></c:if>
		</p>
		<p class="smallText">
			<spring:message code="forgotpassword.email.label"/>
		</p>
	</fieldset>
	<br />
	<div align="center">
		<input id="submit" type="submit" value="<spring:message code="forgotpassword.get"/>" />
	</div>
	<br />
	<br />

</form>
</c:otherwise>
</c:choose>

</div>
<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - Forgot Password';
	});
</script>
</body>
</html>