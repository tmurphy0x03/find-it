<%@include file="fragments/header.jspf" %>

<c:choose>
<c:when test="${action == 'add'}">
	<h1><spring:message code="branch.add"/></h1>
</c:when>
<c:otherwise>
	<h1><spring:message code="branch.edit"/></h1>
</c:otherwise>
</c:choose>
<form:form id="addBranchForm" name="addBranchForm" commandName="branch" method="post">
	<fieldset>
		<legend><spring:message code="branch.address"/></legend>
		<p>
			<form:errors path="name" cssClass="error" />
			<label for="name"><spring:message code="branch.name"/>: *</label><form:input id="name" path="name" maxlength="24" />
		</p>
		<p>
			<form:errors path="phoneNumber" cssClass="error" />
			<label for="phoneNumber"><spring:message code="phoneNumber"/>: *</label><form:input id="phoneNumber" path="phoneNumber" maxlength="24" />
		</p>
		<p>
			<form:errors path="email" cssClass="error" />
			<label for="email"><spring:message code="email"/>: *</label><form:input id="email" path="email" maxlength="24" />
		</p>
		<p>
			<form:errors path="property" cssClass="error" />
			<label for="property"><spring:message code="property"/>: *</label><form:input id="property" path="property" maxlength="24" />
		</p>
		<p>
			<form:errors path="address1" cssClass="error" />
			<label for="address1"><spring:message code="address.1"/>: *</label><form:input id="address1" path="address1" maxlength="25" />
		</p>
		<p>
			<form:errors path="address2" cssClass="error" />
			<label for="address2"><spring:message code="address.2"/>: </label><form:input id="address2" path="address2" maxlength="24" />
		</p>
		<p>
			<form:errors path="address3" cssClass="error" />
			<label for="address3"><spring:message code="address.3"/>: </label><form:input id="address3" path="address3" maxlength="24" />
		</p>
		<p>
			<form:errors path="location" cssClass="error" />
			<label for="location"><spring:message code="location"/>: *</label><form:input id="location" path="location" maxlength="24" />
		</p>
		<p>
			<form:errors path="region" cssClass="error" />
			<label for="region"><spring:message code="region"/>: *</label><form:input id="region" path="region" maxlength="24" />
		</p>
		<p>
			<label for="postcode"><spring:message code="postcode"/>: </label><form:input id="postcode" path="postcode" maxlength="24" />
		</p>
		<p>
			<form:errors path="country" cssClass="error" />
			<label for="country"><spring:message code="country"/>: *</label><form:input id="country" path="country" maxlength="24" />
		</p>
		<form:hidden id="latitude" path="latitude" />
		<form:hidden id="longitude" path="longitude" />
		<form:hidden id="id" path="id" />
		<form:hidden id="retailerName" path="retailerName" />
		<form:hidden id="retailerUsername" path="retailerUsername" />
	</fieldset>
	
	<br />
	<fieldset>
	<legend><spring:message code="map"/></legend>
	<spring:message code="map.addPlaceholder"/><br /><br />
	<form:errors path="latitude" cssClass="map_error" />
	<div align="center" id="map" class="map" ></div>
	</fieldset>
	<br />
	<div align="center">
		<c:choose>
		<c:when test="${action == 'add'}">
			<input id="submit" type="submit" value="<spring:message code="branch.add"/>" />
		</c:when>
		<c:otherwise>
			<input id="submit" type="submit" value="<spring:message code="branch.update"/>" />
		</c:otherwise>
		</c:choose>
	</div>
</form:form>
<br />


<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		if(${action == 'add'}){
			createMap();
			document.title = 'FindIt - Add Branch';
		}
		else {
			updateMap();
			document.title = 'FindIt - Edit Branch';
		}
	});
	
	/* Load Google Maps */
	function createMap(){
		/* Setup the Map */
		var map = new GMap2(document.getElementById("map"));
		map.setMapType(G_HYBRID_MAP);
		map.addControl(new GLargeMapControl());
		map.addControl(new GMapTypeControl());
		map.enableScrollWheelZoom();
        map.setCenter(new GLatLng(53.349731569053155, -8.260318756103516), 6); //set the center to Ireland
		
		/* Check if the users browser supports HTML5 Geolocation */
		if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
            	if(position != null){
            		map.setCenter(new GLatLng(position.coords.latitude, position.coords.longitude), 14); //set the center to current location
            	}
            });
        }
		
		/* Add a listener for when a users marks their branch location */
		GEvent.addListener(map, "click", function(overlay, point){
			map.clearOverlays();
			if (point) {
				map.addOverlay(new GMarker(point));
				document.getElementById("latitude").value = point.lat();
				document.getElementById("longitude").value = point.lng();
			}
		});
	}
	
	function updateMap(){
		/* Setup the Map */
		var map = new GMap2(document.getElementById("map"));
		map.setMapType(G_HYBRID_MAP);
		map.addControl(new GLargeMapControl());
		map.addControl(new GMapTypeControl());
		map.enableScrollWheelZoom();
        map.setCenter(new GLatLng(53.349731569053155, -8.260318756103516), 6); //set the center to Ireland
		
        /* Gets the Longitued and Latitude values */
		if(document.getElementById("latitude").value.length > 0 && document.getElementById("longitude").value.length > 0){
			map.clearOverlays();
			map.addOverlay(new GMarker(new GLatLng(document.getElementById("latitude").value, document.getElementById("longitude").value), 14));
			map.setCenter(new GLatLng(document.getElementById("latitude").value, document.getElementById("longitude").value), 14);
		}
		
		/* Add a listener for when a users marks their branch location */
		GEvent.addListener(map, "click", function(overlay, point){
			map.clearOverlays();
			if (point) {
				map.addOverlay(new GMarker(point));
				document.getElementById("latitude").value = point.lat();
				document.getElementById("longitude").value = point.lng();
			}
		});
	}
		
</script>
<script src="http://maps.google.com/maps?file=api&v=3&key=ABQIAAAAcuBDjlkW6c6EJ4el3KM_8xSc09D1umn1OR-kfLK_fM6PkjzgThStBVznrYdC8qDotONirH73e_-wwQ" type="text/javascript"></script>

</body>
</html>