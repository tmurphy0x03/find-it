<%@include file="fragments/header.jspf" %>

<div id="wrapper">
<h1><spring:message code="login"/></h1>
<form:form id="loginForm" commandName="user" method="post">
	<fieldset>
		<legend><spring:message code="account.details"/></legend>
		<p>
			<form:errors path="username" cssClass="error" />
			<label for="uname"><spring:message code="username"/>: </label><form:input id="uname" path="username" maxlength="24" />
		</p>
		<p>
			<form:errors path="password" cssClass="error" />
			<label for="pword"><spring:message code="password"/>: </label><form:password id="pword" path="password" maxlength="24" />
		</p>
	</fieldset>
	<br />
	<div align="center">
		<input id="submit" type="submit" value="<spring:message code="login"/>" />
	</div>
	<br />
	<br />
	<p class="smallText">
		<spring:message code="account.create.label"/>: <a href="createAccount.html"><spring:message code="account.create"/></a>
	</p>
	<p class="smallText">
		<spring:message code="forgotpassword.label"/> <a href="forgotPassword.html"><spring:message code="forgotpassword"/></a>
	</p>

</form:form>
</div>
<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - Login';
	});
</script>
</body>
</html>