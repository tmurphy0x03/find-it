<%@include file="fragments/header.jspf" %>

<h1><spring:message code="branches"/></h1>
	<div class="results">
		<c:choose>
		<c:when test="${fn:length(branches) == 0}" >
			<div class="emptyList"><spring:message code="search.noBranches"/></div>
		</c:when>
		<c:otherwise>
		
		<c:forEach var="branch" items="${branches}" varStatus="index" >
		<div <c:choose><c:when test="${index.index % 2 == 1}">class="odd"</c:when><c:otherwise>class="even"</c:otherwise></c:choose> >
				<a href="viewBranch.html?branchId=${branch.id}"><span class="result_title">${branch.retailerName} - ${branch.name}</span></a><br />
				<c:if test="${branch.property != ''}">${branch.property} </c:if><c:if test="${branch.address1 != ''}">${branch.address1}</c:if><c:if test="${branch.address2 != ''}">, ${branch.address2}</c:if><c:if test="${branch.address3 != ''}">, ${branch.address3}</c:if><c:if test="${branch.location != ''}">, ${branch.location}</c:if><br />
				<c:if test="${branch.region != ''}">${branch.region}</c:if><c:if test="${branch.postcode != ''}"> ${branch.postcode}</c:if><c:if test="${branch.country != ''}">, ${branch.country}.</c:if>
		</div>
		</c:forEach> 
		</c:otherwise>
		</c:choose>
	</div>
<br />
<p class="smallText">
	<spring:message code="search.again.label"/>: <a href="advancedSearch.html"><spring:message code="search.again"/></a>
</p>








<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - Branch Search Results';
	});
</script>
</body>
</html>