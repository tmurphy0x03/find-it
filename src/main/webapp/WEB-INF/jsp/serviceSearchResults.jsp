<%@include file="fragments/header.jspf" %>

<h1><spring:message code="services"/></h1>
	<div class="results">
		<c:choose>
		<c:when test="${fn:length(services) == 0}" >
			<div class="emptyList"><spring:message code="search.noServices"/></div>
		</c:when>
		<c:otherwise>
		
		<c:forEach var="service" items="${services}" varStatus="index" >
		<div <c:choose><c:when test="${index.index % 2 == 1}">class="odd"</c:when><c:otherwise>class="even"</c:otherwise></c:choose> >
				<a href="viewService.html?serviceId=${service.id}"><span class="result_title">${service.name} - ${service.retailerName}</span></a><br />
				<c:if test="${service.category != ''}">${service.category}</c:if>
				<c:if test="${service.description != ''}"><br />${service.description}</c:if>
				<c:if test="${service.price != ''}"><span class="number"><fmt:formatNumber value="${service.price}" type="currency" currencySymbol="&euro;" minFractionDigits="2" /></span></c:if>
		</div>
		</c:forEach> 
		</c:otherwise>
		</c:choose>
	</div>
<br />
<p class="smallText">
	<spring:message code="search.again.label"/>: <a href="advancedSearch.html"><spring:message code="search.again"/></a>
</p>








<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - Service Search Results';
	});
</script>
</body>
</html>