<%@include file="fragments/header.jspf" %>

<h1><spring:message code="products"/></h1>
	<div class="results">
		<c:choose>
		<c:when test="${fn:length(products) == 0}" >
			<div class="emptyList"><spring:message code="search.noProducts"/></div>
		</c:when>
		<c:otherwise>
		
		<c:forEach var="product" items="${products}" varStatus="index" >
		<div <c:choose><c:when test="${index.index % 2 == 1}">class="odd"</c:when><c:otherwise>class="even"</c:otherwise></c:choose> >
				<a href="viewProduct.html?productId=${product.id}"><span class="result_title">${product.name} - ${product.retailerName}</span></a><br />
				<c:if test="${product.category != ''}">${product.category}</c:if>
				<c:if test="${product.description != ''}"><br />${product.description}</c:if>
				<c:if test="${product.price != ''}"><span class="number"><fmt:formatNumber value="${product.price}" type="currency" currencySymbol="&euro;" minFractionDigits="2" /></span></c:if>
		</div>
		</c:forEach> 
		</c:otherwise>
		</c:choose>
	</div>
<br />
<p class="smallText">
	<spring:message code="search.again.label"/>: <a href="advancedSearch.html"><spring:message code="search.again"/></a>
</p>








<%@include file="fragments/footer.jspf" %>

<script type="text/javascript">
	$(document).ready(function() {
		document.title = 'FindIt - Product Search Results';
	});
</script>
</body>
</html>