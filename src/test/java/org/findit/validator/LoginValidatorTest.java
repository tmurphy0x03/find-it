/**
 *
 * @author Anthony Murphy
 */

package org.findit.validator;

import org.findit.manager.RetailerManager;
import org.findit.manager.UserConverter;
import org.findit.manager.UserManager;
import org.findit.model.Retailer;
import org.findit.model.User;
import org.jasypt.util.text.TextEncryptor;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BindingResult;

public class LoginValidatorTest {
	private LoginValidator loginValidator;
	private BindingResult result;
	private UserManager userManager;
	private Mockery mockery;
	private TextEncryptor basicTextEncryptor;
	private UserConverter userConverter;
	private RetailerManager retailerManager;
	
	@Before
	public void setup(){
		loginValidator = new LoginValidator();
		mockery = new Mockery();
		result = mockery.mock(BindingResult.class);
		userManager = mockery.mock(UserManager.class);
		basicTextEncryptor = mockery.mock(TextEncryptor.class);
		userConverter = mockery.mock(UserConverter.class);
		retailerManager = mockery.mock(RetailerManager.class);
	}
	
	@Test
	public void validationCorrect(){
		final User user = new User();
		user.setUsername("tony");
		user.setPassword("password");
		final Retailer retailer = new Retailer();
		retailer.setUsername("tony");
		retailer.setPassword("password");
		loginValidator.setUserManager(userManager);
		loginValidator.setBasicTextEncryptor(basicTextEncryptor);
		loginValidator.setUserConverter(userConverter);
		loginValidator.setRetailerManager(retailerManager);
		
		mockery.checking(new Expectations() {{
			oneOf (userConverter).convert(user);
			will(returnValue(retailer));
			
			oneOf (userManager).getUserByUsername(user.getUsername());
			will(returnValue(user));
			
			oneOf (retailerManager).getRetailerByUsername(user.getUsername());
			will(returnValue(null));
			
			oneOf (basicTextEncryptor).decrypt(user.getPassword());
			will(returnValue(user.getPassword()));
			
			oneOf (result).hasErrors();
			will(returnValue(false));
		}});
		
		loginValidator.validate(user, result);
		Assert.assertFalse(result.hasErrors());
	}
	
	@Test
	public void blankUsernameAndPassword(){
		final User user = new User();
		user.setUsername("");
		user.setPassword("");
		final Retailer retailer = new Retailer();
		retailer.setUsername("");
		retailer.setPassword("");
		loginValidator.setUserManager(userManager);
		loginValidator.setBasicTextEncryptor(basicTextEncryptor);
		loginValidator.setUserConverter(userConverter);
		loginValidator.setRetailerManager(retailerManager);
		
		mockery.checking(new Expectations() {{
			oneOf (userConverter).convert(user);
			will(returnValue(retailer));
			
			oneOf (userManager).getUserByUsername(user.getUsername());
			will(returnValue(user));
			
			oneOf (retailerManager).getRetailerByUsername(user.getUsername());
			will(returnValue(null));
			
			oneOf (result).rejectValue("username", "requiredField");
			
			oneOf (result).rejectValue("password", "requiredField");
			
			oneOf (basicTextEncryptor).decrypt(user.getPassword());
			will(returnValue(user.getPassword()));
			
			oneOf (result).hasErrors();
			will(returnValue(true));
		}});
		
		loginValidator.validate(user, result);
		Assert.assertTrue(result.hasErrors());
	}
	
	@Test
	public void usernameNotExistsTest(){
		final User user = new User();
		user.setUsername("tony");
		user.setPassword("password");
		final Retailer retailer = new Retailer();
		retailer.setUsername("tony");
		retailer.setPassword("password");
		loginValidator.setUserManager(userManager);
		loginValidator.setBasicTextEncryptor(basicTextEncryptor);
		loginValidator.setUserConverter(userConverter);
		loginValidator.setRetailerManager(retailerManager);
		
		mockery.checking(new Expectations() {{
			oneOf (userConverter).convert(user);
			will(returnValue(retailer));
			
			oneOf (userManager).getUserByUsername(user.getUsername());
			will(returnValue(null));
			
			oneOf (retailerManager).getRetailerByUsername(user.getUsername());
			will(returnValue(null));
			
			oneOf (result).rejectValue("username", "username.notexists");
			
			oneOf (result).hasErrors();
			will(returnValue(true));
		}});
		
		loginValidator.validate(user, result);
		Assert.assertTrue(result.hasErrors());
	}
	
	@Test
	public void invalidPasswordTest(){
		final User user = new User();
		user.setUsername("tony");
		user.setPassword("password");
		final Retailer retailer = new Retailer();
		retailer.setUsername("tony");
		retailer.setPassword("password");
		loginValidator.setUserManager(userManager);
		loginValidator.setBasicTextEncryptor(basicTextEncryptor);
		loginValidator.setUserConverter(userConverter);
		loginValidator.setRetailerManager(retailerManager);
		
		mockery.checking(new Expectations() {{
			oneOf (userConverter).convert(user);
			will(returnValue(retailer));
			
			oneOf (userManager).getUserByUsername(user.getUsername());
			will(returnValue(user));
			
			oneOf (retailerManager).getRetailerByUsername(user.getUsername());
			will(returnValue(null));
			
			oneOf (basicTextEncryptor).decrypt(user.getPassword());
			will(returnValue(user.getPassword()+"invalid"));
			
			oneOf (result).rejectValue("password", "password.invalid");
			
			oneOf (result).hasErrors();
			will(returnValue(true));
		}});
		
		loginValidator.validate(user, result);
		Assert.assertTrue(result.hasErrors());
	}
}
