/**
 *
 * @author Anthony Murphy
 */

package org.findit.validator;

import org.findit.manager.RetailerManager;
import org.findit.model.Retailer;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BindingResult;

public class RetailerValidatorTest {
	private RetailerValidator retailerValidator;
	private BindingResult result;
	private RetailerManager retailerManager;
	private Mockery mockery;
	
	@Before
	public void setup(){
		retailerValidator = new RetailerValidator();
		mockery = new Mockery();
		result = mockery.mock(BindingResult.class);
		retailerManager = mockery.mock(RetailerManager.class);
	}
	
	@Test
	public void validationCorrect(){
		final Retailer retailer = new Retailer();
		retailer.setName("Findit");
		retailer.setEmail("tonymurphy1989@gmail.com");
		retailer.setUsername("tony");
		retailer.setPassword("password");
		retailer.setConfirmPassword("password");
		retailerValidator.setRetailerManager(retailerManager);
		
		mockery.checking(new Expectations() {{
			oneOf (retailerManager).exists(retailer.getUsername());
			will(returnValue(false));
			
			oneOf (result).hasErrors();
			will(returnValue(false));
		}});
		
		retailerValidator.validate(retailer, result);
		Assert.assertFalse(result.hasErrors());
	}
	
	@Test
	public void blankFieldsTest(){
		final Retailer retailer = new Retailer();
		retailer.setName("");
		retailer.setEmail("");
		retailer.setUsername("");
		retailer.setPassword("");
		retailer.setConfirmPassword("");
		retailerValidator.setRetailerManager(retailerManager);
		
		mockery.checking(new Expectations() {{
			oneOf (retailerManager).exists(retailer.getUsername());
			will(returnValue(false));
			
			oneOf (result).rejectValue("name", "requiredField");
			oneOf (result).rejectValue("email", "requiredField");
			oneOf (result).rejectValue("username", "requiredField");
			oneOf (result).rejectValue("password", "requiredField");
			oneOf (result).rejectValue("confirmPassword", "requiredField");
			
			oneOf (result).hasErrors();
			will(returnValue(true));
		}});
		
		retailerValidator.validate(retailer, result);
		Assert.assertTrue(result.hasErrors());
	}
	
	@Test
	public void emailTestInvalid(){
		final Retailer retailer = new Retailer();
		retailer.setName("Findit");
		retailer.setEmail("email");
		retailer.setUsername("tony");
		retailer.setPassword("password");
		retailer.setConfirmPassword("password");
		retailerValidator.setRetailerManager(retailerManager);
		
		mockery.checking(new Expectations() {{
			oneOf (retailerManager).exists(retailer.getUsername());
			will(returnValue(false));
			
			oneOf (result).rejectValue("email", "email.invalid");
			
			oneOf (result).hasErrors();
			will(returnValue(true));
		}});
		
		retailerValidator.validate(retailer, result);
		Assert.assertTrue(result.hasErrors());
	}
	
	@Test
	public void usernameAlreadyExists(){
		final Retailer retailer = new Retailer();
		retailer.setName("Findit");
		retailer.setEmail("tonymurphy1989@gmail.com");
		retailer.setUsername("tony");
		retailer.setPassword("password");
		retailer.setConfirmPassword("password");
		retailerValidator.setRetailerManager(retailerManager);
		
		mockery.checking(new Expectations() {{
			oneOf (retailerManager).exists(retailer.getUsername());
			will(returnValue(true));
			
			oneOf (result).rejectValue("username", "username.exists");
			
			oneOf (result).hasErrors();
			will(returnValue(true));
		}});
		
		retailerValidator.validate(retailer, result);
		Assert.assertTrue(result.hasErrors());
	}
	
	@Test
	public void passwordsDontMatchTest(){
		final Retailer retailer = new Retailer();
		retailer.setName("Findit");
		retailer.setEmail("tonymurphy1989@gmail.com");
		retailer.setUsername("tony");
		retailer.setPassword("password");
		retailer.setConfirmPassword("passwordInvalid");
		retailerValidator.setRetailerManager(retailerManager);
		
		mockery.checking(new Expectations() {{
			oneOf (retailerManager).exists(retailer.getUsername());
			will(returnValue(false));
			
			oneOf (result).rejectValue("password", "password.notEqual");
			oneOf (result).rejectValue("confirmPassword", "password.notEqual");
			
			oneOf (result).hasErrors();
			will(returnValue(true));
		}});
		
		retailerValidator.validate(retailer, result);
		Assert.assertTrue(result.hasErrors());
	}
}
