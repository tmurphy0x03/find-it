/**
 *
 * @author Anthony Murphy
 */

package org.findit.validator;

import org.findit.manager.UserManager;
import org.findit.model.User;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BindingResult;

public class UserValidatorTest {
	private UserValidator userValidator;
	private BindingResult result;
	private UserManager userManager;
	private Mockery mockery;
	
	@Before
	public void setup(){
		userValidator = new UserValidator();
		mockery = new Mockery();
		result = mockery.mock(BindingResult.class);
		userManager = mockery.mock(UserManager.class);
	}
	
	@Test
	public void validationCorrect(){
		final User user = new User();
		user.setFirstName("Tony");
		user.setSurname("Murphy");
		user.setEmail("tonymurphy1989@gmail.com");
		user.setUsername("tony");
		user.setPassword("password");
		user.setConfirmPassword("password");
		userValidator.setUserManager(userManager);
		
		mockery.checking(new Expectations() {{
			oneOf (userManager).exists(user.getUsername());
			will(returnValue(false));
			
			oneOf (result).hasErrors();
			will(returnValue(false));
		}});
		
		userValidator.validate(user, result);
		Assert.assertFalse(result.hasErrors());
	}
	
	@Test
	public void blankFieldsTest(){
		final User user = new User();
		user.setFirstName("");
		user.setSurname("");
		user.setEmail("");
		user.setUsername("");
		user.setPassword("");
		user.setConfirmPassword("");
		userValidator.setUserManager(userManager);
		
		mockery.checking(new Expectations() {{
			oneOf (userManager).exists(user.getUsername());
			will(returnValue(false));
			
			oneOf (result).rejectValue("firstName", "requiredField");
			oneOf (result).rejectValue("surname", "requiredField");
			oneOf (result).rejectValue("email", "requiredField");
			oneOf (result).rejectValue("username", "requiredField");
			oneOf (result).rejectValue("password", "requiredField");
			oneOf (result).rejectValue("confirmPassword", "requiredField");
			
			oneOf (result).hasErrors();
			will(returnValue(true));
		}});
		
		userValidator.validate(user, result);
		Assert.assertTrue(result.hasErrors());
	}
	
	@Test
	public void emailTestInvalid(){
		final User user = new User();
		user.setFirstName("Tony");
		user.setSurname("Murphy");
		user.setEmail("email");
		user.setUsername("tony");
		user.setPassword("password");
		user.setConfirmPassword("password");
		userValidator.setUserManager(userManager);
		
		mockery.checking(new Expectations() {{
			oneOf (userManager).exists(user.getUsername());
			will(returnValue(false));
			
			oneOf (result).rejectValue("email", "email.invalid");
			
			oneOf (result).hasErrors();
			will(returnValue(true));
		}});
		
		userValidator.validate(user, result);
		Assert.assertTrue(result.hasErrors());
	}
	
	@Test
	public void usernameAlreadyExists(){
		final User user = new User();
		user.setFirstName("Tony");
		user.setSurname("Murphy");
		user.setEmail("tonymurphy1989@gmail.com");
		user.setUsername("tony");
		user.setPassword("password");
		user.setConfirmPassword("password");
		userValidator.setUserManager(userManager);
		
		mockery.checking(new Expectations() {{
			oneOf (userManager).exists(user.getUsername());
			will(returnValue(true));
			
			oneOf (result).rejectValue("username", "username.exists");
			
			oneOf (result).hasErrors();
			will(returnValue(true));
		}});
		
		userValidator.validate(user, result);
		Assert.assertTrue(result.hasErrors());
	}
	
	@Test
	public void passwordsDontMatchTest(){
		final User user = new User();
		user.setFirstName("Tony");
		user.setSurname("Murphy");
		user.setEmail("tonymurphy1989@gmail.com");
		user.setUsername("tony");
		user.setPassword("password");
		user.setConfirmPassword("passwordInvalid");
		userValidator.setUserManager(userManager);
		
		mockery.checking(new Expectations() {{
			oneOf (userManager).exists(user.getUsername());
			will(returnValue(false));
			
			oneOf (result).rejectValue("password", "password.notEqual");
			oneOf (result).rejectValue("confirmPassword", "password.notEqual");
			
			oneOf (result).hasErrors();
			will(returnValue(true));
		}});
		
		userValidator.validate(user, result);
		Assert.assertTrue(result.hasErrors());
	}
}
