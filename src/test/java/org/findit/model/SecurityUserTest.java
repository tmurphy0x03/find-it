/**
 *
 * @author Anthony Murphy
 */

package org.findit.model;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class SecurityUserTest {
	
	@Before
	public void setup(){

	}
	
	@Test
	public void testGettersAndSetters(){
		String validUsername = "murpha74";
		String validPassword = "password";
		SecurityUser user = new SecurityUser(validUsername, validPassword, true, true, true, true, null);
		
		String username = user.getUsername();
		String password = user.getPassword();
		
		Assert.assertEquals(validUsername, username);
		Assert.assertEquals(validPassword, password);
		Assert.assertTrue(user.isAccountNonExpired());
		Assert.assertTrue(user.isAccountNonLocked());
		Assert.assertTrue(user.isCredentialsNonExpired());
		Assert.assertFalse(user.isEnabled());
	}
	
	@Test
	public void equalsTest(){
		String validUsername = "murpha74";
		String validPassword = "password";
		SecurityUser user1 = new SecurityUser(validUsername, validPassword, false, false, false, false, null);
		
		SecurityUser user2 = new SecurityUser(validUsername, validPassword, false, false, false, false, null);
		
		Assert.assertEquals(user1, user2);
		Assert.assertTrue(user1.equals(user2));
	}
	
	@Test
	public void equalsTestNotEqual(){
		String validUsername = "murpha74";
		String validPassword = "password";
		SecurityUser user1 = new SecurityUser(validUsername, validPassword, false, false, false, false, null);
		
		SecurityUser user2 = new SecurityUser(validUsername, validPassword, true, true, false, false, null);
		
		Assert.assertNotSame(user1, user2);
		Assert.assertFalse(user1.equals(user2));
	}
}
