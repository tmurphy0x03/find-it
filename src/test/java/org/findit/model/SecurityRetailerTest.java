/**
 *
 * @author Anthony Murphy
 */

package org.findit.model;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class SecurityRetailerTest {
	
	@Before
	public void setup(){

	}
	
	@Test
	public void testGettersAndSetters(){
		String validUsername = "murpha74";
		String validPassword = "password";
		SecurityRetailer retailer = new SecurityRetailer(validUsername, validPassword, true, true, true, true, null);
		
		String username = retailer.getUsername();
		String password = retailer.getPassword();
		
		Assert.assertEquals(validUsername, username);
		Assert.assertEquals(validPassword, password);
		Assert.assertTrue(retailer.isAccountNonExpired());
		Assert.assertTrue(retailer.isAccountNonLocked());
		Assert.assertTrue(retailer.isCredentialsNonExpired());
		Assert.assertTrue(retailer.isEnabled());
	}
	
	@Test
	public void equalsTest(){
		String validUsername = "murpha74";
		String validPassword = "password";
		SecurityRetailer retailer1 = new SecurityRetailer(validUsername, validPassword, false, false, false, false, null);
		
		SecurityRetailer retailer2 = new SecurityRetailer(validUsername, validPassword, false, false, false, false, null);
		
		Assert.assertEquals(retailer1, retailer2);
		Assert.assertTrue(retailer1.equals(retailer2));
	}
	
	@Test
	public void equalsTestNotEqual(){
		String validUsername = "murpha74";
		String validPassword = "password";
		SecurityRetailer retailer1 = new SecurityRetailer(validUsername, validPassword, false, false, false, false, null);
		
		SecurityRetailer retailer2 = new SecurityRetailer(validUsername, validPassword, true, true, false, false, null);
		
		Assert.assertNotSame(retailer1, retailer2);
		Assert.assertFalse(retailer1.equals(retailer2));
	}
}
