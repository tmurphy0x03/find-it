/**
 *
 * @author Anthony Murphy
 */

package org.findit.model;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class RetailerTest {
	private Retailer retailer;
	
	@Before
	public void setup(){
		retailer = new Retailer();
	}
	
	@Test
	public void testGettersAndSetters(){
		String validName = "findit";
		String validEmail = "anthony.murphy74@mail.dcu.ie";
		String validUsername = "murpha74";
		String validPassword = "password";
		String validConfirmPassword = validPassword;
		
		retailer.setName("findit");
		retailer.setEmail("anthony.murphy74@mail.dcu.ie");
		retailer.setPassword("password");
		retailer.setUsername("murpha74");
		retailer.setConfirmPassword("password");

		String name = retailer.getName();
		String email = retailer.getEmail();
		String username = retailer.getUsername();
		String password = retailer.getPassword();
		String confirmPassword = retailer.getConfirmPassword();
		
		Assert.assertEquals(validName, name);
		Assert.assertEquals(validEmail, email);
		Assert.assertEquals(validUsername, username);
		Assert.assertEquals(validPassword, password);
		Assert.assertEquals(validConfirmPassword, confirmPassword);
	}
	
	@Test
	public void equalsTest(){
		retailer.setName("findit");
		retailer.setEmail("anthony.murphy74@mail.dcu.ie");
		retailer.setPassword("password");
		retailer.setUsername("murpha74");
		retailer.setConfirmPassword("password");
		
		Retailer retailer2 = new Retailer();
		retailer2.setName("findit");
		retailer2.setEmail("anthony.murphy74@mail.dcu.ie");
		retailer2.setPassword("password");
		retailer2.setUsername("murpha74");
		retailer2.setConfirmPassword("password");
		
		Assert.assertEquals(retailer, retailer2);
		Assert.assertTrue(retailer.equals(retailer2));
	}
}
