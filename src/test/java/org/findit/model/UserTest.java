/**
 *
 * @author Anthony Murphy
 */

package org.findit.model;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class UserTest {
	private User user;
	
	@Before
	public void setup(){
		user = new User();
	}
	
	@Test
	public void testGettersAndSetters(){
		String validFirstName = "tony";
		String validSurname = "murphy";
		String validEmail = "anthony.murphy74@mail.dcu.ie";
		String validUsername = "murpha74";
		String validPassword = "password";
		String validConfirmPassword = validPassword;
		
		user.setFirstName("tony");
		user.setSurname("murphy");
		user.setEmail("anthony.murphy74@mail.dcu.ie");
		user.setPassword("password");
		user.setUsername("murpha74");
		user.setConfirmPassword("password");
		user.setPromotions(true);

		String firstName = user.getFirstName();
		String surname = user.getSurname();
		String email = user.getEmail();
		String username = user.getUsername();
		String password = user.getPassword();
		String confirmPassword = user.getConfirmPassword();
		
		Assert.assertEquals(validFirstName, firstName);
		Assert.assertEquals(validSurname, surname);
		Assert.assertEquals(validEmail, email);
		Assert.assertEquals(validUsername, username);
		Assert.assertEquals(validPassword, password);
		Assert.assertEquals(validConfirmPassword, confirmPassword);
		Assert.assertTrue(user.isPromotions());
	}
	
	@Test
	public void equalsTest(){
		user.setFirstName("tony");
		user.setSurname("murphy");
		user.setEmail("anthony.murphy74@mail.dcu.ie");
		user.setPassword("password");
		user.setUsername("murpha74");
		user.setConfirmPassword("password");
		user.setPromotions(true);
		
		User user2 = new User();
		user2.setFirstName("tony");
		user2.setSurname("murphy");
		user2.setEmail("anthony.murphy74@mail.dcu.ie");
		user2.setPassword("password");
		user2.setUsername("murpha74");
		user2.setConfirmPassword("password");
		user2.setPromotions(true);
		
		Assert.assertEquals(user, user2);
		Assert.assertTrue(user.equals(user2));
	}
}
