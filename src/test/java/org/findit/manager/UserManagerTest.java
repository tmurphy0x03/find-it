/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import junit.framework.Assert;

import org.findit.dao.UserDao;
import org.findit.model.SecurityUser;
import org.findit.model.User;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.userdetails.UserDetailsService;

public class UserManagerTest {
	private UserManagerImpl userManager;
	private UserDao userDao;
	private Mockery mockery;
	private UserDetailsService userService;
	
	@Before
	public void setup(){
		userManager = new UserManagerImpl();
		mockery = new Mockery();
		userDao = mockery.mock(UserDao.class);
		userService = mockery.mock(UserDetailsService.class);
	}
	
	@Test
	public void getUserByUsernameTest(){
		final String username = "tony";
		final User validUser = new User();
		(userManager).setUserDao(userDao);
		
		mockery.checking(new Expectations() {{
			one (userDao).getUserByUsername(username);
			will(returnValue((validUser)));
		}});
		User user = userManager.getUserByUsername(username);
		
		mockery.assertIsSatisfied();
		Assert.assertEquals(validUser, user);
	}
	
	@Test
	public void updateUserTest(){
		final User validUser = new User();
		userManager.setUserDao(userDao);
		
		mockery.checking(new Expectations() {{
			one (userDao).updateUser(validUser);
			will(returnValue((validUser)));
		}});
		
		User user = userManager.updateUser(validUser);
		
		mockery.assertIsSatisfied();
		Assert.assertEquals(validUser, user);
	}
	
	@Test
	public void existsTest(){
		final String username = "tony";
		userManager.setUserDao(userDao);
		
		mockery.checking(new Expectations() {{
			one (userDao).exists(username);
			will(returnValue((true)));
		}});
		
		Boolean exists = userManager.exists(username);
		
		mockery.assertIsSatisfied();
		Assert.assertTrue(exists);
	}
	
	@Test
	public void loadUserByUsernameTest(){
		final String username = "murpha74";
		final SecurityUser user = new SecurityUser(username, null, false, false, false, false, null);
		
		userManager.setUserService(userService);
		mockery.checking(new Expectations() {{
			one (userService).loadUserByUsername(username);
			will(returnValue((user)));
		}});
		
		SecurityUser user2 = (SecurityUser) userManager.loadUserByUsername(username);
		Assert.assertEquals(user.getUsername(), user2.getUsername());
	}
}
