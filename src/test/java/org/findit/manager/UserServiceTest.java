/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import junit.framework.Assert;

import org.findit.dao.UserDao;
import org.findit.model.SecurityUser;
import org.findit.model.User;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;

public class UserServiceTest {
	private UserService userService;
	private UserDao userDao;
	private Mockery mockery;
	
	@Before
	public void setup(){
		userService = new UserService();
		mockery = new Mockery();
		userDao = mockery.mock(UserDao.class);
	}
	
	@Test
	public void loadUserByUsernameTest(){
		final String username = "murpha74";
		final User user = new User();
		user.setUsername(username);
		userService.setUserDao(userDao);
		
		mockery.checking(new Expectations() {{
			one (userDao).getUserByUsername(username);
			will(returnValue((user)));
		}});
		
		SecurityUser user2 = (SecurityUser) userService.loadUserByUsername(username);
		SecurityUser user3 = new SecurityUser(user.getUsername(), user.getPassword(), false, true, true, true, null);
		Assert.assertEquals(user2, user3);
	}
}	
