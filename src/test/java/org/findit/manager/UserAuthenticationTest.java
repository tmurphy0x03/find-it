/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import junit.framework.Assert;

import org.findit.model.SecurityUser;
import org.junit.Before;
import org.junit.Test;

public class UserAuthenticationTest {
	private UserAuthentication userAuthentication;
	private SecurityUser securityUser;
	
	@Before
	public void setup(){
		securityUser = new SecurityUser("username", "password", false, false, false, false, null);
		userAuthentication = new UserAuthentication(securityUser);
	}
	
	@Test
	public void getNameTest(){
		String username = userAuthentication.getName();
		Assert.assertEquals(username, securityUser.getUsername());
	}
	
	@Test
	public void getAuthoritiesTest(){
		Assert.assertNull(userAuthentication.getAuthorities());
	}
	
	@Test
	public void getCredentialsTest(){
		String password = (String) userAuthentication.getCredentials();
		Assert.assertEquals(password, securityUser.getPassword());
	}
	
	@Test
	public void getPrincipalTest(){
		Assert.assertEquals(securityUser, userAuthentication.getPrincipal());
	}
	
	@Test
	public void isAuthenticatedTest(){
		Assert.assertTrue(userAuthentication.isAuthenticated());
	}
	
	@Test
	public void setAuthenticatedTestNotAuthenticated(){
		userAuthentication.setAuthenticated(false);
		Assert.assertFalse(userAuthentication.isAuthenticated());
	}
	
	@Test
	public void setAuthenticatedTest(){
		userAuthentication.setAuthenticated(false);
		Assert.assertFalse(userAuthentication.isAuthenticated());
		try{
			userAuthentication.setAuthenticated(true);
		}
		catch(IllegalArgumentException e){
			Assert.assertFalse(userAuthentication.isAuthenticated());
		}
	}
}
