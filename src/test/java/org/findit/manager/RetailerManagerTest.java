/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import junit.framework.Assert;

import org.findit.dao.RetailerDao;
import org.findit.model.SecurityRetailer;
import org.findit.model.Retailer;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.userdetails.UserDetailsService;

public class RetailerManagerTest {
	private RetailerManagerImpl retailerManager;
	private RetailerDao retailerDao;
	private Mockery mockery;
	private UserDetailsService retailerService;
	
	@Before
	public void setup(){
		retailerManager = new RetailerManagerImpl();
		mockery = new Mockery();
		retailerDao = mockery.mock(RetailerDao.class);
		retailerService = mockery.mock(UserDetailsService.class);
	}
	
	@Test
	public void getRetailerByRetailernameTest(){
		final String retailername = "tony";
		final Retailer validRetailer = new Retailer();
		(retailerManager).setRetailerDao(retailerDao);
		
		mockery.checking(new Expectations() {{
			one (retailerDao).getRetailerByUsername(retailername);
			will(returnValue((validRetailer)));
		}});
		Retailer retailer = retailerManager.getRetailerByUsername(retailername);
		
		mockery.assertIsSatisfied();
		Assert.assertEquals(validRetailer, retailer);
	}
	
	@Test
	public void updateRetailerTest(){
		final Retailer validRetailer = new Retailer();
		retailerManager.setRetailerDao(retailerDao);
		
		mockery.checking(new Expectations() {{
			one (retailerDao).updateRetailer(validRetailer);
			will(returnValue((validRetailer)));
		}});
		
		Retailer retailer = retailerManager.updateRetailer(validRetailer);
		
		mockery.assertIsSatisfied();
		Assert.assertEquals(validRetailer, retailer);
	}
	
	@Test
	public void existsTest(){
		final String retailername = "tony";
		retailerManager.setRetailerDao(retailerDao);
		
		mockery.checking(new Expectations() {{
			one (retailerDao).exists(retailername);
			will(returnValue((true)));
		}});
		
		Boolean exists = retailerManager.exists(retailername);
		
		mockery.assertIsSatisfied();
		Assert.assertTrue(exists);
	}
	
	@Test
	public void loadRetailerByRetailernameTest(){
		final String retailername = "murpha74";
		final SecurityRetailer retailer = new SecurityRetailer(retailername, null, false, false, false, false, null);
		
		retailerManager.setRetailerService(retailerService);
		mockery.checking(new Expectations() {{
			one (retailerService).loadUserByUsername(retailername);
			will(returnValue((retailer)));
		}});
		
		SecurityRetailer retailer2 = (SecurityRetailer) retailerManager.loadUserByUsername(retailername);
		Assert.assertEquals(retailer.getUsername(), retailer2.getUsername());
	}
}
