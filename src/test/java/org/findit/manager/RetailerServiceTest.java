/**
 *
 * @author Anthony Murphy
 */

package org.findit.manager;

import junit.framework.Assert;

import org.findit.dao.RetailerDao;
import org.findit.model.SecurityRetailer;
import org.findit.model.Retailer;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;

public class RetailerServiceTest {
	private RetailerService retailerService;
	private RetailerDao retailerDao;
	private Mockery mockery;
	
	@Before
	public void setup(){
		retailerService = new RetailerService();
		mockery = new Mockery();
		retailerDao = mockery.mock(RetailerDao.class);
	}
	
	@Test
	public void loadUserByUsernameTest(){
		final String retailername = "murpha74";
		final Retailer retailer = new Retailer();
		retailer.setUsername(retailername);
		retailerService.setRetailerDao(retailerDao);
		
		mockery.checking(new Expectations() {{
			one (retailerDao).getRetailerByUsername(retailername);
			will(returnValue((retailer)));
		}});
		
		SecurityRetailer retailer2 = (SecurityRetailer) retailerService.loadUserByUsername(retailername);
		SecurityRetailer retailer3 = new SecurityRetailer(retailer.getUsername(), retailer.getPassword(), true, true, true, true, null);
		Assert.assertEquals(retailer2, retailer3);
	}
}	
