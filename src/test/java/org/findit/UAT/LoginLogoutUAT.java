/**
 * UAT for loggin in
 * @author Anthony Murphy
 */

package org.findit.UAT;

import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class LoginLogoutUAT {
	private WebClient webClient;
	private HtmlPage page;
	
	@Before
	public void setup() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		webClient = new WebClient();
		webClient.setJavaScriptEnabled(false);
		page = (HtmlPage) webClient.getPage("http://localhost:8080/FindIt/login.html");
	}
	
	/**
	 * Test username doesn't exist
	 */
	@Test
	public void loginTestUsernameNotExist(){
//		Assert.assertTrue(page.getTitleText().contains("FindIt"));
//		Assert.assertTrue(page.asText().contains("Login"));
//		Assert.assertNotNull(page.getFormByName("loginForm"));
//		
//		/* Fill in login form */
//		HtmlForm form = page.getFormByName("loginForm");
//		HtmlTextInput textField = (HtmlTextInput) form.getElementById("uname");
//		textField.setValueAttribute("murpha74");
//		HtmlPasswordInput passwordInput = (HtmlPasswordInput) form.getElementById("pword");
//		passwordInput.setValueAttribute("password");
//		HtmlSubmitInput button = (HtmlSubmitInput) form.getElementById("submit");
//		try {
//			page = (HtmlPage) button.click();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		Assert.assertTrue(page.asText().contains("Username doesn't exist"));
	}
	
	@Test
	public void createAccountTest(){
//		Assert.assertTrue(page.getTitleText().contains("FindIt"));
//		Assert.assertTrue(page.asText().contains("Login"));
//		
//		final HtmlAnchor anchor = page.getAnchorByHref("createAccount.html");
//		try {
//			page = anchor.click();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		Assert.assertTrue(page.getTitleText().contains("FindIt"));
//		Assert.assertTrue(page.asText().contains("Login"));
//		Assert.assertTrue(page.asText().contains("Create Account"));
//		
//		/* Fill in Create Account form */
//		HtmlForm form = page.getFormByName("createAccountForm");
//		HtmlTextInput textField = (HtmlTextInput) form.getElementById("fname");
//		textField.setValueAttribute("Tony");
//		textField = (HtmlTextInput) form.getElementById("sname");
//		textField.setValueAttribute("Murphy");
//		textField = (HtmlTextInput) form.getElementById("email");
//		textField.setValueAttribute("tonymurphy1989@gmail.com");
//		textField = (HtmlTextInput) form.getElementById("uname");
//		textField.setValueAttribute("murpha74");
//		HtmlPasswordInput passwordInput = (HtmlPasswordInput) form.getElementById("pword");
//		passwordInput.setValueAttribute("password");
//		passwordInput = (HtmlPasswordInput) form.getElementById("pword2");
//		passwordInput.setValueAttribute("password");
//		HtmlSubmitInput button = (HtmlSubmitInput) form.getElementById("submit");
//		try {
//			page = (HtmlPage) button.click();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		Assert.assertTrue(page.getTitleText().contains("FindIt"));
//		Assert.assertTrue(page.asText().contains("Logout murpha74"));
//		
//		final HtmlAnchor logoutAnchor = page.getAnchorByHref("logout.html");
//		try {
//			page = logoutAnchor.click();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		Assert.assertTrue(page.asText().contains("Login"));
	}

}
