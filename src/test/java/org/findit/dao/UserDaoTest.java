/**
 * Integration test for <code>UserDao</code>
 * @author Anthony Murphy
 */

package org.findit.dao;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import junit.framework.Assert;

import org.findit.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@TransactionConfiguration(defaultRollback=true)
@ContextConfiguration(locations = {"classpath:findit-hibernate-test.xml"})
public class UserDaoTest {
	
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private UserDao userDao;
	
	@Test
	public void getUserByUsernameTest(){
		User user = new User();
		user.setUsername("username");
		em.persist(user);
		Assert.assertEquals(1, userDao.size());
		User user2 = userDao.getUserByUsername(user.getUsername());
		Assert.assertEquals(user, user2);
	}
	
	@Test
	public void getUserByUsernameTestUserNotExists(){
		User user = userDao.getUserByUsername("wrong username");
		Assert.assertNull(user);
	}
	
	@Test
	public void updateUserTest(){
		User user = new User();
		user.setUsername("username2");
		Assert.assertEquals(0, userDao.size());
		userDao.updateUser(user);
		Query query = em.createQuery("From User WHERE username = :username");
		query.setParameter("username", user.getUsername());
		User user2 = (User) query.getSingleResult();
		Assert.assertEquals(1, userDao.size());
		Assert.assertEquals(user, user2);
	}
	
	@Test
	public void existsTest(){
		User user = new User();
		user.setUsername("username3");
		Assert.assertEquals(0, userDao.size());
		em.persist(user);
		Assert.assertEquals(1, userDao.size());
		Assert.assertTrue(userDao.exists(user.getUsername()));
	}
	
	@Test
	public void existsTestDoesntExist(){
		User user = new User();
		user.setUsername("username4");
		Assert.assertEquals(0, userDao.size());
		em.persist(user);
		Assert.assertEquals(1, userDao.size());
		Assert.assertFalse(userDao.exists(user.getUsername()+"invalid"));
	}
	
	@Test
	public void getAllUsersTest(){
		User user = new User();
		user.setUsername("username5");
		User user2 = new User();
		user2.setUsername("username6");
		List<User> users = new LinkedList<User>();
		users.add(user);
		users.add(user2);
		Assert.assertEquals(0, userDao.size());
		em.persist(user);
		em.persist(user2);
		Assert.assertEquals(users.size(), userDao.getAllUsers().size());
		Assert.assertEquals(users, userDao.getAllUsers());
	}
	
	@Test
	public void sizeTest(){
		User user = new User();
		user.setUsername("username5");
		User user2 = new User();
		user2.setUsername("username6");
		List<User> users = new LinkedList<User>();
		users.add(user);
		users.add(user2);
		Assert.assertEquals(0, userDao.size());
		em.persist(user);
		em.persist(user2);
		Assert.assertEquals(users.size(), userDao.size());
	}
}
