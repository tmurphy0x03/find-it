/**
 * Integration test for <code>RetailerDao</code>
 * @author Anthony Murphy
 */

package org.findit.dao;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import junit.framework.Assert;

import org.findit.model.Retailer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@TransactionConfiguration(defaultRollback=true)
@ContextConfiguration(locations = {"classpath:findit-hibernate-test.xml"})
public class RetailerDaoTest {
	
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private RetailerDao retailerDao;
	
	@Before
	public void setup(){
		retailerDao.setEm(em);
	}
	
	@Test
	public void getRetailerByRetailernameTest(){
		Retailer retailer = new Retailer();
		retailer.setUsername("retailername");
		em.merge(retailer);
		Assert.assertEquals(1, retailerDao.size());
		Retailer retailer2 = retailerDao.getRetailerByUsername(retailer.getUsername());
		Assert.assertEquals(retailer, retailer2);
	}
	
	@Test
	public void getRetailerByRetailernameTestRetailerNotExists(){
		Retailer retailer = retailerDao.getRetailerByUsername("wrong retailername");
		Assert.assertNull(retailer);
	}
	
	@Test
	public void updateRetailerTest(){
		Retailer retailer = new Retailer();
		retailer.setUsername("retailername2");
		Assert.assertEquals(0, retailerDao.size());
		retailerDao.updateRetailer(retailer);
		Query query = em.createQuery("From Retailer WHERE username = :username");
		query.setParameter("username", retailer.getUsername());
		Retailer retailer2 = (Retailer) query.getSingleResult();
		Assert.assertEquals(1, retailerDao.size());
		Assert.assertEquals(retailer, retailer2);
	}
	
	@Test
	public void existsTest(){
		Retailer retailer = new Retailer();
		retailer.setUsername("retailername3");
		Assert.assertEquals(0, retailerDao.size());
		em.persist(retailer);
		Assert.assertEquals(1, retailerDao.size());
		Assert.assertTrue(retailerDao.exists(retailer.getUsername()));
	}
	
	@Test
	public void existsTestDoesntExist(){
		Retailer retailer = new Retailer();
		retailer.setUsername("retailername4");
		Assert.assertEquals(0, retailerDao.size());
		em.persist(retailer);
		Assert.assertEquals(1, retailerDao.size());
		Assert.assertFalse(retailerDao.exists(retailer.getUsername()+"invalid"));
	}
	
	@Test
	public void getAllRetailersTest(){
		Retailer retailer = new Retailer();
		retailer.setUsername("retailername5");
		Retailer retailer2 = new Retailer();
		retailer2.setUsername("retailername6");
		List<Retailer> retailers = new LinkedList<Retailer>();
		retailers.add(retailer);
		retailers.add(retailer2);
		Assert.assertEquals(0, retailerDao.size());
		em.persist(retailer);
		em.persist(retailer2);
		Assert.assertEquals(retailers.size(), retailerDao.getAllRetailers().size());
		Assert.assertEquals(retailers, retailerDao.getAllRetailers());
	}
	
	@Test
	public void sizeTest(){
		Retailer retailer = new Retailer();
		retailer.setUsername("retailername5");
		Retailer retailer2 = new Retailer();
		retailer2.setUsername("retailername6");
		List<Retailer> retailers = new LinkedList<Retailer>();
		retailers.add(retailer);
		retailers.add(retailer2);
		Assert.assertEquals(0, retailerDao.size());
		em.persist(retailer);
		em.persist(retailer2);
		Assert.assertEquals(retailers.size(), retailerDao.size());
	}
}
