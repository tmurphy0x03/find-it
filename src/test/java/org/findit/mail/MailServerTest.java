/**
 *
 * @author Anthony Murphy
 */

package org.findit.mail;

import org.findit.model.Retailer;
import org.findit.model.User;
import org.jasypt.util.text.BasicTextEncryptor;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class MailServerTest {
	private MailServerImpl mailServer;
	private MailSender mailSender;
	private Mockery mockery;
	private BasicTextEncryptor basicTextEncryptor;
	
	@Before
	public void setup(){
		mailServer = new MailServerImpl();
		mockery = new Mockery();
		mailSender = mockery.mock(MailSender.class);
		basicTextEncryptor = new BasicTextEncryptor();
		basicTextEncryptor.setPassword("password");
	}
	
	@Test
	public void sendForgotPasswordMailTest(){
		final User user = new User();
		user.setFirstName("tony");
		user.setEmail("test@fake.com");
		user.setPassword(basicTextEncryptor.encrypt("password"));
		
		mailServer.setMailSender(mailSender);
		mailServer.setBasicTextEncryptor(basicTextEncryptor);
		
		mockery.checking(new Expectations() {{
			oneOf (mailSender).send(with(any(SimpleMailMessage.class)));
		}});
		
		mailServer.sendForgotPasswordMail(user);
		mockery.assertIsSatisfied();
	}
	
	@Test
	public void sendForgotPasswordMailTestRetailer(){
		final Retailer retailer = new Retailer();
		retailer.setName("astropark");
		retailer.setEmail("test@fake.com");
		retailer.setPassword(basicTextEncryptor.encrypt("password"));
		
		mailServer.setMailSender(mailSender);
		mailServer.setBasicTextEncryptor(basicTextEncryptor);
		
		mockery.checking(new Expectations() {{
			oneOf (mailSender).send(with(any(SimpleMailMessage.class)));
		}});
		
		mailServer.sendForgotPasswordMail(retailer);
		mockery.assertIsSatisfied();
	}
}
