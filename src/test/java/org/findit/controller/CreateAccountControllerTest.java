/**
 * Unit test for <code>CreateAccountController</code>
 * @author Anthony Murphy
 */

package org.findit.controller;

import junit.framework.Assert;

import org.findit.manager.RetailerManager;
import org.findit.manager.UserAuthentication;
import org.findit.manager.UserManager;
import org.findit.model.Retailer;
import org.findit.model.SecurityRetailer;
import org.findit.model.SecurityUser;
import org.findit.model.User;
import org.jasypt.util.text.BasicTextEncryptor;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

public class CreateAccountControllerTest {
	private CreateAccountController createAccountController;
	private ModelAndView modelAndView;
	private Mockery mockery;
	private Validator validator;
	private BindingResult result;
	private BasicTextEncryptor basicTextEncryptor;
	private UserManager userManager;
	private RetailerManager retailerManager;
	
	@Before
	public void setup(){
		createAccountController = new CreateAccountController();
		mockery = new Mockery();
		validator = mockery.mock(Validator.class);
		userManager = mockery.mock(UserManager.class);
		retailerManager = mockery.mock(RetailerManager.class);
		basicTextEncryptor = new BasicTextEncryptor();
		basicTextEncryptor.setPassword("password");
	}
	
	@Test
	public void getCreateAccountPageTest(){
		modelAndView = createAccountController.getCreateAccountPage();
		Assert.assertEquals("createAccount", modelAndView.getViewName());
		Assert.assertTrue(modelAndView.getModel().containsKey("user"));
		Assert.assertTrue(modelAndView.getModel().get("user") instanceof User);
		Assert.assertFalse(modelAndView.getModel().containsKey("username"));
		Assert.assertTrue(modelAndView.getModel().containsKey("action"));
		Assert.assertEquals("Create a Customer", modelAndView.getModel().get("action"));
	}
	
	/**
	 * Test creating a valid account
	 */
	@Test
	public void createAccountTestValid(){
//		final User user = new User();
//		final String username = "tony";
//		user.setUsername(username);
//		createAccountController.setUserValidator(validator);
//		result = new BindException(user, "user");
//		createAccountController.setBasicTextEncryptor(basicTextEncryptor);
//		createAccountController.setUserManager(userManager);
//		
//		mockery.checking(new Expectations() {{
//			oneOf (validator).validate(user, result);
//			
//			oneOf (userManager).updateUser(user);
//			
//			oneOf (userManager).loadUserByUsername(username);
//		}});
//		createAccountController.createAccount(user, result);
//		
//		mockery.assertIsSatisfied();
//		Assert.assertEquals("index", modelAndView.getViewName());
//		Assert.assertFalse(modelAndView.getModel().containsKey("errors"));
	}
	
	@Test
	public void getCreateRetailerAccountTest(){
		modelAndView = createAccountController.getCreateRetailerAccountPage();
		Assert.assertEquals("createRetailerAccount", modelAndView.getViewName());
		Assert.assertTrue(modelAndView.getModel().containsKey("retailer"));
		Assert.assertTrue(modelAndView.getModel().get("retailer") instanceof Retailer);
		Assert.assertFalse(modelAndView.getModel().containsKey("username"));
		Assert.assertTrue(modelAndView.getModel().containsKey("action"));
		Assert.assertEquals("Create a Retailer", modelAndView.getModel().get("action"));
	}
	
	@Test
	public void createRetailerAccountValid(){
//		final Retailer retailer = new Retailer();
//		final String username = "tony";
//		retailer.setUsername(username);
//		createAccountController.setRetailerValidator(validator);
//		result = new BindException(retailer, "retailer");
//		createAccountController.setBasicTextEncryptor(basicTextEncryptor);
//		createAccountController.setRetailerManager(retailerManager);
//		
//		mockery.checking(new Expectations() {{
//			oneOf (validator).validate(retailer, result);
//			
//			oneOf (retailerManager).updateRetailer(retailer);
//			
//			oneOf (retailerManager).loadUserByUsername(username);
//		}});
//		modelAndView = createAccountController.createRetailerAccount(retailer, result);
//		
//		mockery.assertIsSatisfied();
//		Assert.assertTrue(modelAndView.getModel() instanceof RedirectView);
//		Assert.assertFalse(modelAndView.getModel().containsKey("errors"));
	}
	
	@Test
	public void getEditAccountPageTest(){
		final String username = "username";
		SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(new SecurityUser(username, null, false, false, false, false, null)));
		createAccountController.setUserManager(userManager);
		createAccountController.setBasicTextEncryptor(basicTextEncryptor);
		final User user = new User();
		
		mockery.checking(new Expectations() {{
			oneOf (userManager).getUserByUsername(username);
			will(returnValue(user));
		}});
		
		modelAndView = createAccountController.getEditAccountPage();
		Assert.assertEquals("createAccount", modelAndView.getViewName());
		Assert.assertTrue(modelAndView.getModel().containsKey("user"));
		Assert.assertTrue(modelAndView.getModel().get("user") instanceof User);
		Assert.assertTrue(modelAndView.getModel().containsKey("action"));
		Assert.assertEquals("Edit", modelAndView.getModel().get("action"));
		SecurityContextHolder.getContext().setAuthentication(null);
	}
	
	@Test
	public void getEditRetailerAccountPageTest(){
		final String username = "username";
		SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(new SecurityRetailer(username, null, false, false, false, false, null)));
		createAccountController.setRetailerManager(retailerManager);
		createAccountController.setBasicTextEncryptor(basicTextEncryptor);
		final Retailer retailer = new Retailer();
		
		mockery.checking(new Expectations() {{
			oneOf (retailerManager).getRetailerByUsername(username);
			will(returnValue(retailer));
		}});
		
		modelAndView = createAccountController.getEditRetailerAccountPage();
		Assert.assertEquals("createRetailerAccount", modelAndView.getViewName());
		Assert.assertTrue(modelAndView.getModel().containsKey("retailer"));
		Assert.assertTrue(modelAndView.getModel().get("retailer") instanceof Retailer);
		Assert.assertTrue(modelAndView.getModel().containsKey("action"));
		Assert.assertEquals("Edit", modelAndView.getModel().get("action"));
		SecurityContextHolder.getContext().setAuthentication(null);
	}
	
	@After
	public void tearDown(){
		SecurityContextHolder.getContext().setAuthentication(null);
	}
}
