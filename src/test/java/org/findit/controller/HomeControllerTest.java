package org.findit.controller;

import junit.framework.Assert;

import org.findit.manager.UserTypeChecker;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;

/**
 * Unit tests for <code>HomeController</code>
 *
 * @author Anthony Murphy
 */
public class HomeControllerTest {
	private HomeController homeController;
	private ModelAndView modelAndView;
	private UserTypeChecker userTypeChecker;
	
	@Before
	public void setup(){
		homeController = new HomeController();
		userTypeChecker = new UserTypeChecker();
		homeController.setUserTypeChecker(userTypeChecker);
	}
	
	@Test
	public void getHomeTest(){
//		homeController.getHome();
//		Assert.assertTrue(homeController.getHome(). instanceof RedirectView);
//		Assert.assertEquals("index", modelAndView.getViewName());
//		Assert.assertFalse(modelAndView.getModel().containsKey("username"));
	}
	
	@Test
	public void getIndexTest(){
//		modelAndView = homeController.getIndex();
//		Assert.assertEquals("index", modelAndView.getViewName());
//		Assert.assertFalse(modelAndView.getModel().containsKey("username"));
	}
	
	@After
	public void tearDown(){
		SecurityContextHolder.getContext().setAuthentication(null);
	}
}
