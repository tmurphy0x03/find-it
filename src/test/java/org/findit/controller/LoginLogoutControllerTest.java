/**
 * Unit tests for <code>LoginController</code>
 * @author Anthony Murphy
 */

package org.findit.controller;

import junit.framework.Assert;

import org.findit.manager.UserManager;
import org.findit.model.User;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.servlet.ModelAndView;

public class LoginLogoutControllerTest {
	private LoginLogoutController loginLogoutController;
	private ModelAndView modelAndView;
	private Mockery mockery;
	private Validator validator;
	private BindingResult result;
	private UserManager userManager;
	
	@Before
	public void setup(){
		loginLogoutController = new LoginLogoutController();
		mockery = new Mockery();
		validator = mockery.mock(Validator.class);
		userManager = mockery.mock(UserManager.class);
	}
	
	@Test
	public void getLoginTest(){
		modelAndView = loginLogoutController.getLogin();
		Assert.assertEquals("login", modelAndView.getViewName());
		Assert.assertTrue(modelAndView.getModel().containsKey("user"));
		Assert.assertTrue(modelAndView.getModel().get("user") instanceof User);
	}
	
//	/**
//	 * Test a valid Login
//	 */
//	@Test
//	public void loginTestValid(){
//		final User user = new User();
//		final String username = "tony";
//		user.setUsername(username);
//		loginLogoutController.setLoginValidator(validator);
//		result = new BindException(user, "user");
//		loginLogoutController.setUserManager(userManager);
//		
//		mockery.checking(new Expectations() {{
//			oneOf (validator).validate(user, result);
//			
//			oneOf (userManager).loadUserByUsername(username);
//		}});
//		modelAndView = loginLogoutController.login(user, result);
//		
//		mockery.assertIsSatisfied();
//		Assert.assertEquals("index", modelAndView.getViewName());
//		Assert.assertFalse(modelAndView.getModel().containsKey("errors"));
//	}
	
	/**
	 * Test a invalid Login
	 */
	@Test
	public void loginTestInvalid(){
		final User user = new User();
		loginLogoutController.setLoginValidator(validator);
		result = new BindException(user, "user");
		result.addError(new ObjectError("user", "login error"));
		mockery.checking(new Expectations() {{
			oneOf (validator).validate(user, result);
		}});
		modelAndView = loginLogoutController.login(user, result);
		
		mockery.assertIsSatisfied();
		Assert.assertEquals("login", modelAndView.getViewName());
		Assert.assertTrue(modelAndView.getModel().containsKey("errors"));
		Assert.assertFalse(modelAndView.getModel().containsKey("username"));
	}
	
	@After
	public void tearDown(){
		SecurityContextHolder.getContext().setAuthentication(null);
	}
}
